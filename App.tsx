'use strict'
import React from 'react'
import {ErrorHandlerCallback} from 'react-native'

import {ContainerStartManager} from 'grassica-app'

import {Localization, catchRender, globalUtil, config} from 'grassica-ui'

const localeEnJson = {
    data: require('./src/localization/en.json'),
}
const localeDeJson = {
    data: require('./src/localization/de.json'),
}
let en = localeEnJson.data

let de = localeDeJson.data

global.Buffer = global.Buffer || require('buffer').Buffer

import Redux from 'redux'
import {Middleware} from 'redux'
import {combineReducers, createStore, applyMiddleware} from 'redux'
import {createLogger} from 'redux-logger'
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'
import {composeWithDevTools} from 'redux-devtools-extension'

import {ReducerRoot as reducerGrassicaApp} from 'grassica-app'
import {ReducerRoot as reducerGrassicaUi} from 'grassica-ui'

import {IRootState} from 'RootState'

const reducer: Redux.Reducer<IRootState> = combineReducers<IRootState>({
    grassicaApp: reducerGrassicaApp,
    grassicaUi: reducerGrassicaUi,
})
Localization.setLocale(
    {
        en: en,
        de: de
    }
)

declare const __DEV__: boolean
console.disableYellowBox = true

const middleware: Redux.Middleware[] = [ thunk ]
if (__DEV__) {
    middleware.push(createLogger( ))
}

const enhancer = __DEV__ ? composeWithDevTools(applyMiddleware(...middleware)) : applyMiddleware(...middleware)

const store = createStore(
    reducer,
    enhancer,
)

const defaultErrorHandler: ErrorHandlerCallback = ErrorUtils.getGlobalHandler()

const wrapGlobalHandler = (error: any, isFatal?: boolean) => {

    alert(error + '\n\n' + JSON.stringify(error))

    // After that call the defaultHandler so that react-native also gets the error
    // defaultErrorHandler(error, isFatal)
}

ErrorUtils.setGlobalHandler(wrapGlobalHandler)


class Application extends React.Component<any, any> {

    render() {
        return (
            <Provider store={store}>
                <ContainerStartManager testID={'test-id-ContainerAuthorization'} instanceType={'Default'}/>
            </Provider>
        )
    }
}
export default Application
