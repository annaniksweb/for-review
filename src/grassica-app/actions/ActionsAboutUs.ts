/**
 * Actions of AboutUs container.
 *
 * @author Burnaev M.U.
 * @see
 */

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import {IAboutUsState} from 'grassica-app'

/* START - AboutUs container additional imports and module code. */

/* END - AboutUs container additional imports and module code. */

export const PERFORM_CONTAINER_EXECUTE = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE'
export const PERFORM_CONTAINER_EXECUTE_EXECUTE = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE_EXECUTE'
export const PERFORM_CONTAINER_EXECUTE_SUCCESS = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE_SUCCESS'
export const PERFORM_CONTAINER_EXECUTE_FAILURE = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE_FAILURE'
export const PERFORM_CONTAINER_RESET = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_RESET'
export const PERFORM_CONTAINER_RECOVER = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_RECOVER'
export const PERFORM_CONTAINER_INIT = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_INIT'
export const PERFORM_CONTAINER_REFRESH = 'ABOUT_US_CONTAINER_PERFORM_CONTAINER_REFRESH'
export const PRESENTER_UPDATE = 'ABOUT_US_CONTAINER_PRESENTER_UPDATE'
export const PRESENTER_UPDATE_EXECUTE = 'ABOUT_US_CONTAINER_PRESENTER_UPDATE_EXECUTE'
export const PRESENTER_UPDATE_SUCCESS = 'ABOUT_US_CONTAINER_PRESENTER_UPDATE_SUCCESS'
export const PRESENTER_UPDATE_FAILURE = 'ABOUT_US_CONTAINER_PRESENTER_UPDATE_FAILURE'

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 */
export type PERFORM_CONTAINER_EXECUTE_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType}
export const performContainerExecute = (instanceType: Enums.ContainerAboutUsInstanceType): Models.IAction<PERFORM_CONTAINER_EXECUTE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_EXECUTE,
    payload: {
        instanceType: instanceType,
    },
})

/*
 * Action dispatched on thunk chain 'performContainerExecute' started. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
 */
export type PERFORM_CONTAINER_EXECUTE_EXECUTE_PAYLOAD = {operationId: string | null}
export const performContainerExecuteExecute = (instanceType: Enums.ContainerAboutUsInstanceType, operationId: string | null): Models.IAction<PERFORM_CONTAINER_EXECUTE_EXECUTE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_EXECUTE_EXECUTE,
    payload: {
        operationId: operationId,
    },
})

/*
 * Action dispatched on success in thunk 'performContainerExecute'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
 *
 * @param {Models.IAboutUsContainerExecute} data Result data of thunk chain.
 */
export type PERFORM_CONTAINER_EXECUTE_SUCCESS_PAYLOAD = {data: Models.IAboutUsContainerExecute}
export const performContainerExecuteSuccess = (instanceType: Enums.ContainerAboutUsInstanceType, data: Models.IAboutUsContainerExecute): Models.IAction<PERFORM_CONTAINER_EXECUTE_SUCCESS_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_EXECUTE_SUCCESS,
    payload: {
        data: data,
    },
})

/*
 * Action dispatched on failure in thunk 'performContainerExecute'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
 *
 * @param {Error} error Description of error while executing thunk chain.
 */
export type PERFORM_CONTAINER_EXECUTE_FAILURE_PAYLOAD = {error: Models.IError}
export const performContainerExecuteFailure = (instanceType: Enums.ContainerAboutUsInstanceType, error: Models.IError): Models.IAction<PERFORM_CONTAINER_EXECUTE_FAILURE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_EXECUTE_FAILURE,
    payload: {
        error: error,
    },
})

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 */
export type PERFORM_CONTAINER_RESET_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType}
export const performContainerReset = (instanceType: Enums.ContainerAboutUsInstanceType): Models.IAction<PERFORM_CONTAINER_RESET_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_RESET,
    payload: {
        instanceType: instanceType,
    },
})

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 * @param {IAboutUsState} stateRecovering Container "AboutUs" recovering state..
 */
export type PERFORM_CONTAINER_RECOVER_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState}
export const performContainerRecover = (instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState): Models.IAction<PERFORM_CONTAINER_RECOVER_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_RECOVER,
    payload: {
        instanceType: instanceType,
        stateRecovering: stateRecovering,
    },
})

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to init container state.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 * @param {Models.IAboutUsContext} context Container "AboutUs" context parameters..
 * @param {Models.IAboutUsInstance} instance Container "AboutUs" instance parameters..
 */
export type PERFORM_CONTAINER_INIT_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, }
export const performContainerInit = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ): Models.IAction<PERFORM_CONTAINER_INIT_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_INIT,
    payload: {
        instanceType: instanceType,
        context: context,
        instance: instance,
    },
})

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to refresh container state.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 * @param {Models.IAboutUsContext} context Container "AboutUs" context parameters..
 * @param {Models.IAboutUsInstance} instance Container "AboutUs" instance parameters..
 */
export type PERFORM_CONTAINER_REFRESH_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, }
export const performContainerRefresh = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ): Models.IAction<PERFORM_CONTAINER_REFRESH_PAYLOAD> => ({
    instanceType: instanceType,
    type: PERFORM_CONTAINER_REFRESH,
    payload: {
        instanceType: instanceType,
        context: context,
        instance: instance,
    },
})

/**
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
 *
 * @param {Enums.ContainerAboutUsInstanceType} instanceType Current instance..
 */
export type PRESENTER_UPDATE_PAYLOAD = {instanceType: Enums.ContainerAboutUsInstanceType}
export const presenterUpdate = (instanceType: Enums.ContainerAboutUsInstanceType): Models.IAction<PRESENTER_UPDATE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PRESENTER_UPDATE,
    payload: {
        instanceType: instanceType,
    },
})

/*
 * Action dispatched on thunk chain 'presenterUpdate' started. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
 */
export type PRESENTER_UPDATE_EXECUTE_PAYLOAD = {operationId: string | null}
export const presenterUpdateExecute = (instanceType: Enums.ContainerAboutUsInstanceType, operationId: string | null): Models.IAction<PRESENTER_UPDATE_EXECUTE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PRESENTER_UPDATE_EXECUTE,
    payload: {
        operationId: operationId,
    },
})

/*
 * Action dispatched on success in thunk 'presenterUpdate'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
 *
 * @param {Models.IAboutUsPresenter} data Result data of thunk chain.
 */
export type PRESENTER_UPDATE_SUCCESS_PAYLOAD = {data: Models.IAboutUsPresenter}
export const presenterUpdateSuccess = (instanceType: Enums.ContainerAboutUsInstanceType, data: Models.IAboutUsPresenter): Models.IAction<PRESENTER_UPDATE_SUCCESS_PAYLOAD> => ({
    instanceType: instanceType,
    type: PRESENTER_UPDATE_SUCCESS,
    payload: {
        data: data,
    },
})

/*
 * Action dispatched on failure in thunk 'presenterUpdate'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
 *
 * @param {Error} error Description of error while executing thunk chain.
 */
export type PRESENTER_UPDATE_FAILURE_PAYLOAD = {error: Models.IError}
export const presenterUpdateFailure = (instanceType: Enums.ContainerAboutUsInstanceType, error: Models.IError): Models.IAction<PRESENTER_UPDATE_FAILURE_PAYLOAD> => ({
    instanceType: instanceType,
    type: PRESENTER_UPDATE_FAILURE,
    payload: {
        error: error,
    },
})
