/*
 * Created by Burnaev M.U.
 */

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as Converters from '../models/Converters'
import * as Cache from '../modules/Cache'
import {config, globalUtil, IFetchParameters} from 'grassica-ui'
import {IRootState} from 'RootState'
import {IStartManagerState} from 'grassica-app'
import {ISplashState} from 'grassica-app'
import {IRegistrationState} from 'grassica-app'
import {IAuthorizationState} from 'grassica-app'
import {IMainState} from 'grassica-app'
import {IAboutUsState} from 'grassica-app'
import {IClaimState} from 'grassica-app'
import {IMarketListState} from 'grassica-app'
import {IQrCodeState} from 'grassica-app'
import {IBonusState} from 'grassica-app'
import {IQuestionState} from 'grassica-app'
import {IIdeaState} from 'grassica-app'
import {IConfirmState} from 'grassica-app'
import {IProfileState} from 'grassica-app'
import {IDataSecurityState} from 'grassica-app'

// const url = 'https://virtserver.swaggerhub.com/Rubedite/USTRONG/1.0.0'  // SWAGGER
// const url = 'http://grassica.rubedite-dev.tw1.ru/rest/grassica.mobile'  // DEV
// const url = 'http://grassica.ustrong.ru/rest/grassica.mobile'  // TEST
const url = 'https://test.newgardenlab.com/rest/grassica.mobile'  // PROD

export const urlStartManager = {
    callPostUserDetails : (state: IRootState, reducerState: IStartManagerState, instanceType: Enums.ContainerStartManagerInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container StartManager thunk callPostUserDetails call url. */`${url}.user.info`/* END - Container StartManager thunk callPostUserDetails call url. */, tagList: tagList/* START - Call callPostUserDetails async params. *//* END - Call callPostUserDetails async params. */}
    },
    callPostLogout : (state: IRootState, reducerState: IStartManagerState, instanceType: Enums.ContainerStartManagerInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container StartManager thunk callPostLogout call url. */`${url}.logout`/* END - Container StartManager thunk callPostLogout call url. */, tagList: tagList/* START - Call callPostLogout async params. *//* END - Call callPostLogout async params. */}
    },
    callPostVersion : (state: IRootState, reducerState: IStartManagerState, instanceType: Enums.ContainerStartManagerInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container StartManager thunk callPostVersion call url. */`${url}.info`/* END - Container StartManager thunk callPostVersion call url. */, tagList: tagList/* START - Call callPostVersion async params. *//* END - Call callPostVersion async params. */}
    },
    callPostLog : (state: IRootState, reducerState: IStartManagerState, instanceType: Enums.ContainerStartManagerInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container StartManager thunk callPostLog call url. */`https://rubedite.com/app_logs/`/* END - Container StartManager thunk callPostLog call url. */, tagList: tagList/* START - Call callPostLog async params. *//* END - Call callPostLog async params. */}
    },
}
export const urlSplash = {
}
export const urlRegistration = {
    callPostRegistrationConfirm : (state: IRootState, reducerState: IRegistrationState, instanceType: Enums.ContainerRegistrationInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Registration thunk callPostRegistrationConfirm call url. */`${url}.confirmcode.send`/* END - Container Registration thunk callPostRegistrationConfirm call url. */, tagList: tagList/* START - Call callPostRegistrationConfirm async params. *//* END - Call callPostRegistrationConfirm async params. */}
    },
    callPostRegistration : (state: IRootState, reducerState: IRegistrationState, instanceType: Enums.ContainerRegistrationInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Registration thunk callPostRegistration call url. */`${url}.user.update`/* END - Container Registration thunk callPostRegistration call url. */, tagList: tagList/* START - Call callPostRegistration async params. *//* END - Call callPostRegistration async params. */}
    },
}
export const urlAuthorization = {
    callPostAuthorization : (state: IRootState, reducerState: IAuthorizationState, instanceType: Enums.ContainerAuthorizationInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Authorization thunk callPostAuthorization call url. */`${url}.confirmcode.send`/* END - Container Authorization thunk callPostAuthorization call url. */, tagList: tagList/* START - Call callPostAuthorization async params. *//* END - Call callPostAuthorization async params. */}
    },
    callPostAuthorizationConfirm : (state: IRootState, reducerState: IAuthorizationState, instanceType: Enums.ContainerAuthorizationInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Authorization thunk callPostAuthorizationConfirm call url. */`${url}.confirmcode.check`/* END - Container Authorization thunk callPostAuthorizationConfirm call url. */, tagList: tagList/* START - Call callPostAuthorizationConfirm async params. *//* END - Call callPostAuthorizationConfirm async params. */}
    },
}
export const urlMain = {
}
export const urlAboutUs = {
}
export const urlClaim = {
    callPostClaim : (state: IRootState, reducerState: IClaimState, instanceType: Enums.ContainerClaimInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Claim thunk callPostClaim call url. */`${url}.claim`/* END - Container Claim thunk callPostClaim call url. */, tagList: tagList/* START - Call callPostClaim async params. *//* END - Call callPostClaim async params. */}
    },
}
export const urlMarketList = {
    callPostMarketList : (state: IRootState, reducerState: IMarketListState, instanceType: Enums.ContainerMarketListInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container MarketList thunk callPostMarketList call url. */`${url}.shop.list`/* END - Container MarketList thunk callPostMarketList call url. */, tagList: tagList/* START - Call callPostMarketList async params. *//* END - Call callPostMarketList async params. */}
    },
}
export const urlQrCode = {
}
export const urlBonus = {
}
export const urlQuestion = {
    callPostQuestion : (state: IRootState, reducerState: IQuestionState, instanceType: Enums.ContainerQuestionInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Question thunk callPostQuestion call url. */`${url}.question`/* END - Container Question thunk callPostQuestion call url. */, tagList: tagList/* START - Call callPostQuestion async params. *//* END - Call callPostQuestion async params. */}
    },
}
export const urlIdea = {
    callPostIdea : (state: IRootState, reducerState: IIdeaState, instanceType: Enums.ContainerIdeaInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Idea thunk callPostIdea call url. */`${url}.idea`/* END - Container Idea thunk callPostIdea call url. */, tagList: tagList/* START - Call callPostIdea async params. *//* END - Call callPostIdea async params. */}
    },
}
export const urlConfirm = {
    callPostConfirm : (state: IRootState, reducerState: IConfirmState, instanceType: Enums.ContainerConfirmInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Confirm thunk callPostConfirm call url. */`${url}FIXME/revalidate_confirm`/* END - Container Confirm thunk callPostConfirm call url. */, tagList: tagList/* START - Call callPostConfirm async params. *//* END - Call callPostConfirm async params. */}
    },
}
export const urlProfile = {
    callPostProfileConfirm : (state: IRootState, reducerState: IProfileState, instanceType: Enums.ContainerProfileInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Profile thunk callPostProfileConfirm call url. */`${url}.confirmcode.send`/* END - Container Profile thunk callPostProfileConfirm call url. */, tagList: tagList/* START - Call callPostProfileConfirm async params. *//* END - Call callPostProfileConfirm async params. */}
    },
    callPostProfile : (state: IRootState, reducerState: IProfileState, instanceType: Enums.ContainerProfileInstanceType, tagList: Models.ICacheTag[]): Models.IUrl => {
        return {url: /* START - Container Profile thunk callPostProfile call url. */`${url}.user.update`/* END - Container Profile thunk callPostProfile call url. */, tagList: tagList/* START - Call callPostProfile async params. *//* END - Call callPostProfile async params. */}
    },
}
export const urlDataSecurity = {
}

const requestBuilder = (method: string, requestData: any, headers: any, isPinningEnabled: boolean): IFetchParameters => {
    switch (method) {
        case 'GET': {
            let fetchParameters: IFetchParameters = {method: method, headers: headers, credentials: 'same-origin'}

            return fetchParameters
        }
        default: {
            let fetchParameters: IFetchParameters = {method: method, headers: headers, credentials: 'same-origin'}

            fetchParameters.body = JSON.stringify(requestData)
            return fetchParameters
        }
    }
}

export const call = <T, TB>(callUrl: Models.IUrl,
                            success: (response: Models.IResponse<T>) => void,
                            failure: (response: Models.IError) => void,
                            converter: (data: any) => T,
                            method: string = 'GET',
                            mock: ( (success: (response: Models.IResponse<T>) => void, failure: (response: Models.IError) => void) => void ) | null,
                            headers: any = {},
                            requestConverter: (data: TB) => any = () => null,
                            requestData: TB | null = null,
) => {
    log('FETCH ' + method + ' ' + callUrl.url)

    // Use mock object
    if (mock) {
        mock(success, failure)

        return
    }

    // RequestData
    let requestDataToBuilder: any = null
    try {
        if (requestData !== null) {
            performanceCheckStart('REQUEST_CONVERTER: ' + callUrl.url)

            requestDataToBuilder = requestConverter(requestData)

            performanceCheckStop('REQUEST_CONVERTER: ' + callUrl.url, 500)
        }
    } catch (error) {
        let requestError = {
            ...defaultValues.IError,
            type: 'SomethingWentWrong' as Enums.ErrorType,
        }
        log('RequestConverterError', error.message ? error.message : error)
        failure(Converters.CONVERT_ERROR(requestError))
        return
    }

    // Recover body from cache
    let cacheData = Cache.responseRecover(callUrl, requestDataToBuilder, headers)
    try {
        if (cacheData !== null) {
            performanceCheckStart('RESPONSE_CONVERTER: ' + callUrl.url)
            let data = converter(cacheData)
            performanceCheckStop('RESPONSE_CONVERTER: ' + callUrl.url, 500)

            success({
                data: data,
                cachedDate: new Date(),
            })

            return
        }
    } catch (error) {
        let responseError = {
            ...defaultValues.IError,
            type: 'SomethingWentWrong' as Enums.ErrorType,
        }
        log('ResponseConverterError', error.message ? error.message : error)
        failure(Converters.CONVERT_ERROR(responseError))
        return
    }

    let loggerUrl: string = callUrl.url
    let loggerRequestHeaders: any = headers
    let loggerRequestBody: any = requestDataToBuilder
    let loggerResponseInfo: any = null
    let loggerResponseHeaders: any = null
    let loggerResponseBody: any = null
    let loggerResponseJson: any = null
    let loggerResponseData: any = null
    let loggerResponseError: any = null

    logNetworkRequest(false, loggerUrl, loggerRequestHeaders, loggerRequestBody, loggerResponseHeaders, loggerResponseInfo, loggerResponseBody, loggerResponseJson, loggerResponseData, loggerResponseError)

    const isPinningEnabled: boolean = config.isPinningEnabled(callUrl.url)

    fetch(callUrl.url, requestBuilder(method, requestDataToBuilder, headers, isPinningEnabled))
        .then((response: any) => {

            loggerResponseBody = (response as any).bodyString
            loggerResponseInfo = response
            loggerResponseHeaders = response.headers

            switch (response.status) {

                case 400:
                case 200: {

                    /* START - Additional code on fetch success. */
                    try {
                        let json = response.json()
                        return json
                    } catch (error) {
                        throw {
                            type: 'JsonParserError',
                            code: '',
                            message: error.message ? error.message : error,
                            comment: '',
                        }
                    }
                    /* END - Additional code on fetch success. */

                }
                case 404: {
                    throw {
                        type: 'TechnicalError',
                        code: '404',
                        message: '',
                        comment: '',
                    }
                }
                default: {
                    throw {
                        type: 'NetworkError',
                        code: String(response.status),
                        message: '',
                        comment: '',
                    }
                }
            }
        })
        .then((responseJSON: any) => {
            loggerResponseJson = responseJSON

            // FIXME временно, пока из swagger приходит и ошибка и success=true
            // if (responseJSON.success === true && responseJSON.error) {
            //     let responseError = responseJSON.error
            //     loggerResponseBody = responseJSON
            //     throw {
            //         type: 'ServerError',
            //         code: responseError && responseError.code ? responseError.code : '',
            //         message: responseError && responseError.text ? responseError.text : '',
            //         comment: '',
            //     }
            // }

            try {
                if (responseJSON.success === undefined && responseJSON.result) {
                    if (responseJSON.result.error === undefined || responseJSON.result.error === null) {
                        responseJSON = {success: true, data: responseJSON.result.data ? responseJSON.result.data : responseJSON.result}
                    } else {
                        responseJSON = {success: false, error: responseJSON.result.error}
                    }
                }
                if (responseJSON.success === true) {
                    let body = responseJSON.data ? responseJSON.data : responseJSON

                    loggerResponseJson = responseJSON
                    loggerResponseBody = body
                    // Save body to cache
                    Cache.responsePersist(callUrl, body, requestDataToBuilder)

                    performanceCheckStart('RESPONSE_CONVERTER: ' + callUrl.url)
                    let data = converter(body)
                    performanceCheckStop('RESPONSE_CONVERTER: ' + callUrl.url, 500)

                    loggerResponseData = data

                    logNetworkRequest(true, loggerUrl, loggerRequestHeaders, loggerRequestBody, loggerResponseHeaders, loggerResponseInfo, loggerResponseBody, loggerResponseJson, loggerResponseData, loggerResponseError)

                    success({
                        data: data,
                        cachedDate: new Date(),
                    })
                    return
                } else if (responseJSON.success === false) {
                    if (responseJSON.error) {
                        let commentError: string = ''
                        if (responseJSON.error.text) {
                            commentError = responseJSON.error.text
                        }
                        throw {
                            ...defaultValues.IError,
                            type: 'ServerError',
                            message: commentError
                        }
                    } else {
                        throw {
                            ...defaultValues.IError,
                            type: 'TechnicalError'
                        }
                    }
                } else {
                    throw {
                        ...defaultValues.IError,
                        type: 'TechnicalError'
                    }
                }
            } catch (error) {

                loggerResponseData = error

                throw {
                    type: error.type ? error.type : 'JsonConverterError',
                    code: error.code ? error.code.toString() : '',
                    message: error.message ? error.message : '',
                    comment: '',
                }
            }
        })
        .catch((error) => {

            if (!error.type) {
                error = {
                    type: 'JsonParserError',
                    code: '',
                    message: error.message ? error.message : error,
                    comment: '',
                }
            }

            loggerResponseError = error

            error.details = JSON.stringify({
                url: callUrl.url,
                requestHeaders: loggerRequestHeaders,
                requestBody: loggerRequestBody,
                responseHeaders: loggerResponseHeaders,
                responseBody: loggerResponseJson,
            })

            logNetworkRequest(true, loggerUrl, loggerRequestHeaders, loggerRequestBody, loggerResponseHeaders, loggerResponseInfo, loggerResponseBody, loggerResponseJson, loggerResponseData, loggerResponseError)

            failure(Converters.CONVERT_ERROR(error))
        })
}

export const logNetworkRequest = (isCompleted: boolean, urlLog: string, requestHeaders: any, requestBody: any, responseHeaders: any, responseInfo: any, responseBody: any, loggerResponseJson: any, responseData: any, responseError: any, requestAsync?: any) => {

    let responseBodyJson = ''
    let logPrefix: string = requestAsync ? '[ASYNC] Network request' : 'Network request'
    try {
        responseBodyJson = JSON.parse(responseBody)
    } catch (e) {
        responseBodyJson = responseBody
    }

    if (__DEV__) {
        if (!isCompleted) {
            log(`>>>>>>>>>>>>>>>>>>>> ${logPrefix} started ` + urlLog + ' >>>>>>>>>>>>>>>>>>>>', {urlLog: urlLog, requestHeaders: requestHeaders, requestBody: requestBody, responseHeaders: responseHeaders, responseInfo: responseInfo, responseBody: responseBodyJson, responseJson: loggerResponseJson, responseData: responseData, responseError: responseError, requestAsync: requestAsync})
        } else {
            log(`<<<<<<<<<<<<<<<<<<<< ${logPrefix} completed ` + urlLog + ' <<<<<<<<<<<<<<<<<<<<', {urlLog: urlLog, requestHeaders: requestHeaders, requestBody: requestBody, responseHeaders: responseHeaders, responseInfo: responseInfo, responseBody: responseBodyJson, responseJson: loggerResponseJson, responseData: responseData, responseError: responseError, requestAsync: requestAsync})
        }
    }
}

export const log = (message?: any, ...optionalParams: any[]): void => {
    if (__DEV__) {
        const tsConsole = console
        tsConsole.log(message, ...optionalParams)
    }
}

export const consoleError = (message?: any, ...optionalParams: any[]): void => {
    if (__DEV__) {
        const tsConsole = console
        tsConsole.error(message, ...optionalParams)
    }
}

export const consoleInfo = (message?: any, ...optionalParams: any[]): void => {
    if (__DEV__) {
        const tsConsole = console
        tsConsole.info(message, ...optionalParams)
    }
}

let performanceCheckData: any = {}
export const performanceCheckStart = (performanceCheckOperation: string) => {
    if (__DEV__) {
        performanceCheckData[performanceCheckOperation] = new Date().getTime()
    }
}

export const performanceCheckStop = (performanceCheckOperation: string, durationLimit: number) => {
    if (__DEV__) {
        let performanceCheckDuration = Math.floor(new Date().getTime() - performanceCheckData[performanceCheckOperation])
        let performanceCheckMessage = 'Operation "' + performanceCheckOperation + '" not optimized ! Execution took ' + performanceCheckDuration + ' milliseconds.'
        if (performanceCheckDuration > durationLimit && performanceCheckDuration < 20000 /* Debugger skip */) {
            consoleWarning(performanceCheckMessage)
        } else {
            if (performanceCheckDuration > 15) {
                consoleWarning('Operation "' + performanceCheckOperation + '" took ' + performanceCheckDuration + ' milliseconds.')
            }
        }
    }
}

export const consoleWarning = (message?: any, ...optionalParams: any[]): void => {
    if (__DEV__) {
        const tsConsole = console
        tsConsole.warn(message, ...optionalParams)
    }
}

export const guid = (): string => {
    const s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1)
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4()
}

export const pushRoute = (route: any, item: any, isSingleTop: boolean, navigationPreviousApp?: any, isHiddenNavigation?: boolean, removeRouteType?: string): any => {
    let newRouteData = route.data.slice()
    if (isSingleTop) {
        newRouteData = newRouteData.filter((s: Models.IRouteItem) => s.type !== item.type)
    }
    if (navigationPreviousApp) {
        item.navigationPreviousApp = navigationPreviousApp
    }
    if (isHiddenNavigation !== undefined) {
        item.isHiddenNavigation = isHiddenNavigation
    }
    if (removeRouteType) {
        newRouteData = newRouteData.filter((s: Models.IRouteItem) => s.type !== removeRouteType)
    }
    newRouteData.push(item)
    return {data: newRouteData}
}

export const popRoute = <T extends Models.IRoute>(route: T): T => {
    if (!route.data) {
        return route
    }
    if (route.data.length === 1) {
        return route
    }
    let newRoute: T = JSON.parse(JSON.stringify(route))

    const top: Models.IRouteItem | null = topRoute(newRoute)
    if (top && top.isHiddenNavigation === false) {
        newRoute.data.pop()
        return newRoute
    }

    let newRouteData: Models.IRouteItem[] = newRoute.data.filter( (routeItem: Models.IRouteItem) => {
        if (routeItem.isHiddenNavigation === true) {
            return false
        }
        return true
    })

    if (newRouteData.length === 1) {
        newRoute.data = newRouteData
        return newRoute
    } else {
        if (newRouteData.length === route.data.length) {
            newRouteData.pop()
        }
    }
    newRoute.data = newRouteData
    return newRoute
}

export const topRoute = <T extends Models.IRoute>(route: T): Models.IRouteItem | null => {
    let index = route.data.length - 1
    if (index < 0) {
        return null
    }
    return route.data[index]
}

export const topRouteEquals = (route: any, item: any) => {
    let routeTop = topRoute(route)

    if (routeTop !== null) {
        return routeTop.type === item.type
    } else {
        return false
    }
}

export const topRouteOneOf = (route: any, items: string[]) => {
    let routeTop = topRoute(route)

    if (routeTop !== null) {
        return items.indexOf(routeTop.type) !== -1
    } else {
        return false
    }

}

const isXmlRequest = (headers: any): boolean => {
    return headers && headers['Content-Type'] && headers['Content-Type'] === 'application/xml'
}

const isXmlResponse = (headers: any, isPinningEnabled: boolean): boolean => {
    return isPinningEnabled ?
        headers && headers['Content-Type'] && headers['Content-Type'] === 'application/xml' :
        headers && headers.get('Content-Type') && headers.get('Content-Type') === 'application/xml'
}

const isBlobResponse = (headers: any, isPinningEnabled: boolean): boolean => {
    return isPinningEnabled ?
        headers && headers['Content-Type'] && headers['Content-Type'] === 'application/pdf' :
        headers && headers.get('Content-Type') && headers.get('Content-Type') === 'application/pdf'
}

const getResponseRqUID = (headers: any, isPinningEnabled: boolean): string => {
    if (isPinningEnabled) {
        if (headers && headers.rquid) {
            return headers.rquid
        }
    } else {
        if (headers && headers.get('rquid')) {
            return headers.get('rquid')
        }
    }
    log('getResponseRqUID return new RqUID!')
    return guid()
}

/* START - Additional utility functions and constants. */
import {Localization} from 'grassica-ui'

export const isAuthorizationValid = (state: IAuthorizationState): Models.IAuthorizationValidationErrors => {
    let authorizationValidationErrors: Models.IAuthorizationValidationErrors = {...defaultValues.IAuthorizationValidationErrors}
    if (state.authorization.success && state.inputCode.length < 4) {
        authorizationValidationErrors.errorCode = Localization.t('grassica-ui.floatInput.invalidCode')
    } else if (state.authorizationMode === 'Email' && (state.inputEmail === '' || !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g))) {
        authorizationValidationErrors.errorEmail = Localization.t('grassica-ui.floatInput.invalidEmail')
    } else if (state.authorizationMode === 'Phone' && (state.inputPhone === '' || !state.inputPhone.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im))) {
        authorizationValidationErrors.errorPhone = Localization.t('grassica-ui.floatInput.invalidPhone')
    }
    return authorizationValidationErrors
}

export const isRegistrationValid = (state: IRegistrationState): Models.IRegistrationValidationErrors => {

    let validationErrors = {...defaultValues.IRegistrationValidationErrors}
    if (state.inputName === null || state.inputName.length === 0) {
        validationErrors.inputNameError = Localization.t('grassica-app.registration.nameError')
    }
    if (state.inputLastName === null || state.inputLastName.length === 0) {
        validationErrors.inputLastNameError = Localization.t('grassica-app.registration.lastNameError')
    }

    if ( (state.inputPhone === null || state.inputPhone.length === 0) && (state.inputEmail === null || state.inputEmail.length === 0) ) {
        validationErrors.inputPhoneError = Localization.t('grassica-ui.floatInput.invalidPhone')
    } else {
        if (state.inputPhone !== null && state.inputPhone.length > 0 && !state.inputPhone.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
            validationErrors.inputPhoneError = Localization.t('grassica-ui.floatInput.invalidPhone')
        }
        if (state.inputEmail !== null && state.inputEmail.length > 0 && !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
            validationErrors.inputEmailError = Localization.t('grassica-ui.floatInput.invalidEmail')
        }
    }

    if (state.inputLanguage === null || state.inputLanguage.length === 0) {
        validationErrors.inputLanguageError = Localization.t('grassica-app.registration.languageError')
    }
    // if (state.inputPassword === null || state.inputPassword.length === 0) {
    //     validationErrors.inputPasswordError = Localization.t('grassica-app.registration.passwordError')
    // }
    // if (state.inputPasswordConfirm === null || state.inputPasswordConfirm.length === 0 || state.inputPasswordConfirm !== state.inputPassword ) {
    //     validationErrors.inputPasswordConfirmError = Localization.t('grassica-app.registration.passwordConfirmError')
    // }

    return validationErrors
}

export const isClaimValid = (state: IClaimState): Models.IClaimValidationErrors => {

    let validationErrors = {...defaultValues.IClaimValidationErrors}
    if (state.inputEmail === null || state.inputEmail.length === 0
    || !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
        validationErrors.inputEmailError = Localization.t('grassica-app.sendingComments.emailError')
    }
    if (state.inputWhatToFix === null || state.inputWhatToFix.length === 0) {
        validationErrors.inputWhatToFixError = Localization.t('grassica-app.sendingComments.whatToFixError')
    }

    return validationErrors
}

export const isQuestionValid = (state: IQuestionState): Models.IQuestionValidationErrors => {

    let validationErrors = {...defaultValues.IQuestionValidationErrors}

	if (state.inputEmail === null || state.inputEmail.length === 0
		|| !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
			validationErrors.inputEmailError = Localization.t('grassica-app.question.emailError')
		}
    if (state.inputQuestion === null || state.inputQuestion.length === 0) {
        validationErrors.inputQuestionError = Localization.t('grassica-app.question.messageError')
    }
    return validationErrors
}

export const isIdeaValid = (state: IIdeaState): Models.IIdeaValidationErrors => {

    let validationErrors = {...defaultValues.IIdeaValidationErrors}

	if (state.inputEmail === null || state.inputEmail.length === 0
		|| !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
			validationErrors.inputEmailError = Localization.t('grassica-app.idea.emailError')
		}
    if (state.inputIdea === null || state.inputIdea.length === 0) {
        validationErrors.inputIdeaError = Localization.t('grassica-app.idea.ideaError')
    }
    return validationErrors
}

export const isConfirmValid = (state: IConfirmState): Models.IConfirmValidationErrors => {

    let validationErrors = {...defaultValues.IConfirmValidationErrors}
    if (state.inputConfirm === null || state.inputConfirm.length !== 4 || !/^\d+$/.test(state.inputConfirm)) {
        validationErrors.inputCodeError = Localization.t('grassica-app.confirm.codeError')
    }

    return validationErrors
}

export const isProfileValid = (state: IProfileState): Models.IProfileValidationErrors => {

    let validationErrors = {...defaultValues.IProfileValidationErrors}
    if (state.inputName === null || state.inputName.length === 0) {
        validationErrors.inputNameError = Localization.t('grassica-app.registration.nameError')
    }
    if (state.inputLastName === null || state.inputLastName.length === 0) {
        validationErrors.inputLastNameError = Localization.t('grassica-app.registration.lastNameError')
    }

    if ( (state.inputPhone === null || state.inputPhone.length === 0) && (state.inputEmail === null || state.inputEmail.length === 0) ) {
        validationErrors.inputPhoneError = Localization.t('grassica-ui.floatInput.invalidPhone')
    } else {
        if (state.inputPhone !== null && state.inputPhone.length > 0 && !state.inputPhone.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im)) {
            validationErrors.inputPhoneError = Localization.t('grassica-ui.floatInput.invalidPhone')
        }
        if (state.inputEmail !== null && state.inputEmail.length > 0 && !state.inputEmail.match(/^(([^<>()\[\]\\.,:\s@']+(\.[^<>()\[\]\\.,:\s@']+)*)|('.+'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/g)) {
            validationErrors.inputEmailError = Localization.t('grassica-ui.floatInput.invalidEmail')
        }
    }

    if (state.inputLanguage === null || state.inputLanguage.length === 0) {
        validationErrors.inputLanguageError = Localization.t('grassica-app.registration.languageError')
    }
    // if (state.inputPassword === null || state.inputPassword.length === 0) {
    //     validationErrors.inputPasswordError = Localization.t('grassica-app.registration.passwordError')
    // }
    // if (state.inputPasswordConfirm === null || state.inputPasswordConfirm.length === 0 || state.inputPasswordConfirm !== state.inputPassword ) {
    //     validationErrors.inputPasswordConfirmError = Localization.t('grassica-app.registration.passwordConfirmError')
    // }

    return validationErrors
}

export const getDistanceFromLatLonInKm = (lat1: number, lon1: number, lat2: number, lon2: number): number => {
    let R = 6371 // Radius of the earth in km
    let dLat = deg2rad(lat2 - lat1)  // deg2rad below
    let dLon = deg2rad(lon2 - lon1)
    let a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
    let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    let d = R * c // Distance in km
    return d
}

function deg2rad(deg: number) {
    return deg * (Math.PI / 180)
}

let isMockEnabled: boolean = false
export const getIsMockEnabled = (): boolean => {
    return isMockEnabled
}

let tokenApi: string = 'grassica'
export const getTokenApi = (): string => {
    return 'grassica'
}

let versionBackEndMP: string = '1.0'
export const compareVersionBackEnd = (versionBackEnd: string): boolean => {
    if (!versionBackEnd) {
        return true
    }
    if (versionBackEnd && versionBackEndMP) {
        const versionBackEndMPList: string[] = versionBackEndMP.split('.')
        const versionBackEndList: string[] = versionBackEnd.split('.')
        if (versionBackEndList.length > 1 && versionBackEndMP.length > 1) {
            if (versionBackEndMPList[0] === versionBackEndList[0] && versionBackEndMPList[1] === versionBackEndList[1]) {
                return true
            }
        }
    }
    return false
}

export const isAuthorized = (userDetails: Models.IUserDetailsResult): boolean => {
    if (userDetails.email && userDetails.email.length > 0) {
        return true
    }
    if (userDetails.phone && userDetails.phone.length > 0) {
        return true
    }
    return false
}

export const isCorrectUser = (userDetails: Models.IUserDetailsResult): boolean => {
    return ((userDetails.email === '' && userDetails.phone === '') || (!userDetails.name || !userDetails.lastName))
}

/* END - Additional utility functions and constants. */
