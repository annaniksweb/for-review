/*
 * Created by Burnaev M.U.
 */

import React, {Component, ReactElement} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as thunkAboutUs from '../thunk/ThunkAboutUs'

import {
    /* START - Mobile platform components import. */

	View, Text, Localization, Button, Header, Image,

	/* END - Mobile platform components import. */
} from 'grassica-ui'

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

/* START - View AboutUs additional imports and module code. */
import Styles from './styles/ViewAboutUsStyles'
import { ScrollView } from 'react-native'

const renderContent = (props: IProps): ReactElement<View> => {
    return (
        <View style={Styles.main}>
            <Header type={'Default'}
                    getLeftContent={() => <Button title={''}
                                                  type={'ArrowLeft'}
                                                  isEnabled={true}
                                                  onTap={() => props.navigateBack('Default')} />
                    }
                    getCenterContent={() => <Text title={Localization.t('grassica-app.aboutUs.headerTitle')}
                                                  type={'TextMediumBezh'} />
                    }
            />
            <View style={Styles.containerHeaderLine}/>
            <ScrollView>
                <View style={{marginTop: 20}}>
                    <Text type={'TextLargGreen'}
                          title={Localization.t('grassica-app.aboutUs.articleTitle')}
                    />
                </View>
                <View style={{marginTop: 20, marginHorizontal: 16, marginBottom: 80}}>
                    <Text type={'TextMediumBezhDefault'}
                          title={Localization.t('grassica-app.aboutUs.articleFirstDescription')}
                    />
                </View>
                <View style={Styles.whiteContainer}>
                    <View style={Styles.imageStyle}>
                        <Image type={'AboutUsFirstFlex'} />
                    </View>
                    <View style = {{marginTop:30}}>
                        <Text type={'TextLargBlack'} title={Localization.t('grassica-app.aboutUs.headTitle')} />
                    </View>
                    <View style = {{marginTop:25}}>
                        <Text weight ={'bold'} type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.firstParagraphHeading')} />
                    </View>
                    <View style = {{marginTop:15}}>
                        <Text  type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.firstParagraphText')} />
                    </View>

                    <View style = {{marginTop:25}}>
                        <Text weight ={'bold'} type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.secondParagraphHeading')} />
                    </View>
                    <View style = {{marginTop:15}}>
                        <Text  type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.secondParagraphText')} />
                    </View>

                    <View style = {{marginTop:25}}>
                        <Text weight ={'bold'} type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.thirdParagraphHeading')} />
                    </View>
                    <View style = {{marginTop:15}}>
                        <Text  type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.thirdParagraphText')} />
                    </View>

                    <View style = {{marginTop:25}}>
                        <Text weight ={'bold'} type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.fourthParagraphHeading')} />
                    </View>
                    <View style = {{marginTop:15}}>
                        <Text  type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.fourthParagraphText')} />
                    </View>
                    <View style={[Styles.imageStyle,{ marginTop: 20}]}>
                        <Image type={'AboutUsSecond'} />
                    </View>
                    <View style = {{marginTop:25}}>
                        <Text weight ={'bold'} type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.fifthParagraphHeading')} />
                    </View>
                    <View style = {{marginTop:15, paddingBottom:50}}>
                        <Text  type={'TextMediumBlack'} title={Localization.t('grassica-app.aboutUs.fifthParagraphText')} />
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}
/* END - View AboutUs additional imports and module code. */

/*
 * UI stateless functional component presenter for 'AboutUs' container.
 */

export interface IProps {
    performContainerExecute: ModelsAboutUs.IPerformContainerExecute,
    performContainerReset: ModelsAboutUs.IPerformContainerReset,
    performContainerRecover: ModelsAboutUs.IPerformContainerRecover,
    performContainerInit: ModelsAboutUs.IPerformContainerInit,
    performContainerRefresh: ModelsAboutUs.IPerformContainerRefresh,
    presenterUpdate: ModelsAboutUs.IPresenterUpdate,
    navigateBack: ModelsStartManager.INavigateBack,
    containerExecute: Models.IAboutUsContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IAboutUsPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IAboutUsContext,
    instanceId: string,
    instance: Models.IAboutUsInstance,

    /* START - View AboutUs additional props. */

	/* END - View AboutUs additional props. */

    testID?: string | undefined,
    instanceType: Enums.ContainerAboutUsInstanceType
}

const ViewAboutUs: React.FunctionComponent<IProps> = (props: IProps): React.ReactElement<View> =>
/* START - View AboutUs content. */
	(
		renderContent(props)
	)
/* END - View AboutUs content. */

export default ViewAboutUs
