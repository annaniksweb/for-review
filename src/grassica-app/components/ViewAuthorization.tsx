/*
 * Created by Burnaev M.U.
 */

import React, {Component, ReactElement} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as thunkAuthorization from '../thunk/ThunkAuthorization'

import {
    /* START - Mobile platform components import. */

    View,
    Button,
    IItem,
    TabSelect,
    FloatInput,
    Header,
    Localization,
    Text,
    InputCode,
    Modal,

    /* END - Mobile platform components import. */
} from 'grassica-ui'

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

/* START - View Authorization additional imports and module code. */
import Styles from './styles/ViewAuthorizationStyles'
import PhoneInput from 'react-native-phone-input'

// todo локализация
// todo test-id

const ACTIVATION_CODE_LENGTH: number = 4

const getEnumMode = (mode: string): Enums.AuthorizationMode => {
    return mode === 'Email' ? 'Email' : 'Phone'
}

const getCharAt = (activationCode: string, index: number): string | null => {
    if (activationCode.length < index) {
        return null
    }
    return activationCode[index]
}

const activationCodeSymbolRender = (renderSymbol: string | null, props: IProps, index: number): React.ReactElement<View> => {
    if (renderSymbol == null) {
        return (
            <View key={'Key-Text' + index} style={[Styles.containerCodeActivationCell, Styles.borderColorDot]}>
                <View style={Styles.dot}/>
            </View>
        )
    }

    return (
        <View key={'Key-Symbol' + index} style={[Styles.containerCodeActivationCell, Styles.borderColorNumber]}>
            <Text type={'TextBezh'} title={renderSymbol}/>
        </View>
    )
}

const renderInputCode = (props: IProps): React.ReactElement<View> => {
    let result: React.ReactNode[] = []

    for (let i = 0; i < ACTIVATION_CODE_LENGTH; i++) {
        let renderSymbol: null | string = getCharAt(props.inputCode, i)
        let text: React.ReactElement<View> = activationCodeSymbolRender(renderSymbol, props, i)
        result.push(text)
    }

    return (
        <View>
            <View style={Styles.containerCodeActivationText}>
                <Text type={'TextSmallGrey'}
                      title={'Email Confirmation'}
                      testID={props.testID + '-InputString-Default-Text-1'}
                />
                <Text type={'TextSmallGreen'}
                      title={'*'}
                      testID={props.testID + '-InputString-Default-Text-2'}
                />
            </View>
            <View style={Styles.containerCodeActivation}>
                {result}
            </View>
            {
                props.validationErrors.errorCode ?
                    <View style={{marginTop: 3}}>
                        <Text type={'TextSmallRed'} title={props.validationErrors.errorCode}/>
                    </View> : null
            }
        </View>
    )
}

const renderInput = (props: IProps): React.ReactElement<View> => {
    let phoneRef: any
    if (props.authorization.success && props.authorizationPhase === 'Success') {
        return renderInputCode(props)
    } else if (props.authorizationMode === 'Email') {
        return (
            <View>
                <FloatInput keyboardType={'email-address'}
                            error={props.validationErrors.errorEmail ? props.validationErrors.errorEmail : ''}
                            label={Localization.t('grassica-app.authorization.email')}
                            onChangeText={(text: string) => props.performInputEmail(props.instanceType, text)}
                            value={props.inputEmail}
                            isDisabled={false}
                            isRequired={true}
                />
            </View>
        )
    } else {
        return (
            <View>
                <View style={{borderBottomColor: 'white', borderBottomWidth: 2, marginTop: 15}}>
                    <View style={Styles.phoneTitle}>
                        <Text title={Localization.t('grassica-app.authorization.phone')} type={'TextSmallGrey'}/>
                        <Text title={'*'} type={'TextSmallGreen'}/>
                    </View>
                    <PhoneInput
                        flagStyle={{width: 33, height: 20, borderWidth: 0}}
                        onPressFlag={() => null}
                        ref={(ref: any) => phoneRef = ref}
                        style={{
                            paddingBottom: 8,
                        }}
                        textStyle={{
                            fontSize: 18,
                            color: 'white'
                        }}
                        onChangePhoneNumber={(textNumber: number) => {
                            let text: string = textNumber.toString()

                            if (text.substr(0, 1) !== '+') {
                                text = '+' + text
                                phoneRef.onChangePhoneNumber(text)
                                return
                            }
                            props.performInputPhone(props.instanceType, text)
                        }}
                    />
                </View>
                {
                    props.validationErrors.errorPhone ?
                        <View style={{paddingTop: 3}}>
                            <Text type={'TextSmallRed'} title={props.validationErrors.errorPhone}/>
                        </View> : null
                }
            </View>

        )
    }
}

const renderContent = (props: IProps): ReactElement<View> => {

    const itemListTemp: IItem[] = [
        {
            value: 'Email',
            label: 'grassica-app.authorization.email',
            isDisabled: !!(props.instance && props.instance.mode),
        },
        {
            value: 'Phone',
            label: 'grassica-app.authorization.phone',
            isDisabled: !!(props.instance && props.instance.mode),
        }
    ]

    return (
        <View style={Styles.main}>
            <Header type={'Default'}
                    getLeftContent={() => <Button title={''}
                                                  type={'ArrowLeft'}
                                                  isEnabled={true}
                                                  onTap={() => props.navigateBack('Default')}/>
                    }
                    getCenterContent={() => (
                        <Text
                            type={'TextMediumBezh'}
                            title={Localization.t(
                                props.instance && props.instance.mode ?
                                    props.instance.mode === 'Email' ?
                                        'grassica-app.authorization.headerTitleEmail' :
                                        props.instance.mode === 'Phone' ?
                                            'grassica-app.authorization.headerTitlePhone' :
                                            'grassica-app.authorization.headerTitle' :
                                    'grassica-app.authorization.headerTitle'
                            )}
                        />
                    )}
            />
            <View style={Styles.container}>

                <View>
                    <TabSelect type={'Default'}
                               isEnabled={props.instance.mode === null}
                               value={props.authorizationMode}
                               onChange={(value: string | null) => value ? props.performInputAuthorizationMode(props.instanceType, getEnumMode(value)) : null}
                               itemList={itemListTemp}
                    />
                </View>
                {renderInput(props)}

                <View style={props.authorization.success || props.authorizationMode === 'Phone' ? Styles.buttonTopPhoneCode : Styles.buttonTopEmail}>
                    {
                        props.authorization.success ?
                            <Button title={Localization.t('grassica-app.authorization.nextButton')}
                                    type={'ButtonGreen'}
                                    isEnabled={true}
                                    onTap={() => props.callPostAuthorizationConfirm('Default')}
                            />
                            :
                            <Button title={Localization.t('grassica-app.authorization.sendButton')}
                                    type={'ButtonGreen'}
                                    isEnabled={true}
                                    onTap={() => props.callPostAuthorization('Default')}
                            />
                    }

                </View>
            </View>
            {
                props.authorization.success ?
                    <View style={Styles.hiddenInput}>
                        <InputCode type={'Default'}
                                   title={Localization.t('grassica-app.authorization.emailConfirmation')}
                                   value={props.inputCode}
                                   onChange={(value: string | null) => value !== null ? props.performInputCode(props.instanceType, value) : null}
                                   isEnabled={true}
                        />
                    </View> : null
            }
        </View>
    )
}
/* END - View Authorization additional imports and module code. */

/*
 * UI stateless functional component presenter for 'Authorization' container.
 */

export interface IProps {
    performInputAuthorizationMode: ModelsAuthorization.IPerformInputAuthorizationMode,
    performInputPhone: ModelsAuthorization.IPerformInputPhone,
    performInputEmail: ModelsAuthorization.IPerformInputEmail,
    performInputCode: ModelsAuthorization.IPerformInputCode,
    performValidate: ModelsAuthorization.IPerformValidate,
    callPostAuthorizationReset: ModelsAuthorization.ICallPostAuthorizationReset,
    callPostAuthorization: ModelsAuthorization.ICallPostAuthorization,
    callPostAuthorizationConfirmReset: ModelsAuthorization.ICallPostAuthorizationConfirmReset,
    callPostAuthorizationConfirm: ModelsAuthorization.ICallPostAuthorizationConfirm,
    performContainerExecute: ModelsAuthorization.IPerformContainerExecute,
    performContainerReset: ModelsAuthorization.IPerformContainerReset,
    performContainerRecover: ModelsAuthorization.IPerformContainerRecover,
    performContainerInit: ModelsAuthorization.IPerformContainerInit,
    performContainerRefresh: ModelsAuthorization.IPerformContainerRefresh,
    presenterUpdate: ModelsAuthorization.IPresenterUpdate,
    authorizationMode: Enums.AuthorizationMode,
    inputPhone: string,
    inputEmail: string,
    inputCode: string,
    validationErrors: Models.IAuthorizationValidationErrors,
    authorization: Models.IAuthorizationResult,
    authorizationPhase: Enums.ThunkChainPhase,
    authorizationOperationId: string | null,
    authorizationError: Models.IError | null,
    authorizationCachedDate: Date | null,
    authorizationConfirm: Models.IAuthorizationConfirmResult,
    authorizationConfirmPhase: Enums.ThunkChainPhase,
    authorizationConfirmOperationId: string | null,
    authorizationConfirmError: Models.IError | null,
    authorizationConfirmCachedDate: Date | null,
    containerExecute: Models.IAuthorizationContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IAuthorizationPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IAuthorizationContext,
    instanceId: string,
    instance: Models.IAuthorizationInstance,

    /* START - View Authorization additional props. */
    navigateBack: ModelsStartManager.INavigateBack,
    /* END - View Authorization additional props. */

    testID?: string | undefined,
    instanceType: Enums.ContainerAuthorizationInstanceType
}

const ViewAuthorization: React.FunctionComponent<IProps> = (props: IProps): React.ReactElement<View> =>
/* START - View Authorization content. */
(
    <>
        {renderContent(props)}
        <Modal type={'Error'}
               isVisible={props.authorizationError !== null || props.authorizationConfirmError !== null}
               onClose={() =>
                   props.authorizationError ? props.callPostAuthorizationReset('Default') :
                       props.authorizationConfirmError ? props.callPostAuthorizationConfirmReset('Default') :
                           null
               }
               message={
                   props.authorizationError ? props.authorizationError.message :
                       props.authorizationConfirmError ? props.authorizationConfirmError.message :
                           null
               }
        />
    </>
)
/* END - View Authorization content. */

export default ViewAuthorization
