/*
 * Created by Burnaev M.U.
 */

import React, {Component, ReactElement} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as thunkBonus from '../thunk/ThunkBonus'

import {
    /* START - Mobile platform components import. */

	View, Header, Button, Text, Localization, Image,

	/* END - Mobile platform components import. */
} from 'grassica-ui'

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

/* START - View Bonus additional imports and module code. */
import Styles from './styles/ViewBonusStyles'
import {SafeAreaView, ScrollView} from 'react-native'

const renderContent = (props: IProps): ReactElement<View> => {

	return (
			<ScrollView style={{backgroundColor: '#2F2F2F'}}>
				<View style={[Styles.header]}>
					<Image type={'BonusHeader'} />
				</View>
				<View style={Styles.content}>
					<View style = {Styles.backButtonContenier}>
						<Button title={''}
							type={'CircleArrowLeft'}
							isEnabled={true}
							onTap={() => props.navigateBack('Default')}
							icon={() => null} />
					</View>
					<View style = {[Styles.marginTop30, {alignItems: 'center'}]}>
						<Text type={'TextLargeWhiteBold'} title={Localization.t('grassica-app.bonus.articleTitle')}/>
					</View>
					<View style = {[Styles.marginTop30,Styles.textContent]}>
						<Text type={'TextMediumGreyLineHeight'} title={Localization.t('grassica-app.bonus.article')}/>
					</View>
					<View style={[Styles.marginTop30, {height: 190, marginBottom: 20}]}>
						<Image type={'AboutUsFirstFlex'}/>
					</View>
				</View>
			</ScrollView>
	)

}
/* END - View Bonus additional imports and module code. */

/*
 * UI stateless functional component presenter for 'Bonus' container.
 */

export interface IProps {
    performContainerExecute: ModelsBonus.IPerformContainerExecute,
    performContainerReset: ModelsBonus.IPerformContainerReset,
    performContainerRecover: ModelsBonus.IPerformContainerRecover,
    performContainerInit: ModelsBonus.IPerformContainerInit,
    performContainerRefresh: ModelsBonus.IPerformContainerRefresh,
    presenterUpdate: ModelsBonus.IPresenterUpdate,
    navigateBack: ModelsStartManager.INavigateBack,
    containerExecute: Models.IBonusContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IBonusPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IBonusContext,
    instanceId: string,
    instance: Models.IBonusInstance,

    /* START - View Bonus additional props. */

	/* END - View Bonus additional props. */

    testID?: string | undefined,
    instanceType: Enums.ContainerBonusInstanceType
}

const ViewBonus: React.FunctionComponent<IProps> = (props: IProps): React.ReactElement<View> =>
/* START - View Bonus content. */
	(
		renderContent(props)
	)
/* END - View Bonus content. */

export default ViewBonus
