/*
 * Created by Burnaev M.U.
 */

import React, {Component, ReactElement} from 'react'

import {connect} from 'react-redux'
import {bindActionCreators} from 'redux'
import * as thunkClaim from '../thunk/ThunkClaim'

import {
    /* START - Mobile platform components import. */

    View,
    Button,
    Header,
    Localization,
    Text,
    InputString,
    ScrollView,
    Modal,

    /* END - Mobile platform components import. */
} from 'grassica-ui'

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

/* START - View Claim additional imports and module code. */
import Styles from './styles/ViewClaimStyles'
import * as util from '../common/Util'

const renderContent = (props: IProps): ReactElement<View> => {
    return (
        <View style={Styles.main}>
            <Header type={'Default'}
                    getLeftContent={() => <Button title={''}
                                                  type={'ArrowLeft'}
                                                  isEnabled={true}
                                                  onTap={() => props.navigateBack('Default')}/>
                    }
                    getCenterContent={() => <Text title={Localization.t('grassica-app.sendingComments.headerTitle')}
                                                  type={'TextMediumBezh'}/>
                    }
            />
            <View style={Styles.containerHeaderLine}/>
            <ScrollView style={Styles.containerInputTextButton}>
                <View style={Styles.containerText}>
                    <Text type={'TextVeryLargeBezh'}
                          title={Localization.t('grassica-app.sendingComments.sendClaim')}
                    />
                </View>
                {
                    util.isAuthorized(props.userDetails) && !props.userDetails.email && props.isVisibleEmailPhone ?
                        <View style={Styles.containerEmail}>
                            <InputString type={'DefaultUnderlineGreen'}
                                         title={Localization.t('grassica-app.sendingComments.email')}
                                         isEnabled={true}
                                         value={props.inputEmail}
                                         onChange={(text: string | null) => text ? props.performInputEmail(props.instanceType, text) : null}
                                         isRequired={true}
                                         placeholder={''}
                                         error={props.validationErrors.inputEmailError ? props.validationErrors.inputEmailError : ''}
                            />
                        </View> : null
                }
                {
                    util.isAuthorized(props.userDetails) && !props.userDetails.phone && props.isVisibleEmailPhone ?
                        <View style={Styles.containerPhone}>
                            <InputString type={'DefaultUnderlineGrey'}
                                         title={''}
                                         isEnabled={true}
                                         value={props.inputPhone}
                                         onChange={(text: string | null) => props.performInputPhone(props.instanceType, text !== null ? text : '')}
                                         isRequired={true}
                                         placeholder={Localization.t('grassica-app.sendingComments.phone')}
                                         error={''}
                            />
                        </View> : null
                }
                <View style={[Styles.containerProduct, !props.isVisibleEmailPhone ? Styles.containerProductActive : null]}>
                    <InputString type={'DefaultUnderlineGrey'}
                                 title={''}
                                 isEnabled={true}
                                 value={props.inputProduct}
                                 onChange={(text: string | null) => props.performInputProduct(props.instanceType, text !== null ? text : '')}
                                 isRequired={true}
                                 placeholder={Localization.t('grassica-app.sendingComments.product')}
                                 onFocus={() => props.performEmailPhoneHide(props.instanceType)}
                                 onBlur={() => props.performEmailPhoneShow(props.instanceType)}
                                 error={''}
                    />
                </View>
                <View style={Styles.containerWhatToFix}>
                    <InputString title={Localization.t('grassica-app.sendingComments.whatToFix')}
                                 isEnabled={true}
                                 value={props.inputWhatToFix}
                                 type={'LargeHeight'}
                                 onChange={(text: string | null) => props.performInputWhatToFix(props.instanceType, text !== null ? text : '')}
                                 isRequired={true}
                                 error={props.validationErrors.inputWhatToFixError}
                                 placeholder={''}
                    />
                </View>
                <View style={Styles.containerButton}>
                    <Button title={Localization.t('grassica-app.sendingComments.sendButton')}
                            type={'ButtonGreen'}
                            isEnabled={true}
                            onTap={() => props.callPostClaim(props.instanceType)}
                    />
                </View>
            </ScrollView>
        </View>
    )
}
/* END - View Claim additional imports and module code. */

/*
 * UI stateless functional component presenter for 'Claim' container.
 */

export interface IProps {
    performValidate: ModelsClaim.IPerformValidate,
    performInputPhone: ModelsClaim.IPerformInputPhone,
    performInputEmail: ModelsClaim.IPerformInputEmail,
    performInputProduct: ModelsClaim.IPerformInputProduct,
    performInputWhatToFix: ModelsClaim.IPerformInputWhatToFix,
    callPostClaimReset: ModelsClaim.ICallPostClaimReset,
    callPostClaim: ModelsClaim.ICallPostClaim,
    performSentNoticeShow: ModelsClaim.IPerformSentNoticeShow,
    performSentNoticeHide: ModelsClaim.IPerformSentNoticeHide,
    performEmailPhoneShow: ModelsClaim.IPerformEmailPhoneShow,
    performEmailPhoneHide: ModelsClaim.IPerformEmailPhoneHide,
    performContainerExecute: ModelsClaim.IPerformContainerExecute,
    performContainerReset: ModelsClaim.IPerformContainerReset,
    performContainerRecover: ModelsClaim.IPerformContainerRecover,
    performContainerInit: ModelsClaim.IPerformContainerInit,
    performContainerRefresh: ModelsClaim.IPerformContainerRefresh,
    presenterUpdate: ModelsClaim.IPresenterUpdate,
    navigateBack: ModelsStartManager.INavigateBack,
    validationErrors: Models.IClaimValidationErrors,
    inputPhone: string,
    inputEmail: string,
    inputProduct: string,
    inputWhatToFix: string,
    isVisibleSentNotice: boolean,
    isVisibleEmailPhone: boolean,
    Claim: Models.IClaimResult,
    ClaimPhase: Enums.ThunkChainPhase,
    ClaimOperationId: string | null,
    ClaimError: Models.IError | null,
    ClaimCachedDate: Date | null,
    containerExecute: Models.IClaimContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IClaimPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IClaimContext,
    instanceId: string,
    instance: Models.IClaimInstance,

    /* START - View Claim additional props. */
    userDetails: Models.IUserDetailsResult,
    /* END - View Claim additional props. */

    testID?: string | undefined,
    instanceType: Enums.ContainerClaimInstanceType
}

const ViewClaim: React.FunctionComponent<IProps> = (props: IProps): React.ReactElement<View> =>
/* START - View Claim content. */
(
    <>
        <Modal type={'Success'}
               isVisible={props.isVisibleSentNotice}
               onClose={() => props.performSentNoticeHide('Default')}
               message={Localization.t('grassica-ui.modal.successMessage')}
        />
        {renderContent(props)}
    </>
)
/* END - View Claim content. */

export default ViewClaim
