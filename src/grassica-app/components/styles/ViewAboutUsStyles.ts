/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style AboutUs additional imports and module code. */

/* END - Style AboutUs additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View AboutUs styles. */
	main: {
		flex: 1,
		backgroundColor: '#2F2F2F',
	},
    containerHeaderLine: {
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1,
        marginHorizontal: 16,
    },
	whiteContainer: {
		paddingHorizontal: 16,
		backgroundColor: 'white',
		flex: 1,
	},
	imageStyle: {
		marginTop: -50,
		height: 190,
	}

	/* END - View AboutUs styles. */

})

export default Styles
