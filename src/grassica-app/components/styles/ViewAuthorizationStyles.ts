/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Authorization additional imports and module code. */
import {Platform} from 'grassica-ui'
/* END - Style Authorization additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Authorization styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingTop: 20,
        marginHorizontal: 16,
        marginBottom: 30,
        borderTopColor: '#4B4B4B',
        borderTopWidth: 1,
    },
    phoneTitle: {
        flexDirection: 'row',
        marginVertical: 12,
    },
    dot: {
        height: 6,
        width: 6,
        borderRadius: 6,
        backgroundColor: '#4B4B4B',
    },
    hiddenInput: {
        position: 'absolute',
        top: -500
    },
    containerCodeActivationText: {
        flexDirection: 'row',
        marginBottom: 6.14,
        marginTop: 17,
    },
    containerCodeActivation: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    containerCodeActivationCell: {
        height: 45.5,
        width: 71,
        justifyContent: 'center',
        alignItems: 'center',
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 3,
    },
    borderColorNumber: {
        borderBottomColor: '#FDFBF3',
    },
    borderColorDot: {
        borderBottomColor: '#4B4B4B',
    },
    buttonTopPhoneCode: {
        marginTop: 31.35,
        marginBottom: Platform.OS === 'ios' ? 120 : 0,
    },
    buttonTopEmail: {
        marginTop: 23,
        marginBottom: Platform.OS === 'ios' ? 120 : 0,
    },
    /* END - View Authorization styles. */

})

export default Styles
