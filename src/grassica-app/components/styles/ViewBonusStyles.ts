/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Bonus additional imports and module code. */

/* END - Style Bonus additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Bonus styles. */
    main: {
        flex: 1,
	},
	header:{
		width:'100%',
		height:222,
	},
	content:{
		backgroundColor:'#2F2F2F',
		flex:1,
		borderTopLeftRadius:20,
		borderTopRightRadius:20,
		marginTop:-20,
		paddingHorizontal:16,
	},
	marginTop30:{
		marginTop:30
	},
	backButtonContenier:{
		position:'absolute',
		left:0,
	},
	textContent:{
		width:'100%'
	},
    /* END - View Bonus styles. */

})

export default Styles
