/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Claim additional imports and module code. */

/* END - Style Claim additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Claim styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    containerHeaderLine: {
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1,
        marginHorizontal: 16,
    },
    containerInputTextButton: {
        flex: 1,
        marginHorizontal: 16,
    },
    containerText: {
        marginBottom: 26,
        marginTop: 15.5
    },
    containerEmail: {
        marginBottom: 24,
    },
    containerPhone: {
        marginBottom: 30,
    },
    containerProduct: {
        marginBottom: 11,
    },
    containerProductActive: {
        marginTop: 20,
    },
    containerWhatToFix: {
        marginBottom: 50,
    },
    containerButton: {
        marginBottom: 20,
    },
    /* END - View Claim styles. */

})

export default Styles
