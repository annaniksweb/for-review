/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Confirm additional imports and module code. */

/* END - Style Confirm additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Confirm styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    mainScroll: {
        backgroundColor: '#2F2F2F',
        flex: 1,
        paddingTop: 20,
        marginHorizontal: 16,
        marginBottom: 30,
        borderTopColor: '#4B4B4B',
        borderTopWidth: 1,
    },
    title: {
        marginTop: 15,
        marginBottom: 16,
    },
    comment: {
        marginTop: 16,
        marginBottom: 30,
    },

    input: {
        marginBottom: 45,
    },
    margin24: {
        marginTop: 24,
    }

    /* END - View Confirm styles. */

})

export default Styles
