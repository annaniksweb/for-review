/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style DataSecurity additional imports and module code. */

/* END - Style DataSecurity additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View DataSecurity styles. */
    main: {
   		flex: 1,
   	},
   	header: {
   		backgroundColor: '#2F2F2F',
   	},
   	content: {
   		flex: 1,
   		backgroundColor: '#E5E5E5',
   		paddingHorizontal: 16,
   	},
   	privacy: {
   		marginTop: 22.5,
   		alignItems: 'center',
   	},
   	privacyParagraph: {
   		marginTop: 22.5,
   	},
   	dataProtection: {
   		marginTop: 38,
   		alignItems: 'center',
   	},
   	dataProtectionParagraphOne: {
   		marginTop: 10,
   	},
   	dataProtectionParagraphTwo: {
   		marginTop: 24,
   	},
   	contactPerson: {
   		marginTop: 18,
   		alignItems: 'center',
   	},
   	contactParagraphPersonOne: {
   		marginTop: 14,
   	},
   	contactParagraphPersonTwo: {
   		marginTop: 20,
   		marginBottom: 24,
   	},
    /* END - View DataSecurity styles. */

})

export default Styles
