/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Main additional imports and module code. */

/* END - Style Main additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Main styles. */
    main: {
        flex: 1,
        flexDirection: 'column',
        opacity: 1,
        backgroundColor: 'white'
    },
    header: {
        height: 356,
        flexDirection: 'column'
    },
    appName: {
        fontSize: 22,
        letterSpacing: 10,
        color: '#51A535',
        fontWeight: '600',
    },
    expContenier: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    expText: {
        fontSize: 17,
        fontWeight: '500',
        color: 'white',
        marginRight: 2.85,

    },
    expIcon: {
        width: 26,
        height: 29
    },
    buttonsContainer: {
        marginHorizontal: 16
    },
    topButtonsGroup: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: -26
    },
    topButton: {
        width: 158,
    },
    horizontalScroll: {
        flex: 1,
        flexDirection: 'row',
        height: 300
    },
    mainButtonsGroup: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        flexWrap: 'wrap'
    },

    block: {
        marginTop: 15,
        borderRadius: 18,
    },
    blockContainer: {
        width: 280,
        height: 160,
        marginHorizontal: 15,
        alignItems: 'center',
        position: 'relative',
    },

    blockText: {
        fontSize: 17,
        color: 'black',
        width: 160,
        marginTop: 30,
        marginLeft: 15,
        height: 100,
    },
    textContactUs: {
        color: '#000',
        fontSize: 28,
        marginTop: 45,
        marginBottom: 10,
        fontWeight: 'bold',
    },
    menu: {
        flex: 1,
        backgroundColor: '#2F2F2F',
        paddingLeft: 16,
        paddingRight: 22,
        paddingTop: 104,
    },
    registrationButton: {
        marginBottom: 24,
    },
    menuLanguage: {
        marginTop: 12,
        marginBottom: 10,
    },
    menuBonusProgram: {
        marginTop: 17,
        marginBottom: 11,
    },
    menuBuyOnline: {
        marginBottom: 14,
    },
    menuFindStore: {
        marginBottom: 11,
    },
    menuAboutUs: {
        marginBottom: 16,
    },
    menuAsk: {
        marginTop: 17,
        marginBottom: 11,
    },
    menuNewIdea: {
        marginBottom: 12,
    },
    menuSendClaim: {
        marginBottom: 12,
    },
    menuWhatsApp: {
        marginTop: 12,
        marginBottom: 11,
    },
    menuFacebook: {
        marginBottom: 14,
    },
    menuInstagram: {
        marginBottom: 16,
    },
    menuSecurity: {
        marginTop: 14,
        marginBottom: 124,
    },

    menuItem: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    menuIcon: {
        width: 38,
        alignItems: 'center',
        marginRight: 4,
    },
    menuProfile: {
        height: 52,
        marginBottom: 23,
        alignItems: 'flex-start'
    },
    menuProfileTitle: {
        marginTop: 2,
    },
    menuProfileButton: {
        alignSelf: 'flex-start',
    },
    containerModalButtonClose: {
        marginTop: 12,
        alignItems: 'flex-end',
        marginRight: 12,
    },
    containerModalText: {
        marginTop: 30,
        marginHorizontal: 16,
        alignItems: 'center',
    },
    containerModalButtonInstall: {
        marginTop: 20,
        marginHorizontal: 12,
        marginBottom: 10,
    },
    menuOverlay: {
        position: 'absolute',
        zIndex: 1000,
        width: '100%',
        height: '100%',
    },
    modalContentChangeLanguage: {
        backgroundColor: '#424242',
    },
    modalContentTextButtonClose: {
        marginLeft: 29.5,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 23.4,
        marginRight: 20,
        marginTop: 20,
    },
    modalContentInputPicker: {
        marginBottom: 51.6,
    },
    modalContentButton: {
        marginBottom: 29.2,
        marginRight: 30.5,
        alignItems: 'flex-end',
    },
    /* END - View Main styles. */

})

export default Styles
