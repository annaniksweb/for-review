/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style MarketList additional imports and module code. */

/* END - Style MarketList additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View MarketList styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    mainScroll: {
        backgroundColor: '#2F2F2F',
        flex: 1,
        paddingTop: 20,
        marginHorizontal: 16,
        marginBottom: 30,
        borderTopColor: '#4B4B4B',
        borderTopWidth: 1,
    },
    marketCard: {
        height: 190.5,
        width: '100%',
        marginBottom: 42,
    },
    marketCardContainer: {
        paddingTop: 26,
        paddingHorizontal: 30,
    },
    marketCardTitle: {
        height: 72,
    },
    marketCardRow: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
    },
    marketCardIcon: {
        width: 17,
        marginRight: 13,
    },
    /* END - View MarketList styles. */

})

export default Styles
