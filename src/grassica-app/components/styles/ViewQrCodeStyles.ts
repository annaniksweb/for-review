/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style QrCode additional imports and module code. */

/* END - Style QrCode additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View QrCode styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    headerContainer: {
        height: 160,
    },
    shadow: {
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 12,
        },

        shadowOpacity: 0.1,
        shadowRadius: 5,

        elevation: 10,
    },
    content: {
        flex: 1,
        marginTop: 33,
        marginHorizontal: 16,
        marginBottom: 30,
    },
    qrContainer: {
        marginTop: 50,
        alignSelf: 'center',
        marginBottom: 31,
    },
    containerButton: {
        marginTop: 43,
        marginBottom: 20,
    },
    containerHeaderLine: {
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1,
        marginHorizontal: 16,
    },
    containerImage: {
   		flex: 1,
   	},
    imageStyle: {
   		height: 205,
   	},
    cardTextName: {
        marginBottom: 1,
        marginRight: 18,
   	},
    containerTextCard: {
        flex: 1,
        justifyContent: 'flex-end',
        marginLeft: 22,
        marginBottom: 19,
   	},
    cardNumber: {
        marginRight: 18,
   	}
    /* END - View QrCode styles. */

})

export default Styles
