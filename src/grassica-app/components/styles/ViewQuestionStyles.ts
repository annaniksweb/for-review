/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Question additional imports and module code. */

/* END - Style Question additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Question styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    containerHeader: {
        borderBottomColor: '#4B4B4B',
        borderBottomWidth: 1,
        marginHorizontal: 16,
    },
    containerInputTextButton: {
        flex: 1,
        paddingTop: 15.5,
        marginHorizontal: 16,
    },
    containerText: {
        marginBottom: 26,
    },
    containerEmail: {
        marginBottom: 24,
    },
    containerPhone: {
        marginBottom: 30,
    },
    containerProduct: {
        marginBottom: 11,
    },
    containerProductActive: {
        marginTop: 20,
    },
    containerWhatToFix: {
        marginBottom: 50,
    },
    /* END - View Question styles. */

})

export default Styles
