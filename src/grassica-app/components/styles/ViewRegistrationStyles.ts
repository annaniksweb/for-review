/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Registration additional imports and module code. */

/* END - Style Registration additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Registration styles. */
    main: {
        flex: 1,
        backgroundColor: '#2F2F2F',
    },
    mainScroll: {
        backgroundColor: '#2F2F2F',
        flex: 1,
        paddingTop: 20,
        marginHorizontal: 16,
        marginBottom: 30,
        borderTopColor: '#4B4B4B',
        borderTopWidth: 1,
    },
    margin16: {
        marginTop: 16,
    },
    margin10: {
        marginTop: 10,
    },
    /* END - View Registration styles. */

})

export default Styles
