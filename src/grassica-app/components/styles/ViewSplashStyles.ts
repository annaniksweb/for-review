/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style Splash additional imports and module code. */

/* END - Style Splash additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View Splash styles. */
    splashBg: {
        flex: 1,
        backgroundColor: 'rgba(47, 47, 47, 0.8)',
        justifyContent: 'center',
        flexDirection: 'column',
        alignItems: 'center'
    },
    splashSlogin: {
        fontSize: 46,
        fontWeight: 'bold',
        color: 'white',
        lineHeight: 58
    },
    splashAppName: {
        fontSize: 22,
        marginTop: 5,
        marginBottom: 15,
        letterSpacing: 10,
        color: '#51A535'
    }
    /* END - View Splash styles. */

})

export default Styles
