/*
 * Created by Burnaev M.U.
 */

import {StyleSheet} from 'react-native'

/* START - Style StartManager additional imports and module code. */

/* END - Style StartManager additional imports and module code. */

const Styles: any = StyleSheet.create({

    /* START - View StartManager styles. */
    transitioner: {
        flex: 1
    },

    /* END - View StartManager styles. */

})

export default Styles
