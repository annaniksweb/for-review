/*
 * Created by Burnaev M.U.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as thunkStartManager from '../thunk/ThunkStartManager'
import * as thunkSplash from '../thunk/ThunkSplash'
import * as thunkRegistration from '../thunk/ThunkRegistration'
import * as thunkAuthorization from '../thunk/ThunkAuthorization'
import * as thunkMain from '../thunk/ThunkMain'
import * as thunkAboutUs from '../thunk/ThunkAboutUs'
import * as thunkClaim from '../thunk/ThunkClaim'
import * as thunkMarketList from '../thunk/ThunkMarketList'
import * as thunkQrCode from '../thunk/ThunkQrCode'
import * as thunkBonus from '../thunk/ThunkBonus'
import * as thunkQuestion from '../thunk/ThunkQuestion'
import * as thunkIdea from '../thunk/ThunkIdea'
import * as thunkConfirm from '../thunk/ThunkConfirm'
import * as thunkProfile from '../thunk/ThunkProfile'
import * as thunkDataSecurity from '../thunk/ThunkDataSecurity'
import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

import ViewAboutUs from '../components/ViewAboutUs'
import * as util from '../common/Util'
import {IRootState} from 'RootState'
import {IAboutUsState} from 'grassica-app'

import {catchRender, RNLayoutAnimation} from 'grassica-ui'

/* START - Container AboutUs additional imports and module code. */

/* END - Container AboutUs additional imports and module code. */

/*
 * Container "AboutUs". About us screen
 */

class ContainerAboutUs extends React.PureComponent<IAboutUsProps> {

    constructor(props: IAboutUsProps, context: any) {
        super(props, context)
    }

    componentDidMount() {
        /* START - Container AboutUs did mount actions. */

        /* END - Container AboutUs did mount actions. */
    }

    componentWillUpdate() {
        util.performanceCheckStart('UPDATE: ContainerAppAboutUs')
    }

    componentDidUpdate() {
        util.performanceCheckStop('UPDATE: ContainerAppAboutUs', 1000)
    }

    @catchRender
    render() {
        RNLayoutAnimation.configureNext(RNLayoutAnimation.Presets.easeInEaseOut)
        util.performanceCheckStart('RENDER: ViewAboutUs')

        let result = (
            <ViewAboutUs
                performContainerExecute={this.props.performContainerExecute}
                performContainerReset={this.props.performContainerReset}
                performContainerRecover={this.props.performContainerRecover}
                performContainerInit={this.props.performContainerInit}
                performContainerRefresh={this.props.performContainerRefresh}
                presenterUpdate={this.props.presenterUpdate}
                navigateBack={this.props.navigateBack}
                containerExecute={this.props.containerExecute}
                containerExecutePhase={this.props.containerExecutePhase}
                containerExecuteOperationId={this.props.containerExecuteOperationId}
                containerExecuteError={this.props.containerExecuteError}
                presenter={this.props.presenter}
                presenterPhase={this.props.presenterPhase}
                presenterOperationId={this.props.presenterOperationId}
                presenterError={this.props.presenterError}
                context={this.props.context}
                instanceId={this.props.instanceId}
                instance={this.props.instance}

                /* START - View AboutUs additional props. */

                /* END - View AboutUs additional props. */

                testID={this.props.testID}
                instanceType={this.props.instanceType}>
            </ViewAboutUs>
        )

        util.performanceCheckStop('RENDER: ViewAboutUs', 100)

        return result
    }
}

export interface IOwnProps {
    testID?: string
    instanceType: Enums.ContainerAboutUsInstanceType
}

export interface IStateProps {
    containerExecute: Models.IAboutUsContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IAboutUsPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IAboutUsContext,
    instanceId: string,
    instance: Models.IAboutUsInstance,

    /* START - View AboutUs interface additional props. */

    /* END - View AboutUs interface additional props. */
}

export interface IDispatchProps {
    performContainerExecute: ModelsAboutUs.IPerformContainerExecute,
    performContainerReset: ModelsAboutUs.IPerformContainerReset,
    performContainerRecover: ModelsAboutUs.IPerformContainerRecover,
    performContainerInit: ModelsAboutUs.IPerformContainerInit,
    performContainerRefresh: ModelsAboutUs.IPerformContainerRefresh,
    presenterUpdate: ModelsAboutUs.IPresenterUpdate,
    navigateBack: ModelsStartManager.INavigateBack,

    /* START - Container AboutUs additional dispatch props. */

    /* END - Container AboutUs additional dispatch props. */
}

export type IAboutUsProps = IStateProps & IDispatchProps & IOwnProps

function mapStateToProps(state: IRootState, props: IOwnProps) {

    let reducerState = thunkAboutUs.getInstanceReducerState(state, props.instanceType)

    return {
        containerExecute: reducerState.containerExecute,
        containerExecutePhase: reducerState.containerExecutePhase,
        containerExecuteOperationId: reducerState.containerExecuteOperationId,
        containerExecuteError: reducerState.containerExecuteError,
        presenter: reducerState.presenter,
        presenterPhase: reducerState.presenterPhase,
        presenterOperationId: reducerState.presenterOperationId,
        presenterError: reducerState.presenterError,
        context: reducerState.context,
        instanceId: reducerState.instanceId,
        instance: reducerState.instance,

        /* START - Container AboutUs map state to props. */

        /* END - Container AboutUs map state to props. */
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        performContainerExecute: (instanceType: Enums.ContainerAboutUsInstanceType) => {
            dispatch(thunkAboutUs.performContainerExecute(instanceType))
        },
        performContainerReset: (instanceType: Enums.ContainerAboutUsInstanceType) => {
            dispatch(thunkAboutUs.performContainerReset(instanceType))
        },
        performContainerRecover: (instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState) => {
            dispatch(thunkAboutUs.performContainerRecover(instanceType, stateRecovering))
        },
        performContainerInit: (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => {
            dispatch(thunkAboutUs.performContainerInit(instanceType, context, instance, ))
        },
        performContainerRefresh: (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => {
            dispatch(thunkAboutUs.performContainerRefresh(instanceType, context, instance, ))
        },
        presenterUpdate: (instanceType: Enums.ContainerAboutUsInstanceType) => {
            dispatch(thunkAboutUs.presenterUpdate(instanceType))
        },
        navigateBack: (instanceType: Enums.ContainerStartManagerInstanceType, ) => {
            dispatch(thunkStartManager.navigateBack(instanceType, ))
        },

        /* START - Container AboutUs additional map dispatch props. */

        /* END - Container AboutUs additional map dispatch props. */
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerAboutUs)
