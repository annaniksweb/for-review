/*
 * Created by Burnaev M.U.
 */

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as thunkStartManager from '../thunk/ThunkStartManager'
import * as thunkSplash from '../thunk/ThunkSplash'
import * as thunkRegistration from '../thunk/ThunkRegistration'
import * as thunkAuthorization from '../thunk/ThunkAuthorization'
import * as thunkMain from '../thunk/ThunkMain'
import * as thunkAboutUs from '../thunk/ThunkAboutUs'
import * as thunkClaim from '../thunk/ThunkClaim'
import * as thunkMarketList from '../thunk/ThunkMarketList'
import * as thunkQrCode from '../thunk/ThunkQrCode'
import * as thunkBonus from '../thunk/ThunkBonus'
import * as thunkQuestion from '../thunk/ThunkQuestion'
import * as thunkIdea from '../thunk/ThunkIdea'
import * as thunkConfirm from '../thunk/ThunkConfirm'
import * as thunkProfile from '../thunk/ThunkProfile'
import * as thunkDataSecurity from '../thunk/ThunkDataSecurity'
import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

import ViewAuthorization from '../components/ViewAuthorization'
import * as util from '../common/Util'
import {IRootState} from 'RootState'
import {IAuthorizationState} from 'grassica-app'

import {catchRender, RNLayoutAnimation} from 'grassica-ui'

/* START - Container Authorization additional imports and module code. */

/* END - Container Authorization additional imports and module code. */

/*
 * Container "Authorization". Authorization screen
 */

class ContainerAuthorization extends React.PureComponent<IAuthorizationProps> {

    constructor(props: IAuthorizationProps, context: any) {
        super(props, context)
    }

    componentDidMount() {
        /* START - Container Authorization did mount actions. */

        /* END - Container Authorization did mount actions. */
    }

    componentWillUpdate() {
        util.performanceCheckStart('UPDATE: ContainerAppAuthorization')
    }

    componentDidUpdate() {
        util.performanceCheckStop('UPDATE: ContainerAppAuthorization', 1000)
    }

    @catchRender
    render() {
        RNLayoutAnimation.configureNext(RNLayoutAnimation.Presets.easeInEaseOut)
        util.performanceCheckStart('RENDER: ViewAuthorization')

        let result = (
            <ViewAuthorization
                performInputAuthorizationMode={this.props.performInputAuthorizationMode}
                performInputPhone={this.props.performInputPhone}
                performInputEmail={this.props.performInputEmail}
                performInputCode={this.props.performInputCode}
                performValidate={this.props.performValidate}
                callPostAuthorizationReset={this.props.callPostAuthorizationReset}
                callPostAuthorization={this.props.callPostAuthorization}
                callPostAuthorizationConfirmReset={this.props.callPostAuthorizationConfirmReset}
                callPostAuthorizationConfirm={this.props.callPostAuthorizationConfirm}
                performContainerExecute={this.props.performContainerExecute}
                performContainerReset={this.props.performContainerReset}
                performContainerRecover={this.props.performContainerRecover}
                performContainerInit={this.props.performContainerInit}
                performContainerRefresh={this.props.performContainerRefresh}
                presenterUpdate={this.props.presenterUpdate}
                authorizationMode={this.props.authorizationMode}
                inputPhone={this.props.inputPhone}
                inputEmail={this.props.inputEmail}
                inputCode={this.props.inputCode}
                validationErrors={this.props.validationErrors}
                authorization={this.props.authorization}
                authorizationPhase={this.props.authorizationPhase}
                authorizationOperationId={this.props.authorizationOperationId}
                authorizationError={this.props.authorizationError}
                authorizationCachedDate={this.props.authorizationCachedDate}
                authorizationConfirm={this.props.authorizationConfirm}
                authorizationConfirmPhase={this.props.authorizationConfirmPhase}
                authorizationConfirmOperationId={this.props.authorizationConfirmOperationId}
                authorizationConfirmError={this.props.authorizationConfirmError}
                authorizationConfirmCachedDate={this.props.authorizationConfirmCachedDate}
                containerExecute={this.props.containerExecute}
                containerExecutePhase={this.props.containerExecutePhase}
                containerExecuteOperationId={this.props.containerExecuteOperationId}
                containerExecuteError={this.props.containerExecuteError}
                presenter={this.props.presenter}
                presenterPhase={this.props.presenterPhase}
                presenterOperationId={this.props.presenterOperationId}
                presenterError={this.props.presenterError}
                context={this.props.context}
                instanceId={this.props.instanceId}
                instance={this.props.instance}

                /* START - View Authorization additional props. */
                navigateBack={this.props.navigateBack}
                /* END - View Authorization additional props. */

                testID={this.props.testID}
                instanceType={this.props.instanceType}>
            </ViewAuthorization>
        )

        util.performanceCheckStop('RENDER: ViewAuthorization', 100)

        return result
    }
}

export interface IOwnProps {
    testID?: string
    instanceType: Enums.ContainerAuthorizationInstanceType
}

export interface IStateProps {
    authorizationMode: Enums.AuthorizationMode,
    inputPhone: string,
    inputEmail: string,
    inputCode: string,
    validationErrors: Models.IAuthorizationValidationErrors,
    authorization: Models.IAuthorizationResult,
    authorizationPhase: Enums.ThunkChainPhase,
    authorizationOperationId: string | null,
    authorizationError: Models.IError | null,
    authorizationCachedDate: Date | null,
    authorizationConfirm: Models.IAuthorizationConfirmResult,
    authorizationConfirmPhase: Enums.ThunkChainPhase,
    authorizationConfirmOperationId: string | null,
    authorizationConfirmError: Models.IError | null,
    authorizationConfirmCachedDate: Date | null,
    containerExecute: Models.IAuthorizationContainerExecute,
    containerExecutePhase: Enums.ThunkChainPhase,
    containerExecuteOperationId: string | null,
    containerExecuteError: Models.IError | null,
    presenter: Models.IAuthorizationPresenter,
    presenterPhase: Enums.ThunkChainPhase,
    presenterOperationId: string | null,
    presenterError: Models.IError | null,
    context: Models.IAuthorizationContext,
    instanceId: string,
    instance: Models.IAuthorizationInstance,

    /* START - View Authorization interface additional props. */
    navigateBack: ModelsStartManager.INavigateBack,
    /* END - View Authorization interface additional props. */
}

export interface IDispatchProps {
    performInputAuthorizationMode: ModelsAuthorization.IPerformInputAuthorizationMode,
    performInputPhone: ModelsAuthorization.IPerformInputPhone,
    performInputEmail: ModelsAuthorization.IPerformInputEmail,
    performInputCode: ModelsAuthorization.IPerformInputCode,
    performValidate: ModelsAuthorization.IPerformValidate,
    callPostAuthorizationReset: ModelsAuthorization.ICallPostAuthorizationReset,
    callPostAuthorization: ModelsAuthorization.ICallPostAuthorization,
    callPostAuthorizationConfirmReset: ModelsAuthorization.ICallPostAuthorizationConfirmReset,
    callPostAuthorizationConfirm: ModelsAuthorization.ICallPostAuthorizationConfirm,
    performContainerExecute: ModelsAuthorization.IPerformContainerExecute,
    performContainerReset: ModelsAuthorization.IPerformContainerReset,
    performContainerRecover: ModelsAuthorization.IPerformContainerRecover,
    performContainerInit: ModelsAuthorization.IPerformContainerInit,
    performContainerRefresh: ModelsAuthorization.IPerformContainerRefresh,
    presenterUpdate: ModelsAuthorization.IPresenterUpdate,

    /* START - Container Authorization additional dispatch props. */

    /* END - Container Authorization additional dispatch props. */
}

export type IAuthorizationProps = IStateProps & IDispatchProps & IOwnProps

function mapStateToProps(state: IRootState, props: IOwnProps) {

    let reducerState = thunkAuthorization.getInstanceReducerState(state, props.instanceType)

    return {
        authorizationMode: reducerState.authorizationMode,
        inputPhone: reducerState.inputPhone,
        inputEmail: reducerState.inputEmail,
        inputCode: reducerState.inputCode,
        validationErrors: reducerState.validationErrors,
        authorization: reducerState.authorization,
        authorizationPhase: reducerState.authorizationPhase,
        authorizationOperationId: reducerState.authorizationOperationId,
        authorizationError: reducerState.authorizationError,
        authorizationCachedDate: reducerState.authorizationCachedDate,
        authorizationConfirm: reducerState.authorizationConfirm,
        authorizationConfirmPhase: reducerState.authorizationConfirmPhase,
        authorizationConfirmOperationId: reducerState.authorizationConfirmOperationId,
        authorizationConfirmError: reducerState.authorizationConfirmError,
        authorizationConfirmCachedDate: reducerState.authorizationConfirmCachedDate,
        containerExecute: reducerState.containerExecute,
        containerExecutePhase: reducerState.containerExecutePhase,
        containerExecuteOperationId: reducerState.containerExecuteOperationId,
        containerExecuteError: reducerState.containerExecuteError,
        presenter: reducerState.presenter,
        presenterPhase: reducerState.presenterPhase,
        presenterOperationId: reducerState.presenterOperationId,
        presenterError: reducerState.presenterError,
        context: reducerState.context,
        instanceId: reducerState.instanceId,
        instance: reducerState.instance,

        /* START - Container Authorization map state to props. */

        /* END - Container Authorization map state to props. */
    }
}

function mapDispatchToProps(dispatch: Function) {
    return {
        performInputAuthorizationMode: (instanceType: Enums.ContainerAuthorizationInstanceType, mode: Enums.AuthorizationMode) => {
            dispatch(thunkAuthorization.performInputAuthorizationMode(instanceType, mode))
        },
        performInputPhone: (instanceType: Enums.ContainerAuthorizationInstanceType, phone: string) => {
            dispatch(thunkAuthorization.performInputPhone(instanceType, phone))
        },
        performInputEmail: (instanceType: Enums.ContainerAuthorizationInstanceType, email: string) => {
            dispatch(thunkAuthorization.performInputEmail(instanceType, email))
        },
        performInputCode: (instanceType: Enums.ContainerAuthorizationInstanceType, code: string) => {
            dispatch(thunkAuthorization.performInputCode(instanceType, code))
        },
        performValidate: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => {
            dispatch(thunkAuthorization.performValidate(instanceType, ))
        },
        callPostAuthorizationReset: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => {
            dispatch(thunkAuthorization.callPostAuthorizationReset(instanceType, ))
        },
        callPostAuthorization: (instanceType: Enums.ContainerAuthorizationInstanceType) => {
            dispatch(thunkAuthorization.callPostAuthorization(instanceType))
        },
        callPostAuthorizationConfirmReset: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => {
            dispatch(thunkAuthorization.callPostAuthorizationConfirmReset(instanceType, ))
        },
        callPostAuthorizationConfirm: (instanceType: Enums.ContainerAuthorizationInstanceType) => {
            dispatch(thunkAuthorization.callPostAuthorizationConfirm(instanceType))
        },
        performContainerExecute: (instanceType: Enums.ContainerAuthorizationInstanceType) => {
            dispatch(thunkAuthorization.performContainerExecute(instanceType))
        },
        performContainerReset: (instanceType: Enums.ContainerAuthorizationInstanceType) => {
            dispatch(thunkAuthorization.performContainerReset(instanceType))
        },
        performContainerRecover: (instanceType: Enums.ContainerAuthorizationInstanceType, stateRecovering: IAuthorizationState) => {
            dispatch(thunkAuthorization.performContainerRecover(instanceType, stateRecovering))
        },
        performContainerInit: (instanceType: Enums.ContainerAuthorizationInstanceType, context: Models.IAuthorizationContext, instance: Models.IAuthorizationInstance, ) => {
            dispatch(thunkAuthorization.performContainerInit(instanceType, context, instance, ))
        },
        performContainerRefresh: (instanceType: Enums.ContainerAuthorizationInstanceType, context: Models.IAuthorizationContext, instance: Models.IAuthorizationInstance, ) => {
            dispatch(thunkAuthorization.performContainerRefresh(instanceType, context, instance, ))
        },
        presenterUpdate: (instanceType: Enums.ContainerAuthorizationInstanceType) => {
            dispatch(thunkAuthorization.presenterUpdate(instanceType))
        },

        /* START - Container Authorization additional map dispatch props. */
        navigateBack: (instanceType: Enums.ContainerStartManagerInstanceType, ) => {
            dispatch(thunkStartManager.navigateBack(instanceType, ))
        },
        /* END - Container Authorization additional map dispatch props. */
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContainerAuthorization)
