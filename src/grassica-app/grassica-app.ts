///<reference path='./index.ts' />

declare module 'grassica-app' {
    import React from 'react'
    import {Action} from 'redux-actions'
    import {Reducer} from 'redux'
    /* START - Additional imports for mobile application. */

    /* END - Additional imports for mobile application. */

    export namespace Enums {
        export type AppStatusType = (
            /* START - AppStatusType enum used in actions and thunks. */
            'Active' | 'Background' | 'Inactive'
            /* END - AppStatusType enum used in actions and thunks. */
        )
        export type SplashAnimationPhase = (
            /* START - SplashAnimationPhase enum used in actions and thunks. */
            'First' | 'Second' | 'Third' | 'Fourth'
            /* END - SplashAnimationPhase enum used in actions and thunks. */
        )
        export type AuthorizationMode = (
            /* START - AuthorizationMode enum used in actions and thunks. */
            'Email' | 'Phone'
            /* END - AuthorizationMode enum used in actions and thunks. */
        )
        export type CredentialsChangePhase = (
            /* START - CredentialsChangePhase enum used in actions and thunks. */
            'Never' |
            'EmailChange' |
            'EmailChangeCodeRequested' |
            'PhoneChange' |
            'PhoneChangeCodeRequested'
            /* END - CredentialsChangePhase enum used in actions and thunks. */
        )

        export type CacheContext = (
            /* START - Cache context. */
            'Undefined'
            /* END - Cache context. */
        )

        export type ThunkChainPhase = (
            'Never' | 'InProgress' | 'Success' | 'Failure'
        )

        export type CachePolicy = (
            'Disabled' | 'Default' | 'DebugOnly'
        )

        export type ErrorType = (
            'None' | 'Unknown' | 'JsonParserError' | 'JsonConverterError' | 'AuthorizationError' | 'ResponseConverterError' | 'RequestConverterError'
            /* START - Error types. */
            | 'AuthorizationInvalidCredentialsError'
            | 'LocalError'
            | 'ServerError'
            /* END - Error types. */
        )
        // Container StartManager interfaces.
        export type ContainerStartManagerInstanceType = (
            'Default'
            /* START - Container StartManager instance types. */
            /* END - Container StartManager instance types. */
        )
        export type ContainerStartManagerRouteType = (
            'StartManager'
            /* START - Container StartManager route types. */
            | 'Authorization'
            | 'Main'
            | 'MarketList'
            | 'Registration'
            | 'Splash'
            | 'AboutUs'
            | 'Claim'
            | 'QrCode'
            | 'Confirm'
            | 'Profile'
            | 'Idea'
            | 'Question'
            | 'Bonus'
            | 'DataSecurity'
            /* END - Container StartManager route types. */
        )
        // Container Splash interfaces.
        export type ContainerSplashInstanceType = (
            'Default'
            /* START - Container Splash instance types. */
            /* END - Container Splash instance types. */
        )
        export type ContainerSplashRouteType = (
            'Splash'
            /* START - Container Splash route types. */
            /* END - Container Splash route types. */
        )
        // Container Registration interfaces.
        export type ContainerRegistrationInstanceType = (
            'Default'
            /* START - Container Registration instance types. */
            /* END - Container Registration instance types. */
        )
        export type ContainerRegistrationRouteType = (
            'Registration'
            /* START - Container Registration route types. */
            /* END - Container Registration route types. */
        )
        // Container Authorization interfaces.
        export type ContainerAuthorizationInstanceType = (
            'Default'
            /* START - Container Authorization instance types. */
            /* END - Container Authorization instance types. */
        )
        export type ContainerAuthorizationRouteType = (
            'Authorization'
            /* START - Container Authorization route types. */
            /* END - Container Authorization route types. */
        )
        // Container Main interfaces.
        export type ContainerMainInstanceType = (
            'Default'
            /* START - Container Main instance types. */
            /* END - Container Main instance types. */
        )
        export type ContainerMainRouteType = (
            'Main'
            /* START - Container Main route types. */
            /* END - Container Main route types. */
        )
        // Container AboutUs interfaces.
        export type ContainerAboutUsInstanceType = (
            'Default'
            /* START - Container AboutUs instance types. */
            /* END - Container AboutUs instance types. */
        )
        export type ContainerAboutUsRouteType = (
            'AboutUs'
            /* START - Container AboutUs route types. */
            /* END - Container AboutUs route types. */
        )
        // Container Claim interfaces.
        export type ContainerClaimInstanceType = (
            'Default'
            /* START - Container Claim instance types. */
            /* END - Container Claim instance types. */
        )
        export type ContainerClaimRouteType = (
            'Claim'
            /* START - Container Claim route types. */
            /* END - Container Claim route types. */
        )
        // Container MarketList interfaces.
        export type ContainerMarketListInstanceType = (
            'Default'
            /* START - Container MarketList instance types. */
            /* END - Container MarketList instance types. */
        )
        export type ContainerMarketListRouteType = (
            'MarketList'
            /* START - Container MarketList route types. */
            /* END - Container MarketList route types. */
        )
        // Container QrCode interfaces.
        export type ContainerQrCodeInstanceType = (
            'Default'
            /* START - Container QrCode instance types. */
            /* END - Container QrCode instance types. */
        )
        export type ContainerQrCodeRouteType = (
            'QrCode'
            /* START - Container QrCode route types. */
            /* END - Container QrCode route types. */
        )
        // Container Bonus interfaces.
        export type ContainerBonusInstanceType = (
            'Default'
            /* START - Container Bonus instance types. */
            /* END - Container Bonus instance types. */
        )
        export type ContainerBonusRouteType = (
            'Bonus'
            /* START - Container Bonus route types. */
            /* END - Container Bonus route types. */
        )
        // Container Question interfaces.
        export type ContainerQuestionInstanceType = (
            'Default'
            /* START - Container Question instance types. */
            /* END - Container Question instance types. */
        )
        export type ContainerQuestionRouteType = (
            'Question'
            /* START - Container Question route types. */
            /* END - Container Question route types. */
        )
        // Container Idea interfaces.
        export type ContainerIdeaInstanceType = (
            'Default'
            /* START - Container Idea instance types. */
            /* END - Container Idea instance types. */
        )
        export type ContainerIdeaRouteType = (
            'Idea'
            /* START - Container Idea route types. */
            /* END - Container Idea route types. */
        )
        // Container Confirm interfaces.
        export type ContainerConfirmInstanceType = (
            'Default'
            /* START - Container Confirm instance types. */
            /* END - Container Confirm instance types. */
        )
        export type ContainerConfirmRouteType = (
            'Confirm'
            /* START - Container Confirm route types. */
            /* END - Container Confirm route types. */
        )
        // Container Profile interfaces.
        export type ContainerProfileInstanceType = (
            'Default'
            /* START - Container Profile instance types. */
            /* END - Container Profile instance types. */
        )
        export type ContainerProfileRouteType = (
            'Profile'
            /* START - Container Profile route types. */
            /* END - Container Profile route types. */
        )
        // Container DataSecurity interfaces.
        export type ContainerDataSecurityInstanceType = (
            'Default'
            /* START - Container DataSecurity instance types. */
            /* END - Container DataSecurity instance types. */
        )
        export type ContainerDataSecurityRouteType = (
            'DataSecurity'
            /* START - Container DataSecurity route types. */
            /* END - Container DataSecurity route types. */
        )

        /* START - Describe additional enums used mobile application. */
        /* END - Describe additional enums used mobile application. */
    }
    export class ContainerStartManager extends React.Component<any, any> {}
    export class ContainerSplash extends React.Component<any, any> {}
    export class ContainerRegistration extends React.Component<any, any> {}
    export class ContainerAuthorization extends React.Component<any, any> {}
    export class ContainerMain extends React.Component<any, any> {}
    export class ContainerAboutUs extends React.Component<any, any> {}
    export class ContainerClaim extends React.Component<any, any> {}
    export class ContainerMarketList extends React.Component<any, any> {}
    export class ContainerQrCode extends React.Component<any, any> {}
    export class ContainerBonus extends React.Component<any, any> {}
    export class ContainerQuestion extends React.Component<any, any> {}
    export class ContainerIdea extends React.Component<any, any> {}
    export class ContainerConfirm extends React.Component<any, any> {}
    export class ContainerProfile extends React.Component<any, any> {}
    export class ContainerDataSecurity extends React.Component<any, any> {}

    export namespace Models {
        export interface IStartManagerContext {
            /* START - IStartManagerContext model used in actions and thunks. */
            // TODO Describe IStartManagerContext model used in actions and thunks.
            [key: string]: string,
            /* END - IStartManagerContext model used in actions and thunks. */
        }
        export interface IStartManagerInstance {
            /* START - IStartManagerInstance model used in actions and thunks. */
            // TODO Describe IStartManagerInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IStartManagerInstance model used in actions and thunks. */
        }
        export interface IUserDetailsResult {
            /* START - IUserDetailsResult model used in actions and thunks. */
            id: number | null,
            bonusAmount: number | null,
            ufLoyaltyId: string,
            phone: string,
            lastName: string,
            email: string,
            name: string,
            language: string,
            /* END - IUserDetailsResult model used in actions and thunks. */
        }
        export interface IRequestUserDetails {
            /* START - IRequestUserDetails model used in actions and thunks. */
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestUserDetails model used in actions and thunks. */
        }
        export interface ICredentials {
            /* START - ICredentials model used in actions and thunks. */
            token: string | null,
            /* END - ICredentials model used in actions and thunks. */
        }
        export interface ILogoutResult {
            /* START - ILogoutResult model used in actions and thunks. */
            success: boolean,
            /* END - ILogoutResult model used in actions and thunks. */
        }
        export interface IRequestLogout {
            /* START - IRequestLogout model used in actions and thunks. */
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestLogout model used in actions and thunks. */
        }
        export interface IVersionResult {
            /* START - IVersionResult model used in actions and thunks. */
            version: string,
            appStoreUrl: string,
            googlePlayUrl: string,
            instagramUrl: string,
            facebookUrl: string,
            whatsAppUrl: string,
            /* END - IVersionResult model used in actions and thunks. */
        }
        export interface IRequestVersion {
            /* START - IRequestVersion model used in actions and thunks. */
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestVersion model used in actions and thunks. */
        }
        export interface IRequestLog {
            /* START - IRequestLog model used in actions and thunks. */
            logError: string,
            /* END - IRequestLog model used in actions and thunks. */
        }
        export interface IStartManagerContainerExecute {
            /* START - IStartManagerContainerExecute model used in actions and thunks. */
            // TODO Describe IStartManagerContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IStartManagerContainerExecute model used in actions and thunks. */
        }
        export interface IStartManagerPresenter {
            /* START - IStartManagerPresenter model used in actions and thunks. */
            language: string,
            /* END - IStartManagerPresenter model used in actions and thunks. */
        }
        export interface ISplashContext {
            /* START - ISplashContext model used in actions and thunks. */
            // TODO Describe ISplashContext model used in actions and thunks.
            [key: string]: string,
            /* END - ISplashContext model used in actions and thunks. */
        }
        export interface ISplashInstance {
            /* START - ISplashInstance model used in actions and thunks. */
            // TODO Describe ISplashInstance model used in actions and thunks.
            [key: string]: string,
            /* END - ISplashInstance model used in actions and thunks. */
        }
        export interface ISplashContainerExecute {
            /* START - ISplashContainerExecute model used in actions and thunks. */
            // TODO Describe ISplashContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - ISplashContainerExecute model used in actions and thunks. */
        }
        export interface ISplashPresenter {
            /* START - ISplashPresenter model used in actions and thunks. */
            // TODO Describe ISplashPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - ISplashPresenter model used in actions and thunks. */
        }
        export interface IRegistrationValidationErrors {
            /* START - IRegistrationValidationErrors model used in actions and thunks. */
            inputNameError: string | null,
            inputLastNameError: string | null,
            inputPhoneError: string | null,
            inputEmailError: string | null,
            inputLanguageError: string | null,
            inputPasswordError: string | null,
            inputPasswordConfirmError: string | null,
            /* END - IRegistrationValidationErrors model used in actions and thunks. */
        }
        export interface IRegistrationContext {
            /* START - IRegistrationContext model used in actions and thunks. */
            // TODO Describe IRegistrationContext model used in actions and thunks.
            [key: string]: string,
            /* END - IRegistrationContext model used in actions and thunks. */
        }
        export interface IRegistrationInstance {
            /* START - IRegistrationInstance model used in actions and thunks. */
            // TODO Describe IRegistrationInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IRegistrationInstance model used in actions and thunks. */
        }
        export interface IRegistrationConfirmResult {
            /* START - IRegistrationConfirmResult model used in actions and thunks. */
            success: boolean,
            /* END - IRegistrationConfirmResult model used in actions and thunks. */
        }
        export interface IRequestRegistrationConfirm {
            /* START - IRequestRegistrationConfirm model used in actions and thunks. */
            phone: string | null,
            email: string | null,
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestRegistrationConfirm model used in actions and thunks. */
        }
        export interface IRegistrationResult {
            /* START - IRegistrationResult model used in actions and thunks. */
            success: boolean
            /* END - IRegistrationResult model used in actions and thunks. */
        }
        export interface IRequestRegistration {
            /* START - IRequestRegistration model used in actions and thunks. */
            token: string | null,
            tokenApi: string | null,
            name: string | null,
            lastName: string | null,
            phone: string | null,
            email: string | null,
            userData: IUserData | null,
            language: string | null,
            code: string | null,
            /* END - IRequestRegistration model used in actions and thunks. */
        }
        export interface IRegistrationContainerExecute {
            /* START - IRegistrationContainerExecute model used in actions and thunks. */
            // TODO Describe IRegistrationContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IRegistrationContainerExecute model used in actions and thunks. */
        }
        export interface IRegistrationPresenter {
            /* START - IRegistrationPresenter model used in actions and thunks. */
            confirmSeconds: number | null,
            /* END - IRegistrationPresenter model used in actions and thunks. */
        }
        export interface IAuthorizationValidationErrors {
            /* START - IAuthorizationValidationErrors model used in actions and thunks. */
            errorPhone: string | null,
            errorCode: string | null,
            errorEmail: string | null,
            /* END - IAuthorizationValidationErrors model used in actions and thunks. */
        }
        export interface IAuthorizationContext {
            /* START - IAuthorizationContext model used in actions and thunks. */
            // TODO Describe IAuthorizationContext model used in actions and thunks.
            [key: string]: string,
            /* END - IAuthorizationContext model used in actions and thunks. */
        }
        export interface IAuthorizationInstance {
            /* START - IAuthorizationInstance model used in actions and thunks. */
            mode: Enums.AuthorizationMode | null,
            /* END - IAuthorizationInstance model used in actions and thunks. */
        }
        export interface IAuthorizationResult {
            /* START - IAuthorizationResult model used in actions and thunks. */
            // TODO Describe IAuthorizationResult model used in actions and thunks.
            success: boolean,
            /* END - IAuthorizationResult model used in actions and thunks. */
        }
        export interface IRequestAuthorization {
            /* START - IRequestAuthorization model used in actions and thunks. */
            phone: string | null,
            email: string | null,
            token: string | null,
            tokenApi: string | null ,
            /* END - IRequestAuthorization model used in actions and thunks. */
        }
        export interface IAuthorizationConfirmResult {
            /* START - IAuthorizationConfirmResult model used in actions and thunks. */
            token: string | null,
            userData: IUserData | null,
            /* END - IAuthorizationConfirmResult model used in actions and thunks. */
        }
        export interface IRequestAuthorizationConfirm {
            /* START - IRequestAuthorizationConfirm model used in actions and thunks. */
            code: string,
            phone: string | null,
            email: string | null,
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestAuthorizationConfirm model used in actions and thunks. */
        }
        export interface IAuthorizationContainerExecute {
            /* START - IAuthorizationContainerExecute model used in actions and thunks. */
            // TODO Describe IAuthorizationContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IAuthorizationContainerExecute model used in actions and thunks. */
        }
        export interface IAuthorizationPresenter {
            /* START - IAuthorizationPresenter model used in actions and thunks. */
            // TODO Describe IAuthorizationPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IAuthorizationPresenter model used in actions and thunks. */
        }
        export interface IMainContext {
            /* START - IMainContext model used in actions and thunks. */
            // TODO Describe IMainContext model used in actions and thunks.
            [key: string]: string,
            /* END - IMainContext model used in actions and thunks. */
        }
        export interface IMainInstance {
            /* START - IMainInstance model used in actions and thunks. */
            // TODO Describe IMainInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IMainInstance model used in actions and thunks. */
        }
        export interface IMainContainerExecute {
            /* START - IMainContainerExecute model used in actions and thunks. */
            // TODO Describe IMainContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IMainContainerExecute model used in actions and thunks. */
        }
        export interface IMainPresenter {
            /* START - IMainPresenter model used in actions and thunks. */
            // TODO Describe IMainPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IMainPresenter model used in actions and thunks. */
        }
        export interface IAboutUsContext {
            /* START - IAboutUsContext model used in actions and thunks. */
            // TODO Describe IAboutUsContext model used in actions and thunks.
            [key: string]: string,
            /* END - IAboutUsContext model used in actions and thunks. */
        }
        export interface IAboutUsInstance {
            /* START - IAboutUsInstance model used in actions and thunks. */
            // TODO Describe IAboutUsInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IAboutUsInstance model used in actions and thunks. */
        }
        export interface IAboutUsContainerExecute {
            /* START - IAboutUsContainerExecute model used in actions and thunks. */
            // TODO Describe IAboutUsContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IAboutUsContainerExecute model used in actions and thunks. */
        }
        export interface IAboutUsPresenter {
            /* START - IAboutUsPresenter model used in actions and thunks. */
            // TODO Describe IAboutUsPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IAboutUsPresenter model used in actions and thunks. */
        }
        export interface IClaimValidationErrors {
            /* START - IClaimValidationErrors model used in actions and thunks. */
            inputPhoneError: string | null,
            inputEmailError: string | null,
            inputProductError: string | null,
            inputWhatToFixError: string | null,
            /* END - IClaimValidationErrors model used in actions and thunks. */
        }
        export interface IClaimContext {
            /* START - IClaimContext model used in actions and thunks. */
            // TODO Describe IClaimContext model used in actions and thunks.
            [key: string]: string,
            /* END - IClaimContext model used in actions and thunks. */
        }
        export interface IClaimInstance {
            /* START - IClaimInstance model used in actions and thunks. */
            // TODO Describe IClaimInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IClaimInstance model used in actions and thunks. */
        }
        export interface IClaimResult {
            /* START - IClaimResult model used in actions and thunks. */
            success: boolean,
            /* END - IClaimResult model used in actions and thunks. */
        }
        export interface IRequestClaim {
            /* START - IRequestClaim model used in actions and thunks. */
            tokenApi: string | null,
            phone: string | null,
            message: string,
            producteName: string | null,
            email: string,
            token: string | null,
            /* END - IRequestClaim model used in actions and thunks. */
        }
        export interface IClaimContainerExecute {
            /* START - IClaimContainerExecute model used in actions and thunks. */
            // TODO Describe IClaimContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IClaimContainerExecute model used in actions and thunks. */
        }
        export interface IClaimPresenter {
            /* START - IClaimPresenter model used in actions and thunks. */
            // TODO Describe IClaimPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IClaimPresenter model used in actions and thunks. */
        }
        export interface IMarket {
            /* START - IMarket model used in actions and thunks. */
            address: string,
            email: string,
            phone: string,
            coordinates: ICoordinates | null,
            /* END - IMarket model used in actions and thunks. */
        }
        export interface IMarketListContext {
            /* START - IMarketListContext model used in actions and thunks. */
            // TODO Describe IMarketListContext model used in actions and thunks.
            [key: string]: string,
            /* END - IMarketListContext model used in actions and thunks. */
        }
        export interface IMarketListInstance {
            /* START - IMarketListInstance model used in actions and thunks. */
            // TODO Describe IMarketListInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IMarketListInstance model used in actions and thunks. */
        }
        export interface IMarketList {
            /* START - IMarketList model used in actions and thunks. */
            data: Models.IMarket[] | null
            /* END - IMarketList model used in actions and thunks. */
        }
        export interface IRequestMarketList {
            /* START - IRequestMarketList model used in actions and thunks. */
            language: string,
            /* END - IRequestMarketList model used in actions and thunks. */
        }
        export interface IGeolocation {
            /* START - IGeolocation model used in actions and thunks. */
            data: Models.IMarket[] | null
            /* END - IGeolocation model used in actions and thunks. */
        }
        export interface IMarketListContainerExecute {
            /* START - IMarketListContainerExecute model used in actions and thunks. */
            // TODO Describe IMarketListContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IMarketListContainerExecute model used in actions and thunks. */
        }
        export interface IMarketListPresenter {
            /* START - IMarketListPresenter model used in actions and thunks. */
            // TODO Describe IMarketListPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IMarketListPresenter model used in actions and thunks. */
        }
        export interface IQrCodeContext {
            /* START - IQrCodeContext model used in actions and thunks. */
            // TODO Describe IQrCodeContext model used in actions and thunks.
            [key: string]: string,
            /* END - IQrCodeContext model used in actions and thunks. */
        }
        export interface IQrCodeInstance {
            /* START - IQrCodeInstance model used in actions and thunks. */
            // TODO Describe IQrCodeInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IQrCodeInstance model used in actions and thunks. */
        }
        export interface IQrCodeContainerExecute {
            /* START - IQrCodeContainerExecute model used in actions and thunks. */
            // TODO Describe IQrCodeContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IQrCodeContainerExecute model used in actions and thunks. */
        }
        export interface IQrCodePresenter {
            /* START - IQrCodePresenter model used in actions and thunks. */
            // TODO Describe IQrCodePresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IQrCodePresenter model used in actions and thunks. */
        }
        export interface IBonusContext {
            /* START - IBonusContext model used in actions and thunks. */
            // TODO Describe IBonusContext model used in actions and thunks.
            [key: string]: string,
            /* END - IBonusContext model used in actions and thunks. */
        }
        export interface IBonusInstance {
            /* START - IBonusInstance model used in actions and thunks. */
            // TODO Describe IBonusInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IBonusInstance model used in actions and thunks. */
        }
        export interface IBonusContainerExecute {
            /* START - IBonusContainerExecute model used in actions and thunks. */
            // TODO Describe IBonusContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IBonusContainerExecute model used in actions and thunks. */
        }
        export interface IBonusPresenter {
            /* START - IBonusPresenter model used in actions and thunks. */
            // TODO Describe IBonusPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IBonusPresenter model used in actions and thunks. */
        }
        export interface IQuestionValidationErrors {
            /* START - IQuestionValidationErrors model used in actions and thunks. */
            inputEmailError: string | null,
            inputQuestionError: string | null,
            /* END - IQuestionValidationErrors model used in actions and thunks. */
        }
        export interface IQuestionContext {
            /* START - IQuestionContext model used in actions and thunks. */
            // TODO Describe IQuestionContext model used in actions and thunks.
            [key: string]: string,
            /* END - IQuestionContext model used in actions and thunks. */
        }
        export interface IQuestionInstance {
            /* START - IQuestionInstance model used in actions and thunks. */
            // TODO Describe IQuestionInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IQuestionInstance model used in actions and thunks. */
        }
        export interface IQuestionResult {
            /* START - IQuestionResult model used in actions and thunks. */
            success: boolean,
            /* END - IQuestionResult model used in actions and thunks. */
        }
        export interface IRequestQuestion {
            /* START - IRequestQuestion model used in actions and thunks. */
            tokenApi: string | null,
            phone: string | null,
            message: string,
            email: string,
            token: string | null,
            /* END - IRequestQuestion model used in actions and thunks. */
        }
        export interface IQuestionContainerExecute {
            /* START - IQuestionContainerExecute model used in actions and thunks. */
            // TODO Describe IQuestionContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IQuestionContainerExecute model used in actions and thunks. */
        }
        export interface IQuestionPresenter {
            /* START - IQuestionPresenter model used in actions and thunks. */
            // TODO Describe IQuestionPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IQuestionPresenter model used in actions and thunks. */
        }
        export interface IIdeaValidationErrors {
            /* START - IIdeaValidationErrors model used in actions and thunks. */
            inputEmailError: string | null,
            inputIdeaError: string | null,
            /* END - IIdeaValidationErrors model used in actions and thunks. */
        }
        export interface IIdeaContext {
            /* START - IIdeaContext model used in actions and thunks. */
            // TODO Describe IIdeaContext model used in actions and thunks.
            [key: string]: string,
            /* END - IIdeaContext model used in actions and thunks. */
        }
        export interface IIdeaInstance {
            /* START - IIdeaInstance model used in actions and thunks. */
            // TODO Describe IIdeaInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IIdeaInstance model used in actions and thunks. */
        }
        export interface IIdeaResult {
            /* START - IIdeaResult model used in actions and thunks. */
            success: boolean,
            /* END - IIdeaResult model used in actions and thunks. */
        }
        export interface IRequestIdea {
            /* START - IRequestIdea model used in actions and thunks. */
            tokenApi: string | null,
            phone: string | null,
            message: string,
            email: string,
            token: string | null,
            /* END - IRequestIdea model used in actions and thunks. */
        }
        export interface IIdeaContainerExecute {
            /* START - IIdeaContainerExecute model used in actions and thunks. */
            // TODO Describe IIdeaContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IIdeaContainerExecute model used in actions and thunks. */
        }
        export interface IIdeaPresenter {
            /* START - IIdeaPresenter model used in actions and thunks. */
            // TODO Describe IIdeaPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IIdeaPresenter model used in actions and thunks. */
        }
        export interface IConfirmValidationErrors {
            /* START - IConfirmValidationErrors model used in actions and thunks. */
            inputCodeError: string | null,
            /* END - IConfirmValidationErrors model used in actions and thunks. */
        }
        export interface IConfirmContext {
            /* START - IConfirmContext model used in actions and thunks. */
            // TODO Describe IConfirmContext model used in actions and thunks.
            [key: string]: string,
            /* END - IConfirmContext model used in actions and thunks. */
        }
        export interface IConfirmInstance {
            /* START - IConfirmInstance model used in actions and thunks. */
            email: string | null,
            phone: string | null,
            /* END - IConfirmInstance model used in actions and thunks. */
        }
        export interface IConfirmResult {
            /* START - IConfirmResult model used in actions and thunks. */
            success: boolean,
            /* END - IConfirmResult model used in actions and thunks. */
        }
        export interface IRequestConfirm {
            /* START - IRequestConfirm model used in actions and thunks. */
            tokenApi: string | null,
            token: string | null,
            email: string | null,
            phone: string | null,
            code: string,
            /* END - IRequestConfirm model used in actions and thunks. */
        }
        export interface IConfirmContainerExecute {
            /* START - IConfirmContainerExecute model used in actions and thunks. */
            // TODO Describe IConfirmContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IConfirmContainerExecute model used in actions and thunks. */
        }
        export interface IConfirmPresenter {
            /* START - IConfirmPresenter model used in actions and thunks. */
            // TODO Describe IConfirmPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IConfirmPresenter model used in actions and thunks. */
        }
        export interface IProfileValidationErrors {
            /* START - IProfileValidationErrors model used in actions and thunks. */
            inputNameError: string | null,
            inputLastNameError: string | null,
            inputPhoneError: string | null,
            inputEmailError: string | null,
            inputLanguageError: string | null,
            inputPasswordError: string | null,
            inputPasswordConfirmError: string | null,
            /* END - IProfileValidationErrors model used in actions and thunks. */
        }
        export interface IProfileContext {
            /* START - IProfileContext model used in actions and thunks. */
            // TODO Describe IProfileContext model used in actions and thunks.
            [key: string]: string,
            /* END - IProfileContext model used in actions and thunks. */
        }
        export interface IProfileInstance {
            /* START - IProfileInstance model used in actions and thunks. */
            // TODO Describe IProfileInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IProfileInstance model used in actions and thunks. */
        }
        export interface IProfileConfirmResult {
            /* START - IProfileConfirmResult model used in actions and thunks. */
            success: boolean,
            /* END - IProfileConfirmResult model used in actions and thunks. */
        }
        export interface IRequestProfileConfirm {
            /* START - IRequestProfileConfirm model used in actions and thunks. */
            email: string | null,
            phone: string | null,
            token: string | null,
            tokenApi: string | null,
            /* END - IRequestProfileConfirm model used in actions and thunks. */
        }
        export interface IProfileResult {
            /* START - IProfileResult model used in actions and thunks. */
            success: boolean,
            /* END - IProfileResult model used in actions and thunks. */
        }
        export interface IRequestProfile {
            /* START - IRequestProfile model used in actions and thunks. */
            token: string | null,
            tokenApi: string | null,
            name: string | null,
            lastName: string | null,
            phone: string | null,
            email: string | null,
            userData: IUserData | null,
            language: string | null,
            code: string | null,
            /* END - IRequestProfile model used in actions and thunks. */
        }
        export interface IProfileContainerExecute {
            /* START - IProfileContainerExecute model used in actions and thunks. */
            // TODO Describe IProfileContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IProfileContainerExecute model used in actions and thunks. */
        }
        export interface IProfilePresenter {
            /* START - IProfilePresenter model used in actions and thunks. */
            confirmSeconds: number | null,
            /* END - IProfilePresenter model used in actions and thunks. */
        }
        export interface IDataSecurityContext {
            /* START - IDataSecurityContext model used in actions and thunks. */
            // TODO Describe IDataSecurityContext model used in actions and thunks.
            [key: string]: string,
            /* END - IDataSecurityContext model used in actions and thunks. */
        }
        export interface IDataSecurityInstance {
            /* START - IDataSecurityInstance model used in actions and thunks. */
            // TODO Describe IDataSecurityInstance model used in actions and thunks.
            [key: string]: string,
            /* END - IDataSecurityInstance model used in actions and thunks. */
        }
        export interface IDataSecurityContainerExecute {
            /* START - IDataSecurityContainerExecute model used in actions and thunks. */
            // TODO Describe IDataSecurityContainerExecute model used in actions and thunks.
            [key: string]: string,
            /* END - IDataSecurityContainerExecute model used in actions and thunks. */
        }
        export interface IDataSecurityPresenter {
            /* START - IDataSecurityPresenter model used in actions and thunks. */
            // TODO Describe IDataSecurityPresenter model used in actions and thunks.
            [key: string]: string,
            /* END - IDataSecurityPresenter model used in actions and thunks. */
        }

        export interface ICacheTag {
            tag: string | Enums.CacheContext | Enums.CachePolicy,
            contextId?: string
        }
        export interface IUrl {
            url: string,
            tagList: ICacheTag[],
            asyncParam?: {
                url: string,
                param: any,
                pollingCountDefault: number,
                pollingErrorDefault: Enums.ErrorType,
                pollingCount: number | null,
                operationId?: string,
            },
        }
        export interface IRouteItem {
            type: string,
            instance: any,
            isHiddenNavigation?: boolean,
            navigationPreviousApp?: any,
        }
        export interface IRoute {
            data: IRouteItem[]
        }
        interface IAction<T> extends Action<T> {
            instanceType: string,
            payload: T,
        }
        interface IError {
            type: Enums.ErrorType,
            code: string,
            message: string,
            comment: string,
            details: string,
        }
        interface IResponse<T> {
            data: T
            cachedDate: Date
        }
        // Route interfaces for container StartManager
        export interface IRouteStartManagerItem extends IRouteItem {
            type: Enums.ContainerStartManagerRouteType,
            instance: IStartManagerInstance
        }
        export interface IRouteStartManager extends IRoute {
            data: IRouteStartManagerItem[]
        }
        // Route interfaces for container Splash
        export interface IRouteSplashItem extends IRouteItem {
            type: Enums.ContainerSplashRouteType,
            instance: ISplashInstance
        }
        export interface IRouteSplash extends IRoute {
            data: IRouteSplashItem[]
        }
        // Route interfaces for container Registration
        export interface IRouteRegistrationItem extends IRouteItem {
            type: Enums.ContainerRegistrationRouteType,
            instance: IRegistrationInstance
        }
        export interface IRouteRegistration extends IRoute {
            data: IRouteRegistrationItem[]
        }
        // Route interfaces for container Authorization
        export interface IRouteAuthorizationItem extends IRouteItem {
            type: Enums.ContainerAuthorizationRouteType,
            instance: IAuthorizationInstance
        }
        export interface IRouteAuthorization extends IRoute {
            data: IRouteAuthorizationItem[]
        }
        // Route interfaces for container Main
        export interface IRouteMainItem extends IRouteItem {
            type: Enums.ContainerMainRouteType,
            instance: IMainInstance
        }
        export interface IRouteMain extends IRoute {
            data: IRouteMainItem[]
        }
        // Route interfaces for container AboutUs
        export interface IRouteAboutUsItem extends IRouteItem {
            type: Enums.ContainerAboutUsRouteType,
            instance: IAboutUsInstance
        }
        export interface IRouteAboutUs extends IRoute {
            data: IRouteAboutUsItem[]
        }
        // Route interfaces for container Claim
        export interface IRouteClaimItem extends IRouteItem {
            type: Enums.ContainerClaimRouteType,
            instance: IClaimInstance
        }
        export interface IRouteClaim extends IRoute {
            data: IRouteClaimItem[]
        }
        // Route interfaces for container MarketList
        export interface IRouteMarketListItem extends IRouteItem {
            type: Enums.ContainerMarketListRouteType,
            instance: IMarketListInstance
        }
        export interface IRouteMarketList extends IRoute {
            data: IRouteMarketListItem[]
        }
        // Route interfaces for container QrCode
        export interface IRouteQrCodeItem extends IRouteItem {
            type: Enums.ContainerQrCodeRouteType,
            instance: IQrCodeInstance
        }
        export interface IRouteQrCode extends IRoute {
            data: IRouteQrCodeItem[]
        }
        // Route interfaces for container Bonus
        export interface IRouteBonusItem extends IRouteItem {
            type: Enums.ContainerBonusRouteType,
            instance: IBonusInstance
        }
        export interface IRouteBonus extends IRoute {
            data: IRouteBonusItem[]
        }
        // Route interfaces for container Question
        export interface IRouteQuestionItem extends IRouteItem {
            type: Enums.ContainerQuestionRouteType,
            instance: IQuestionInstance
        }
        export interface IRouteQuestion extends IRoute {
            data: IRouteQuestionItem[]
        }
        // Route interfaces for container Idea
        export interface IRouteIdeaItem extends IRouteItem {
            type: Enums.ContainerIdeaRouteType,
            instance: IIdeaInstance
        }
        export interface IRouteIdea extends IRoute {
            data: IRouteIdeaItem[]
        }
        // Route interfaces for container Confirm
        export interface IRouteConfirmItem extends IRouteItem {
            type: Enums.ContainerConfirmRouteType,
            instance: IConfirmInstance
        }
        export interface IRouteConfirm extends IRoute {
            data: IRouteConfirmItem[]
        }
        // Route interfaces for container Profile
        export interface IRouteProfileItem extends IRouteItem {
            type: Enums.ContainerProfileRouteType,
            instance: IProfileInstance
        }
        export interface IRouteProfile extends IRoute {
            data: IRouteProfileItem[]
        }
        // Route interfaces for container DataSecurity
        export interface IRouteDataSecurityItem extends IRouteItem {
            type: Enums.ContainerDataSecurityRouteType,
            instance: IDataSecurityInstance
        }
        export interface IRouteDataSecurity extends IRoute {
            data: IRouteDataSecurityItem[]
        }

        /* START - Describe additional models used mobile application. */

        export interface IUserData {
            id: number | null,
            bonusAmount: number | null,
            ufLoyaltyId: string,
            phone: string,
            lastName: string,
            email: string,
            name: string,
        }
        export interface IErrorList {
            data: IError[],
        }
        export interface ICoordinates {
            latitude: number,
            longitude: number,
        }
        /* END - Describe additional models used mobile application. */
    }
    export interface IStartManagerState {
        appStatus: Enums.AppStatusType,  // State parameter displayed in "StartManager" screen. 
        isAppStatusBarVisible: boolean,  // State parameter displayed in "StartManager" screen. 
        isDemoMode: boolean,  // State parameter displayed in "StartManager" screen. 
        route: Models.IRouteStartManager,  // State parameter displayed in "StartManager" screen. 
        isVisibleModalUpdate: boolean,  // State parameter displayed in "StartManager" screen. 
        logErrorDetails: string,  // Internal state parameter of "StartManager" container. 
        start: boolean,  // Result for "performStart" thunk.
        startPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performStart".
        startOperationId: string | null,  // Unique identifier of started thunk chain "performStart".
        startError: Models.IError | null,  // Error info for thunk chain "performStart".
        userDetails: Models.IUserDetailsResult,  // Fetch result for "callPostUserDetails" thunk.
        userDetailsPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostUserDetails".
        userDetailsOperationId: string | null,  // Unique identifier of started thunk chain "callPostUserDetails".
        userDetailsError: Models.IError | null,  // Network error info for thunk chain "callPostUserDetails".
        userDetailsCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostUserDetails".
        credentials: Models.ICredentials,  // Result for "recoverCredentials" thunk.
        credentialsPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "recoverCredentials".
        credentialsOperationId: string | null,  // Unique identifier of started thunk chain "recoverCredentials".
        credentialsError: Models.IError | null,  // Error info for thunk chain "recoverCredentials".
        language: string,  // Result for "recoverLanguage" thunk.
        languagePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "recoverLanguage".
        languageOperationId: string | null,  // Unique identifier of started thunk chain "recoverLanguage".
        languageError: Models.IError | null,  // Error info for thunk chain "recoverLanguage".
        logout: Models.ILogoutResult,  // Fetch result for "callPostLogout" thunk.
        logoutPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostLogout".
        logoutOperationId: string | null,  // Unique identifier of started thunk chain "callPostLogout".
        logoutError: Models.IError | null,  // Network error info for thunk chain "callPostLogout".
        logoutCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostLogout".
        version: Models.IVersionResult,  // Fetch result for "callPostVersion" thunk.
        versionPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostVersion".
        versionOperationId: string | null,  // Unique identifier of started thunk chain "callPostVersion".
        versionError: Models.IError | null,  // Network error info for thunk chain "callPostVersion".
        versionCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostVersion".
        log: boolean,  // Fetch result for "callPostLog" thunk.
        logPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostLog".
        logOperationId: string | null,  // Unique identifier of started thunk chain "callPostLog".
        logError: Models.IError | null,  // Network error info for thunk chain "callPostLog".
        logCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostLog".
        containerExecute: Models.IStartManagerContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IStartManagerPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IStartManagerContext,  // Container "StartManager" context parameters.
        instanceId: string,  // Container "StartManager" instance identity hash.
        instance: Models.IStartManagerInstance,  // Container "StartManager" instance parameters.

        /* START - Container StartManager state model additional fields. */
        // TODO Describe StartManager reducer state.
        /* END - Container StartManager state model additional fields. */

    }
    export interface ISplashState {
        animationPhase: Enums.SplashAnimationPhase,  // State parameter displayed in "Splash" screen. 
        containerExecute: Models.ISplashContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.ISplashPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.ISplashContext,  // Container "Splash" context parameters.
        instanceId: string,  // Container "Splash" instance identity hash.
        instance: Models.ISplashInstance,  // Container "Splash" instance parameters.

        /* START - Container Splash state model additional fields. */
        // TODO Describe Splash reducer state.
        /* END - Container Splash state model additional fields. */

    }
    export interface IRegistrationState {
        validationErrors: Models.IRegistrationValidationErrors,  // State parameter displayed in "Registration" screen. 
        inputName: string,  // State parameter displayed in "Registration" screen. 
        inputLastName: string,  // State parameter displayed in "Registration" screen. 
        inputPhone: string,  // State parameter displayed in "Registration" screen. 
        inputEmail: string,  // State parameter displayed in "Registration" screen. 
        inputLanguage: string,  // State parameter displayed in "Registration" screen. 
        inputPassword: string,  // State parameter displayed in "Registration" screen. 
        inputPasswordConfirm: string,  // State parameter displayed in "Registration" screen. 
        credentialsChangePhase: Enums.CredentialsChangePhase,  // State parameter displayed in "Registration" screen. 
        isLanguageExpanded: boolean,  // State parameter displayed in "Registration" screen. 
        registrationConfirm: Models.IRegistrationConfirmResult,  // Fetch result for "callPostRegistrationConfirm" thunk.
        registrationConfirmPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostRegistrationConfirm".
        registrationConfirmOperationId: string | null,  // Unique identifier of started thunk chain "callPostRegistrationConfirm".
        registrationConfirmError: Models.IError | null,  // Network error info for thunk chain "callPostRegistrationConfirm".
        registrationConfirmCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostRegistrationConfirm".
        registration: Models.IRegistrationResult,  // Fetch result for "callPostRegistration" thunk.
        registrationPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostRegistration".
        registrationOperationId: string | null,  // Unique identifier of started thunk chain "callPostRegistration".
        registrationError: Models.IError | null,  // Network error info for thunk chain "callPostRegistration".
        registrationCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostRegistration".
        containerExecute: Models.IRegistrationContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IRegistrationPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IRegistrationContext,  // Container "Registration" context parameters.
        instanceId: string,  // Container "Registration" instance identity hash.
        instance: Models.IRegistrationInstance,  // Container "Registration" instance parameters.

        /* START - Container Registration state model additional fields. */
        // TODO Describe Registration reducer state.
        /* END - Container Registration state model additional fields. */

    }
    export interface IAuthorizationState {
        authorizationMode: Enums.AuthorizationMode,  // State parameter displayed in "Authorization" screen. 
        inputPhone: string,  // State parameter displayed in "Authorization" screen. 
        inputEmail: string,  // State parameter displayed in "Authorization" screen. 
        inputCode: string,  // State parameter displayed in "Authorization" screen. 
        validationErrors: Models.IAuthorizationValidationErrors,  // State parameter displayed in "Authorization" screen. 
        authorization: Models.IAuthorizationResult,  // Fetch result for "callPostAuthorization" thunk.
        authorizationPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostAuthorization".
        authorizationOperationId: string | null,  // Unique identifier of started thunk chain "callPostAuthorization".
        authorizationError: Models.IError | null,  // Network error info for thunk chain "callPostAuthorization".
        authorizationCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostAuthorization".
        authorizationConfirm: Models.IAuthorizationConfirmResult,  // Fetch result for "callPostAuthorizationConfirm" thunk.
        authorizationConfirmPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostAuthorizationConfirm".
        authorizationConfirmOperationId: string | null,  // Unique identifier of started thunk chain "callPostAuthorizationConfirm".
        authorizationConfirmError: Models.IError | null,  // Network error info for thunk chain "callPostAuthorizationConfirm".
        authorizationConfirmCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostAuthorizationConfirm".
        containerExecute: Models.IAuthorizationContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IAuthorizationPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IAuthorizationContext,  // Container "Authorization" context parameters.
        instanceId: string,  // Container "Authorization" instance identity hash.
        instance: Models.IAuthorizationInstance,  // Container "Authorization" instance parameters.

        /* START - Container Authorization state model additional fields. */
        // TODO Describe Authorization reducer state.
        /* END - Container Authorization state model additional fields. */

    }
    export interface IMainState {
        isMenuOpen: boolean,  // State parameter displayed in "Main" screen. 
        isVisibleModalLanguage: boolean,  // State parameter displayed in "Main" screen. 
        inputLanguage: string,  // State parameter displayed in "Main" screen. 
        containerExecute: Models.IMainContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IMainPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IMainContext,  // Container "Main" context parameters.
        instanceId: string,  // Container "Main" instance identity hash.
        instance: Models.IMainInstance,  // Container "Main" instance parameters.

        /* START - Container Main state model additional fields. */
        // TODO Describe Main reducer state.
        /* END - Container Main state model additional fields. */

    }
    export interface IAboutUsState {
        containerExecute: Models.IAboutUsContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IAboutUsPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IAboutUsContext,  // Container "AboutUs" context parameters.
        instanceId: string,  // Container "AboutUs" instance identity hash.
        instance: Models.IAboutUsInstance,  // Container "AboutUs" instance parameters.

        /* START - Container AboutUs state model additional fields. */
        // TODO Describe AboutUs reducer state.
        /* END - Container AboutUs state model additional fields. */

    }
    export interface IClaimState {
        validationErrors: Models.IClaimValidationErrors,  // State parameter displayed in "Claim" screen. 
        inputPhone: string,  // State parameter displayed in "Claim" screen. 
        inputEmail: string,  // State parameter displayed in "Claim" screen. 
        inputProduct: string,  // State parameter displayed in "Claim" screen. 
        inputWhatToFix: string,  // State parameter displayed in "Claim" screen. 
        isVisibleSentNotice: boolean,  // State parameter displayed in "Claim" screen. 
        isVisibleEmailPhone: boolean,  // State parameter displayed in "Claim" screen. 
        Claim: Models.IClaimResult,  // Fetch result for "callPostClaim" thunk.
        ClaimPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostClaim".
        ClaimOperationId: string | null,  // Unique identifier of started thunk chain "callPostClaim".
        ClaimError: Models.IError | null,  // Network error info for thunk chain "callPostClaim".
        ClaimCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostClaim".
        containerExecute: Models.IClaimContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IClaimPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IClaimContext,  // Container "Claim" context parameters.
        instanceId: string,  // Container "Claim" instance identity hash.
        instance: Models.IClaimInstance,  // Container "Claim" instance parameters.

        /* START - Container Claim state model additional fields. */
        // TODO Describe Claim reducer state.
        /* END - Container Claim state model additional fields. */

    }
    export interface IMarketListState {
        inputMarket: Models.IMarket,  // State parameter displayed in "MarketList" screen. 
        MarketList: Models.IMarketList,  // Fetch result for "callPostMarketList" thunk.
        MarketListPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostMarketList".
        MarketListOperationId: string | null,  // Unique identifier of started thunk chain "callPostMarketList".
        MarketListError: Models.IError | null,  // Network error info for thunk chain "callPostMarketList".
        MarketListCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostMarketList".
        geolocation: Models.IGeolocation,  // Result for "performGeolocation" thunk.
        geolocationPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performGeolocation".
        geolocationOperationId: string | null,  // Unique identifier of started thunk chain "performGeolocation".
        geolocationError: Models.IError | null,  // Error info for thunk chain "performGeolocation".
        containerExecute: Models.IMarketListContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IMarketListPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IMarketListContext,  // Container "MarketList" context parameters.
        instanceId: string,  // Container "MarketList" instance identity hash.
        instance: Models.IMarketListInstance,  // Container "MarketList" instance parameters.

        /* START - Container MarketList state model additional fields. */
        // TODO Describe MarketList reducer state.
        /* END - Container MarketList state model additional fields. */

    }
    export interface IQrCodeState {
        containerExecute: Models.IQrCodeContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IQrCodePresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IQrCodeContext,  // Container "QrCode" context parameters.
        instanceId: string,  // Container "QrCode" instance identity hash.
        instance: Models.IQrCodeInstance,  // Container "QrCode" instance parameters.

        /* START - Container QrCode state model additional fields. */
        // TODO Describe QrCode reducer state.
        /* END - Container QrCode state model additional fields. */

    }
    export interface IBonusState {
        containerExecute: Models.IBonusContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IBonusPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IBonusContext,  // Container "Bonus" context parameters.
        instanceId: string,  // Container "Bonus" instance identity hash.
        instance: Models.IBonusInstance,  // Container "Bonus" instance parameters.

        /* START - Container Bonus state model additional fields. */
        // TODO Describe Bonus reducer state.
        /* END - Container Bonus state model additional fields. */

    }
    export interface IQuestionState {
        validationErrors: Models.IQuestionValidationErrors,  // State parameter displayed in "Question" screen. 
        inputPhone: string,  // State parameter displayed in "Question" screen. 
        inputEmail: string,  // State parameter displayed in "Question" screen. 
        inputQuestion: string,  // State parameter displayed in "Question" screen. 
        isVisibleSentNotice: boolean,  // State parameter displayed in "Question" screen. 
        isVisibleEmailPhone: boolean,  // State parameter displayed in "Question" screen. 
        Question: Models.IQuestionResult,  // Fetch result for "callPostQuestion" thunk.
        QuestionPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostQuestion".
        QuestionOperationId: string | null,  // Unique identifier of started thunk chain "callPostQuestion".
        QuestionError: Models.IError | null,  // Network error info for thunk chain "callPostQuestion".
        QuestionCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostQuestion".
        containerExecute: Models.IQuestionContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IQuestionPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IQuestionContext,  // Container "Question" context parameters.
        instanceId: string,  // Container "Question" instance identity hash.
        instance: Models.IQuestionInstance,  // Container "Question" instance parameters.

        /* START - Container Question state model additional fields. */
        // TODO Describe Question reducer state.
        /* END - Container Question state model additional fields. */

    }
    export interface IIdeaState {
        validationErrors: Models.IIdeaValidationErrors,  // State parameter displayed in "Idea" screen. 
        inputPhone: string,  // State parameter displayed in "Idea" screen. 
        inputEmail: string,  // State parameter displayed in "Idea" screen. 
        inputIdea: string,  // State parameter displayed in "Idea" screen. 
        isVisibleSentNotice: boolean,  // State parameter displayed in "Idea" screen. 
        isVisibleEmailPhone: boolean,  // State parameter displayed in "Idea" screen. 
        Idea: Models.IIdeaResult,  // Fetch result for "callPostIdea" thunk.
        IdeaPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostIdea".
        IdeaOperationId: string | null,  // Unique identifier of started thunk chain "callPostIdea".
        IdeaError: Models.IError | null,  // Network error info for thunk chain "callPostIdea".
        IdeaCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostIdea".
        containerExecute: Models.IIdeaContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IIdeaPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IIdeaContext,  // Container "Idea" context parameters.
        instanceId: string,  // Container "Idea" instance identity hash.
        instance: Models.IIdeaInstance,  // Container "Idea" instance parameters.

        /* START - Container Idea state model additional fields. */
        // TODO Describe Idea reducer state.
        /* END - Container Idea state model additional fields. */

    }
    export interface IConfirmState {
        validationErrors: Models.IConfirmValidationErrors,  // State parameter displayed in "Confirm" screen. 
        inputConfirm: string,  // State parameter displayed in "Confirm" screen. 
        confirm: Models.IConfirmResult,  // Fetch result for "callPostConfirm" thunk.
        confirmPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostConfirm".
        confirmOperationId: string | null,  // Unique identifier of started thunk chain "callPostConfirm".
        confirmError: Models.IError | null,  // Network error info for thunk chain "callPostConfirm".
        confirmCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostConfirm".
        containerExecute: Models.IConfirmContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IConfirmPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IConfirmContext,  // Container "Confirm" context parameters.
        instanceId: string,  // Container "Confirm" instance identity hash.
        instance: Models.IConfirmInstance,  // Container "Confirm" instance parameters.

        /* START - Container Confirm state model additional fields. */
        // TODO Describe Confirm reducer state.
        /* END - Container Confirm state model additional fields. */

    }
    export interface IProfileState {
        validationErrors: Models.IProfileValidationErrors,  // State parameter displayed in "Profile" screen. 
        inputName: string,  // State parameter displayed in "Profile" screen. 
        inputLastName: string,  // State parameter displayed in "Profile" screen. 
        inputPhone: string,  // State parameter displayed in "Profile" screen. 
        inputEmail: string,  // State parameter displayed in "Profile" screen. 
        inputLanguage: string,  // State parameter displayed in "Profile" screen. 
        inputPassword: string,  // State parameter displayed in "Profile" screen. 
        inputPasswordConfirm: string,  // State parameter displayed in "Profile" screen. 
        credentialsChangePhase: Enums.CredentialsChangePhase,  // State parameter displayed in "Profile" screen. 
        isLanguageExpanded: boolean,  // State parameter displayed in "Profile" screen. 
        profileConfirm: Models.IProfileConfirmResult,  // Fetch result for "callPostProfileConfirm" thunk.
        profileConfirmPhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostProfileConfirm".
        profileConfirmOperationId: string | null,  // Unique identifier of started thunk chain "callPostProfileConfirm".
        profileConfirmError: Models.IError | null,  // Network error info for thunk chain "callPostProfileConfirm".
        profileConfirmCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostProfileConfirm".
        profile: Models.IProfileResult,  // Fetch result for "callPostProfile" thunk.
        profilePhase: Enums.ThunkChainPhase,  // Phase of network thunk chain "callPostProfile".
        profileOperationId: string | null,  // Unique identifier of started thunk chain "callPostProfile".
        profileError: Models.IError | null,  // Network error info for thunk chain "callPostProfile".
        profileCachedDate: Date | null,  // Response data cache date for network thunk chain "callPostProfile".
        containerExecute: Models.IProfileContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IProfilePresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IProfileContext,  // Container "Profile" context parameters.
        instanceId: string,  // Container "Profile" instance identity hash.
        instance: Models.IProfileInstance,  // Container "Profile" instance parameters.

        /* START - Container Profile state model additional fields. */
        // TODO Describe Profile reducer state.
        /* END - Container Profile state model additional fields. */

    }
    export interface IDataSecurityState {
        containerExecute: Models.IDataSecurityContainerExecute,  // Result for "performContainerExecute" thunk.
        containerExecutePhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "performContainerExecute".
        containerExecuteOperationId: string | null,  // Unique identifier of started thunk chain "performContainerExecute".
        containerExecuteError: Models.IError | null,  // Error info for thunk chain "performContainerExecute".
        presenter: Models.IDataSecurityPresenter,  // Result for "presenterUpdate" thunk.
        presenterPhase: Enums.ThunkChainPhase,  // Progress indicator for thunk chain "presenterUpdate".
        presenterOperationId: string | null,  // Unique identifier of started thunk chain "presenterUpdate".
        presenterError: Models.IError | null,  // Error info for thunk chain "presenterUpdate".
        context: Models.IDataSecurityContext,  // Container "DataSecurity" context parameters.
        instanceId: string,  // Container "DataSecurity" instance identity hash.
        instance: Models.IDataSecurityInstance,  // Container "DataSecurity" instance parameters.

        /* START - Container DataSecurity state model additional fields. */
        // TODO Describe DataSecurity reducer state.
        /* END - Container DataSecurity state model additional fields. */

    }
    // Interface for root state
    export interface IRootState {
        reducerStartManager: IStartManagerState,
        reducerSplash: ISplashState,
        reducerRegistration: IRegistrationState,
        reducerAuthorization: IAuthorizationState,
        reducerMain: IMainState,
        reducerAboutUs: IAboutUsState,
        reducerClaim: IClaimState,
        reducerMarketList: IMarketListState,
        reducerQrCode: IQrCodeState,
        reducerBonus: IBonusState,
        reducerQuestion: IQuestionState,
        reducerIdea: IIdeaState,
        reducerConfirm: IConfirmState,
        reducerProfile: IProfileState,
        reducerDataSecurity: IDataSecurityState,

        /* START - Connect additional state. */

        /* END - Connect additional state. */
    }

    export const ReducerRoot: Reducer<IRootState>

    export namespace thunkStartManager {
        export const performInputAppStatus: (instanceType: Enums.ContainerStartManagerInstanceType, mode: Enums.AppStatusType) => void
        export const setIsAppStatusBarVisible: (instanceType: Enums.ContainerStartManagerInstanceType, isAppStatusBarVisible: boolean) => void
        export const performDemoModeEnable: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const performDemoModeDisable: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToSplashScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToAuthorizationScreen: (instanceType: Enums.ContainerStartManagerInstanceType, instance: Models.IAuthorizationInstance, ) => void
        export const navigateToRegistrationScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToMainScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToDataSecurityScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToAboutUsScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToClaimScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToMarketListScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToQrCodeScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToBonusScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToQuestionScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToIdeaScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateToConfirmScreen: (instanceType: Enums.ContainerStartManagerInstanceType, instance: Models.IConfirmInstance, ) => void
        export const navigateToProfileScreen: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const navigateBack: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const performStartReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const performStart: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const callPostUserDetailsReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const callPostUserDetails: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const recoverCredentialsReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const recoverCredentials: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const persistCredentialsReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const persistCredentials: (instanceType: Enums.ContainerStartManagerInstanceType, credentials: Models.ICredentials) => void
        export const recoverLanguageReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const recoverLanguage: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const persistLanguageReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const persistLanguage: (instanceType: Enums.ContainerStartManagerInstanceType, language: string) => void
        export const performLogout: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const callPostLogoutReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const callPostLogout: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const callPostVersionReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const callPostVersion: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const performModalUpdateShow: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const performModalUpdateHide: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const performUpdateNow: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const performSendLog: (instanceType: Enums.ContainerStartManagerInstanceType, logErrorDetails: string) => void
        export const callPostLogReset: (instanceType: Enums.ContainerStartManagerInstanceType, ) => void
        export const callPostLog: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const performContainerExecute: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerStartManagerInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerStartManagerInstanceType, stateRecovering: IStartManagerState) => void
        export const performContainerInit: (instanceType: Enums.ContainerStartManagerInstanceType, context: Models.IStartManagerContext, instance: Models.IStartManagerInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerStartManagerInstanceType, context: Models.IStartManagerContext, instance: Models.IStartManagerInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerStartManagerInstanceType) => void
    }

    export namespace thunkSplash {
        export const performInputAnimationPhase: (instanceType: Enums.ContainerSplashInstanceType, phase: Enums.SplashAnimationPhase) => void
        export const performContainerExecute: (instanceType: Enums.ContainerSplashInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerSplashInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerSplashInstanceType, stateRecovering: ISplashState) => void
        export const performContainerInit: (instanceType: Enums.ContainerSplashInstanceType, context: Models.ISplashContext, instance: Models.ISplashInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerSplashInstanceType, context: Models.ISplashContext, instance: Models.ISplashInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerSplashInstanceType) => void
    }

    export namespace thunkRegistration {
        export const performValidate: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performInputName: (instanceType: Enums.ContainerRegistrationInstanceType, name: string) => void
        export const performInputLastName: (instanceType: Enums.ContainerRegistrationInstanceType, lastName: string) => void
        export const performInputPhone: (instanceType: Enums.ContainerRegistrationInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerRegistrationInstanceType, email: string) => void
        export const performInputLanguage: (instanceType: Enums.ContainerRegistrationInstanceType, language: string) => void
        export const performInputPassword: (instanceType: Enums.ContainerRegistrationInstanceType, password: string) => void
        export const performInputPasswordConfirm: (instanceType: Enums.ContainerRegistrationInstanceType, passwordConfirm: string) => void
        export const performChangeEmailStart: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performChangeEmailCodeRequested: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performChangePhoneStart: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performChangePhoneCodeRequested: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performChangeReset: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const callPostRegistrationConfirmReset: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const callPostRegistrationConfirm: (instanceType: Enums.ContainerRegistrationInstanceType) => void
        export const callPostRegistrationReset: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const callPostRegistration: (instanceType: Enums.ContainerRegistrationInstanceType) => void
        export const performLanguageExpandedToggle: (instanceType: Enums.ContainerRegistrationInstanceType, ) => void
        export const performContainerExecute: (instanceType: Enums.ContainerRegistrationInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerRegistrationInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerRegistrationInstanceType, stateRecovering: IRegistrationState) => void
        export const performContainerInit: (instanceType: Enums.ContainerRegistrationInstanceType, context: Models.IRegistrationContext, instance: Models.IRegistrationInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerRegistrationInstanceType, context: Models.IRegistrationContext, instance: Models.IRegistrationInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerRegistrationInstanceType) => void
    }

    export namespace thunkAuthorization {
        export const performInputAuthorizationMode: (instanceType: Enums.ContainerAuthorizationInstanceType, mode: Enums.AuthorizationMode) => void
        export const performInputPhone: (instanceType: Enums.ContainerAuthorizationInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerAuthorizationInstanceType, email: string) => void
        export const performInputCode: (instanceType: Enums.ContainerAuthorizationInstanceType, code: string) => void
        export const performValidate: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => void
        export const callPostAuthorizationReset: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => void
        export const callPostAuthorization: (instanceType: Enums.ContainerAuthorizationInstanceType) => void
        export const callPostAuthorizationConfirmReset: (instanceType: Enums.ContainerAuthorizationInstanceType, ) => void
        export const callPostAuthorizationConfirm: (instanceType: Enums.ContainerAuthorizationInstanceType) => void
        export const performContainerExecute: (instanceType: Enums.ContainerAuthorizationInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerAuthorizationInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerAuthorizationInstanceType, stateRecovering: IAuthorizationState) => void
        export const performContainerInit: (instanceType: Enums.ContainerAuthorizationInstanceType, context: Models.IAuthorizationContext, instance: Models.IAuthorizationInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerAuthorizationInstanceType, context: Models.IAuthorizationContext, instance: Models.IAuthorizationInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerAuthorizationInstanceType) => void
    }

    export namespace thunkMain {
        export const performMenuOpen: (instanceType: Enums.ContainerMainInstanceType, ) => void
        export const performMenuClose: (instanceType: Enums.ContainerMainInstanceType, ) => void
        export const performOpenFacebook: (instanceType: Enums.ContainerMainInstanceType) => void
        export const performOpenWhatsapp: (instanceType: Enums.ContainerMainInstanceType) => void
        export const performOpenInstagram: (instanceType: Enums.ContainerMainInstanceType) => void
        export const performModalLanguageShow: (instanceType: Enums.ContainerMainInstanceType, ) => void
        export const performModalLanguageHide: (instanceType: Enums.ContainerMainInstanceType, ) => void
        export const performInputLanguage: (instanceType: Enums.ContainerMainInstanceType, language: string) => void
        export const performContainerExecute: (instanceType: Enums.ContainerMainInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerMainInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerMainInstanceType, stateRecovering: IMainState) => void
        export const performContainerInit: (instanceType: Enums.ContainerMainInstanceType, context: Models.IMainContext, instance: Models.IMainInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerMainInstanceType, context: Models.IMainContext, instance: Models.IMainInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerMainInstanceType) => void
    }

    export namespace thunkAboutUs {
        export const performContainerExecute: (instanceType: Enums.ContainerAboutUsInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerAboutUsInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState) => void
        export const performContainerInit: (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerAboutUsInstanceType) => void
    }

    export namespace thunkClaim {
        export const performValidate: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const performInputPhone: (instanceType: Enums.ContainerClaimInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerClaimInstanceType, email: string) => void
        export const performInputProduct: (instanceType: Enums.ContainerClaimInstanceType, product: string) => void
        export const performInputWhatToFix: (instanceType: Enums.ContainerClaimInstanceType, whatToFix: string) => void
        export const callPostClaimReset: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const callPostClaim: (instanceType: Enums.ContainerClaimInstanceType) => void
        export const performSentNoticeShow: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const performSentNoticeHide: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const performEmailPhoneShow: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const performEmailPhoneHide: (instanceType: Enums.ContainerClaimInstanceType, ) => void
        export const performContainerExecute: (instanceType: Enums.ContainerClaimInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerClaimInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerClaimInstanceType, stateRecovering: IClaimState) => void
        export const performContainerInit: (instanceType: Enums.ContainerClaimInstanceType, context: Models.IClaimContext, instance: Models.IClaimInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerClaimInstanceType, context: Models.IClaimContext, instance: Models.IClaimInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerClaimInstanceType) => void
    }

    export namespace thunkMarketList {
        export const performSendEmail: (instanceType: Enums.ContainerMarketListInstanceType, market: Models.IMarket) => void
        export const performLookAtMap: (instanceType: Enums.ContainerMarketListInstanceType, market: Models.IMarket) => void
        export const callPostMarketListReset: (instanceType: Enums.ContainerMarketListInstanceType, ) => void
        export const callPostMarketList: (instanceType: Enums.ContainerMarketListInstanceType) => void
        export const performGeolocationReset: (instanceType: Enums.ContainerMarketListInstanceType, ) => void
        export const performGeolocation: (instanceType: Enums.ContainerMarketListInstanceType) => void
        export const performContainerExecute: (instanceType: Enums.ContainerMarketListInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerMarketListInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerMarketListInstanceType, stateRecovering: IMarketListState) => void
        export const performContainerInit: (instanceType: Enums.ContainerMarketListInstanceType, context: Models.IMarketListContext, instance: Models.IMarketListInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerMarketListInstanceType, context: Models.IMarketListContext, instance: Models.IMarketListInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerMarketListInstanceType) => void
    }

    export namespace thunkQrCode {
        export const performContainerExecute: (instanceType: Enums.ContainerQrCodeInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerQrCodeInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerQrCodeInstanceType, stateRecovering: IQrCodeState) => void
        export const performContainerInit: (instanceType: Enums.ContainerQrCodeInstanceType, context: Models.IQrCodeContext, instance: Models.IQrCodeInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerQrCodeInstanceType, context: Models.IQrCodeContext, instance: Models.IQrCodeInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerQrCodeInstanceType) => void
    }

    export namespace thunkBonus {
        export const performContainerExecute: (instanceType: Enums.ContainerBonusInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerBonusInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerBonusInstanceType, stateRecovering: IBonusState) => void
        export const performContainerInit: (instanceType: Enums.ContainerBonusInstanceType, context: Models.IBonusContext, instance: Models.IBonusInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerBonusInstanceType, context: Models.IBonusContext, instance: Models.IBonusInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerBonusInstanceType) => void
    }

    export namespace thunkQuestion {
        export const performValidate: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const performInputPhone: (instanceType: Enums.ContainerQuestionInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerQuestionInstanceType, email: string) => void
        export const performInputQuestion: (instanceType: Enums.ContainerQuestionInstanceType, question: string) => void
        export const callPostQuestionReset: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const callPostQuestion: (instanceType: Enums.ContainerQuestionInstanceType) => void
        export const performSentNoticeShow: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const performSentNoticeHide: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const performEmailPhoneShow: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const performEmailPhoneHide: (instanceType: Enums.ContainerQuestionInstanceType, ) => void
        export const performContainerExecute: (instanceType: Enums.ContainerQuestionInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerQuestionInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerQuestionInstanceType, stateRecovering: IQuestionState) => void
        export const performContainerInit: (instanceType: Enums.ContainerQuestionInstanceType, context: Models.IQuestionContext, instance: Models.IQuestionInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerQuestionInstanceType, context: Models.IQuestionContext, instance: Models.IQuestionInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerQuestionInstanceType) => void
    }

    export namespace thunkIdea {
        export const performValidate: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const performInputPhone: (instanceType: Enums.ContainerIdeaInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerIdeaInstanceType, email: string) => void
        export const performInputIdea: (instanceType: Enums.ContainerIdeaInstanceType, idea: string) => void
        export const callPostIdeaReset: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const callPostIdea: (instanceType: Enums.ContainerIdeaInstanceType) => void
        export const performSentNoticeShow: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const performSentNoticeHide: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const performEmailPhoneShow: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const performEmailPhoneHide: (instanceType: Enums.ContainerIdeaInstanceType, ) => void
        export const performContainerExecute: (instanceType: Enums.ContainerIdeaInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerIdeaInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerIdeaInstanceType, stateRecovering: IIdeaState) => void
        export const performContainerInit: (instanceType: Enums.ContainerIdeaInstanceType, context: Models.IIdeaContext, instance: Models.IIdeaInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerIdeaInstanceType, context: Models.IIdeaContext, instance: Models.IIdeaInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerIdeaInstanceType) => void
    }

    export namespace thunkConfirm {
        export const performValidate: (instanceType: Enums.ContainerConfirmInstanceType, ) => void
        export const performInputConfirm: (instanceType: Enums.ContainerConfirmInstanceType, confirm: string) => void
        export const callPostConfirmReset: (instanceType: Enums.ContainerConfirmInstanceType, ) => void
        export const callPostConfirm: (instanceType: Enums.ContainerConfirmInstanceType) => void
        export const performContainerExecute: (instanceType: Enums.ContainerConfirmInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerConfirmInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerConfirmInstanceType, stateRecovering: IConfirmState) => void
        export const performContainerInit: (instanceType: Enums.ContainerConfirmInstanceType, context: Models.IConfirmContext, instance: Models.IConfirmInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerConfirmInstanceType, context: Models.IConfirmContext, instance: Models.IConfirmInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerConfirmInstanceType) => void
    }

    export namespace thunkProfile {
        export const performValidate: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performInputName: (instanceType: Enums.ContainerProfileInstanceType, name: string) => void
        export const performInputLastName: (instanceType: Enums.ContainerProfileInstanceType, lastName: string) => void
        export const performInputPhone: (instanceType: Enums.ContainerProfileInstanceType, phone: string) => void
        export const performInputEmail: (instanceType: Enums.ContainerProfileInstanceType, email: string) => void
        export const performInputLanguage: (instanceType: Enums.ContainerProfileInstanceType, language: string) => void
        export const performInputPassword: (instanceType: Enums.ContainerProfileInstanceType, password: string) => void
        export const performInputPasswordConfirm: (instanceType: Enums.ContainerProfileInstanceType, passwordConfirm: string) => void
        export const performChangeEmailStart: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performChangeEmailCodeRequested: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performChangePhoneStart: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performChangePhoneCodeRequested: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performChangeReset: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const callPostProfileConfirmReset: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const callPostProfileConfirm: (instanceType: Enums.ContainerProfileInstanceType) => void
        export const callPostProfileReset: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const callPostProfile: (instanceType: Enums.ContainerProfileInstanceType) => void
        export const performLanguageExpandedToggle: (instanceType: Enums.ContainerProfileInstanceType, ) => void
        export const performContainerExecute: (instanceType: Enums.ContainerProfileInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerProfileInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerProfileInstanceType, stateRecovering: IProfileState) => void
        export const performContainerInit: (instanceType: Enums.ContainerProfileInstanceType, context: Models.IProfileContext, instance: Models.IProfileInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerProfileInstanceType, context: Models.IProfileContext, instance: Models.IProfileInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerProfileInstanceType) => void
    }

    export namespace thunkDataSecurity {
        export const performContainerExecute: (instanceType: Enums.ContainerDataSecurityInstanceType) => void
        export const performContainerReset: (instanceType: Enums.ContainerDataSecurityInstanceType) => void
        export const performContainerRecover: (instanceType: Enums.ContainerDataSecurityInstanceType, stateRecovering: IDataSecurityState) => void
        export const performContainerInit: (instanceType: Enums.ContainerDataSecurityInstanceType, context: Models.IDataSecurityContext, instance: Models.IDataSecurityInstance, ) => void
        export const performContainerRefresh: (instanceType: Enums.ContainerDataSecurityInstanceType, context: Models.IDataSecurityContext, instance: Models.IDataSecurityInstance, ) => void
        export const presenterUpdate: (instanceType: Enums.ContainerDataSecurityInstanceType) => void
    }

    /* START - Additional exports for mobile application. */
    /* END - Additional exports for mobile application. */

}
