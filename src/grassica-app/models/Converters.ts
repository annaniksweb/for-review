/**
 * Converters for network response and request data.
 *
 * @author Burnaev M.U.
 * @see
 */

import {defaultValues} from './Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

/* START - Additional response and request data converters. */
import * as util from '../common/Util'

import * as profileEditRq from 'profileEditRq'
import * as profileEditRs from 'profileEditRs'
import * as profileEditRqValidator from '../integration/validator/profileEditRqValidator'
import * as profileEditRsValidator from '../integration/validator/profileEditRsValidator'

import * as userDetailsRq from 'userDetailsRq'
import * as userDetailsRs from 'userDetailsRs'
import * as userDetailRqValidator from '../integration/validator/userDetailsRqValidator'
import * as userDetailRsValidator from '../integration/validator/userDetailsRsValidator'

import * as authorizationRq from 'authRq'
import * as authorizationRs from 'authRs'
import * as authorizationValidatorRq from '../integration/validator/authRqValidator'
import * as authorizationValidatorRs from '../integration/validator/authRsValidator'

import * as authorizationCodeConfirmRq from 'authCodeConfirmRq'
import * as authorizationCodeConfirmRs from 'authCodeConfirmRs'
import * as authorizationCodeConfirmValidatorRq from '../integration/validator/authCodeConfirmRqValidator'
import * as authorizationCodeConfirmValidatorRs from '../integration/validator/authCodeConfirmRsValidator'

import * as logoutRq from 'logoutRq'
import * as logoutRs from 'logoutRs'
import * as logoutRqValidator from '../integration/validator/logoutRqValidator'
import * as logoutRsValidator from '../integration/validator/logoutRsValidator'

import * as versionRq from 'versionRq'
import * as versionRs from 'versionRs'
import * as versionRqValidator from '../integration/validator/versionRqValidator'
import * as versionRsValidator from '../integration/validator/versionRsValidator'

import * as claimRq from 'claimRq'
import * as claimRs from 'claimRs'
import * as claimRqValidator from '../integration/validator/claimRqValidator'
import * as claimRsValidator from '../integration/validator/claimRsValidator'

import * as ideaRq from 'ideaRq'
import * as ideaRs from 'ideaRs'
import * as ideaRqValidator from '../integration/validator/ideaRqValidator'
import * as ideaRsValidator from '../integration/validator/ideaRsValidator'

import * as questionRq from 'questionRq'
import * as questionRs from 'questionRs'
import * as questionRqValidator from '../integration/validator/questionRqValidator'
import * as questionRsValidator from '../integration/validator/questionRsValidator'

import * as shopListRq from 'shopListRq'
import * as shopListRs from 'shopListRs'
import * as shopListRqValidator from '../integration/validator/shopListRqValidator'
import * as shopListRsValidator from '../integration/validator/shopListRsValidator'

import {Localization} from 'grassica-ui'

/* END - Additional response and request data converters. */
export const RESPONSE_START_MANAGER_CALL_POST_USER_DETAILS_TO_IUSER_DETAILS_RESULT = (data: /* START - Container StartManager thunk callPostUserDetails call response scheme. */userDetailsRs.IData/* END - Container StartManager thunk callPostUserDetails call response scheme. */): Models.IUserDetailsResult => {

    /* START - Network thunk chain callPostUserDetails response data converter to IUserDetailsResult model of container StartManager. */

    const validatorErrorList: string[] = userDetailRsValidator.validateIData(data, 'userDetailRsValidator', false)
    if (validatorErrorList && validatorErrorList.length) {
        util.log(validatorErrorList)
        throw new Error('userDetailRsValidator error')
    }

    const response: Models.IUserDetailsResult = {
        ...defaultValues.IUserDetailsResult,
        id: data && data.USER && data.USER.ID ? data.USER.ID : null,
        phone: data && data.USER && data.USER.PERSONAL_PHONE ? data.USER.PERSONAL_PHONE : '',
        lastName: data && data.USER && data.USER.LAST_NAME ? data.USER.LAST_NAME : '',
        email: data && data.USER && data.USER.EMAIL ? data.USER.EMAIL : '',
        name: data && data.USER && data.USER.NAME ? data.USER.NAME : '',
        language: data && data.USER && data.USER.LANGUAGE_ID ? data.USER.LANGUAGE_ID === 'en' ? 'English' : data.USER.LANGUAGE_ID === 'de' ? 'Deutsche' : '' : '',
        bonusAmount: data && data.LOYALTY && data.LOYALTY.bonuses ? data.LOYALTY.bonuses : null,
        ufLoyaltyId: data && data.LOYALTY && data.LOYALTY.card ? data.LOYALTY.card.toString() : '', // FIXME integration
    }

    return response

    /* END - Network thunk chain callPostUserDetails response data converter to IUserDetailsResult model of container StartManager. */

}
export const REQUEST_START_MANAGER_CALL_POST_USER_DETAILS_FROM_IREQUEST_USER_DETAILS = (data: Models.IRequestUserDetails): /* START - Container StartManager thunk callPostUserDetails call request scheme. */userDetailsRq.IMain /* END - Container StartManager thunk callPostUserDetails call request scheme. */ => {

    /* START - Network thunk chain callPostUserDetails request data converter from IRequestUserDetails model of container StartManager. */

    return {}

    /* END - Network thunk chain callPostUserDetails request data converter from IRequestUserDetails model of container StartManager. */

}
export const RESPONSE_START_MANAGER_CALL_POST_LOGOUT_TO_ILOGOUT_RESULT = (data: /* START - Container StartManager thunk callPostLogout call response scheme. */logoutRs.IData/* END - Container StartManager thunk callPostLogout call response scheme. */): Models.ILogoutResult => {

    /* START - Network thunk chain callPostLogout response data converter to ILogoutResult model of container StartManager. */

    const validatorErrorList: string[] = logoutRsValidator.validateIData(data, 'logoutRsValidator', true)
    if (validatorErrorList && validatorErrorList.length) {
        util.log(validatorErrorList)
        throw new Error('logoutRsValidator error')
    }

    return {
        ...defaultValues.ILogoutResult,
        success: true
    }

    /* END - Network thunk chain callPostLogout response data converter to ILogoutResult model of container StartManager. */

}
export const REQUEST_START_MANAGER_CALL_POST_LOGOUT_FROM_IREQUEST_LOGOUT = (data: Models.IRequestLogout): /* START - Container StartManager thunk callPostLogout call request scheme. */logoutRq.IMain /* END - Container StartManager thunk callPostLogout call request scheme. */ => {

    /* START - Network thunk chain callPostLogout request data converter from IRequestLogout model of container StartManager. */

    return {}

    /* END - Network thunk chain callPostLogout request data converter from IRequestLogout model of container StartManager. */

}
export const RESPONSE_START_MANAGER_CALL_POST_VERSION_TO_IVERSION_RESULT = (data: /* START - Container StartManager thunk callPostVersion call response scheme. */versionRs.IData/* END - Container StartManager thunk callPostVersion call response scheme. */): Models.IVersionResult => {

    /* START - Network thunk chain callPostVersion response data converter to IVersionResult model of container StartManager. */

    const validatorErrorList: string[] = versionRsValidator.validateIData(data, 'versionRsValidator', true)
    if (validatorErrorList && validatorErrorList.length) {
        util.log(validatorErrorList)
        throw new Error('versionRsValidator error')
    }

    const response: Models.IVersionResult = {
        ...defaultValues.IVersionResult,
        version: data.version ? data.version : '',
        appStoreUrl: data.appStore ? data.appStore : '',
        googlePlayUrl: data.googlePlay ? data.googlePlay : '',
        instagramUrl: data.instagram ? data.instagram : '',
        facebookUrl: data.facebook ? data.facebook : '',
        whatsAppUrl: data.whatsApp ? data.whatsApp : '',
    }

    return response

    /* END - Network thunk chain callPostVersion response data converter to IVersionResult model of container StartManager. */

}
export const REQUEST_START_MANAGER_CALL_POST_VERSION_FROM_IREQUEST_VERSION = (data: Models.IRequestVersion): /* START - Container StartManager thunk callPostVersion call request scheme. */versionRq.IMain /* END - Container StartManager thunk callPostVersion call request scheme. */ => {

    /* START - Network thunk chain callPostVersion request data converter from IRequestVersion model of container StartManager. */

    const request: versionRq.IMain = {}

    // const validatorErrorList: string[] = versionRqValidator.validateIMain(request, 'versionRqValidator', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('versionRqValidator error')
    // }

    return request

    /* END - Network thunk chain callPostVersion request data converter from IRequestVersion model of container StartManager. */

}
export const RESPONSE_START_MANAGER_CALL_POST_LOG_TO_BOOLEAN = (data: /* START - Container StartManager thunk callPostLog call response scheme. */any/* END - Container StartManager thunk callPostLog call response scheme. */): boolean => {

    /* START - Network thunk chain callPostLog response data converter to boolean model of container StartManager. */

    return true

    /* END - Network thunk chain callPostLog response data converter to boolean model of container StartManager. */

}
export const REQUEST_START_MANAGER_CALL_POST_LOG_FROM_IREQUEST_LOG = (data: Models.IRequestLog): /* START - Container StartManager thunk callPostLog call request scheme. */any /* END - Container StartManager thunk callPostLog call request scheme. */ => {

    /* START - Network thunk chain callPostLog request data converter from IRequestLog model of container StartManager. */

    return {
        KEY: 'RubediteLog01$',
        APP: 'Grassica',
        LOG: data.logError,
    }

    /* END - Network thunk chain callPostLog request data converter from IRequestLog model of container StartManager. */

}
export const RESPONSE_REGISTRATION_CALL_POST_REGISTRATION_CONFIRM_TO_IREGISTRATION_CONFIRM_RESULT = (data: /* START - Container Registration thunk callPostRegistrationConfirm call response scheme. */any/* END - Container Registration thunk callPostRegistrationConfirm call response scheme. */): Models.IRegistrationConfirmResult => {

    /* START - Network thunk chain callPostRegistrationConfirm response data converter to IRegistrationConfirmResult model of container Registration. */

    return {...defaultValues.IRegistrationConfirmResult, success: data && data.success === true}

    /* END - Network thunk chain callPostRegistrationConfirm response data converter to IRegistrationConfirmResult model of container Registration. */

}
export const REQUEST_REGISTRATION_CALL_POST_REGISTRATION_CONFIRM_FROM_IREQUEST_REGISTRATION_CONFIRM = (data: Models.IRequestRegistrationConfirm): /* START - Container Registration thunk callPostRegistrationConfirm call request scheme. */any /* END - Container Registration thunk callPostRegistrationConfirm call request scheme. */ => {

    /* START - Network thunk chain callPostRegistrationConfirm request data converter from IRequestRegistrationConfirm model of container Registration. */

    return {
        email: data.email,
        phone: data.phone,
    }

    /* END - Network thunk chain callPostRegistrationConfirm request data converter from IRequestRegistrationConfirm model of container Registration. */

}
export const RESPONSE_REGISTRATION_CALL_POST_REGISTRATION_TO_IREGISTRATION_RESULT = (data: /* START - Container Registration thunk callPostRegistration call response scheme. */profileEditRs.IResult/* END - Container Registration thunk callPostRegistration call response scheme. */): Models.IRegistrationResult => {

    /* START - Network thunk chain callPostRegistration response data converter to IRegistrationResult model of container Registration. */

    return {
        ...defaultValues.IRegistrationResult,
        success: true
    }

    /* END - Network thunk chain callPostRegistration response data converter to IRegistrationResult model of container Registration. */

}
export const REQUEST_REGISTRATION_CALL_POST_REGISTRATION_FROM_IREQUEST_REGISTRATION = (data: Models.IRequestRegistration): /* START - Container Registration thunk callPostRegistration call request scheme. */profileEditRq.IMain /* END - Container Registration thunk callPostRegistration call request scheme. */ => {

    /* START - Network thunk chain callPostRegistration request data converter from IRequestRegistration model of container Registration. */

    const request: profileEditRq.IMain = {
        LANGUAGE_ID: data.language === 'English' ? 'en' : 'de',
        NAME: data.name !== null ? data.name : '',
        LAST_NAME: data.lastName !== null ? data.lastName : '',
        PERSONAL_PHONE: data.phone !== null ? data.phone : '',
        EMAIL: data.email !== null ? data.email : '',
        CONFIRMATION_CODE: data.code ? +data.code : 0
    }

    // const validatorErrorList: string[] = profileEditRqValidator.validateIMain(request, 'profileEditRqValidator', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('profileEditRqValidatorz error')
    // }

    return request


    /* END - Network thunk chain callPostRegistration request data converter from IRequestRegistration model of container Registration. */

}
export const RESPONSE_AUTHORIZATION_CALL_POST_AUTHORIZATION_TO_IAUTHORIZATION_RESULT = (data: /* START - Container Authorization thunk callPostAuthorization call response scheme. */authorizationRs.IData/* END - Container Authorization thunk callPostAuthorization call response scheme. */): Models.IAuthorizationResult => {

    /* START - Network thunk chain callPostAuthorization response data converter to IAuthorizationResult model of container Authorization. */

    const validatorErrorList: string[] = authorizationValidatorRs.validateIData(data, 'authorizationValidatorRs', true)
    if (validatorErrorList && validatorErrorList.length) {
        util.log(validatorErrorList)
        // throw new Error('authorizationValidatorRs error')
    }

    return {
        ...defaultValues.IAuthorizationResult,
        success: data.saved,
    }

    /* END - Network thunk chain callPostAuthorization response data converter to IAuthorizationResult model of container Authorization. */

}
export const REQUEST_AUTHORIZATION_CALL_POST_AUTHORIZATION_FROM_IREQUEST_AUTHORIZATION = (data: Models.IRequestAuthorization): /* START - Container Authorization thunk callPostAuthorization call request scheme. */authorizationRq.IMain /* END - Container Authorization thunk callPostAuthorization call request scheme. */ => {

    /* START - Network thunk chain callPostAuthorization request data converter from IRequestAuthorization model of container Authorization. */

    const request: authorizationRq.IMain = {
        phone: data.phone ? data.phone.replace('+', '') : '',
        email: data.email ? data.email : '',
    }

    return request

    /* END - Network thunk chain callPostAuthorization request data converter from IRequestAuthorization model of container Authorization. */

}
export const RESPONSE_AUTHORIZATION_CALL_POST_AUTHORIZATION_CONFIRM_TO_IAUTHORIZATION_CONFIRM_RESULT = (data: /* START - Container Authorization thunk callPostAuthorizationConfirm call response scheme. */authorizationCodeConfirmRs.IData/* END - Container Authorization thunk callPostAuthorizationConfirm call response scheme. */): Models.IAuthorizationConfirmResult => {

    /* START - Network thunk chain callPostAuthorizationConfirm response data converter to IAuthorizationConfirmResult model of container Authorization. */

    const validatorErrorList: string[] = authorizationCodeConfirmValidatorRs.validateIData(data, 'authorizationCodeConfirmValidatorRs', true)
    if (validatorErrorList && validatorErrorList.length) {
        util.log(validatorErrorList)
        throw new Error('authorizationCodeConfirmValidatorRs error')
    }

    const response: Models.IAuthorizationConfirmResult = {
        ...defaultValues.IAuthorizationConfirmResult,
        userData: data.USER ? {
            ...defaultValues.IUserData,
            id: data.USER.ID ? data.USER.ID : null,
            phone: data.USER.PERSONAL_PHONE ? data.USER.PERSONAL_PHONE : '',
            email: data.USER.EMAIL ? data.USER.EMAIL : '',
        } : {...defaultValues.IUserData},
        token: data.TOKEN ? data.TOKEN : '',
    }

    return response

    /* END - Network thunk chain callPostAuthorizationConfirm response data converter to IAuthorizationConfirmResult model of container Authorization. */

}
export const REQUEST_AUTHORIZATION_CALL_POST_AUTHORIZATION_CONFIRM_FROM_IREQUEST_AUTHORIZATION_CONFIRM = (data: Models.IRequestAuthorizationConfirm): /* START - Container Authorization thunk callPostAuthorizationConfirm call request scheme. */authorizationCodeConfirmRq.IMain /* END - Container Authorization thunk callPostAuthorizationConfirm call request scheme. */ => {

    /* START - Network thunk chain callPostAuthorizationConfirm request data converter from IRequestAuthorizationConfirm model of container Authorization. */

    const request: authorizationCodeConfirmRq.IMain = {
        phone: data.phone ? data.phone.replace('+', '') : '',
        email: data.email ? data.email : '',
        code: data.code,
    }

    // const validatorErrorList: string[] = authorizationCodeConfirmValidatorRq.validateIMain(request, 'authorizationCodeConfirmValidatorRq', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('authorizationCodeConfirmRq error')
    // }

    return request

    /* END - Network thunk chain callPostAuthorizationConfirm request data converter from IRequestAuthorizationConfirm model of container Authorization. */

}
export const RESPONSE_CLAIM_CALL_POST_CLAIM_TO_ICLAIM_RESULT = (data: /* START - Container Claim thunk callPostClaim call response scheme. */claimRs.IMain/* END - Container Claim thunk callPostClaim call response scheme. */): Models.IClaimResult => {

    /* START - Network thunk chain callPostClaim response data converter to IClaimResult model of container Claim. */

    return {
        ...defaultValues.IClaimResult,
        success: true,
    }

    /* END - Network thunk chain callPostClaim response data converter to IClaimResult model of container Claim. */

}
export const REQUEST_CLAIM_CALL_POST_CLAIM_FROM_IREQUEST_CLAIM = (data: Models.IRequestClaim): /* START - Container Claim thunk callPostClaim call request scheme. */claimRq.IMain /* END - Container Claim thunk callPostClaim call request scheme. */ => {

    /* START - Network thunk chain callPostClaim request data converter from IRequestClaim model of container Claim. */

    const request: claimRq.IMain = {
        phone: data.phone ? data.phone.replace('+', '') : '',
        message: data.message,
        productName: data.producteName ? data.producteName : '',
        email: data.email,
    }

    // const validatorErrorList: string[] = claimRqValidator.validateIMain(request, 'claimRqValidator', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('claimRqValidator error')
    // }

    return request

    /* END - Network thunk chain callPostClaim request data converter from IRequestClaim model of container Claim. */

}
export const RESPONSE_MARKET_LIST_CALL_POST_MARKET_LIST_TO_IMARKET_LIST = (data: /* START - Container MarketList thunk callPostMarketList call response scheme. */shopListRs.IDataItem[]/* END - Container MarketList thunk callPostMarketList call response scheme. */): Models.IMarketList => {

    /* START - Network thunk chain callPostMarketList response data converter to IMarketList model of container MarketList. */

    data && data.forEach((item: shopListRs.IDataItem) => {
        let validatorErrorList: string[] = shopListRsValidator.validateIDataItem(item, 'shopListRsValidator', true)
        if (validatorErrorList && validatorErrorList.length) {
            util.log(validatorErrorList)
            throw new Error('shopListRsValidator error')
        }
    })

    const response: Models.IMarketList = {
        ...defaultValues.IMarketList,
        data: data && data.length ?
            data.map((market: any) => {

                const coordinatesArray: string[] = market.latLNG ? market.latLNG.split(',') : ''

                return {
                    ...defaultValues.IMarket,
                    address: market.addres,
                    email: market.email,
                    phone: market.phone,
                    coordinates: coordinatesArray && coordinatesArray.length > 1 ? {
                        latitude: parseFloat(coordinatesArray[0]),
                        longitude: parseFloat(coordinatesArray[1]),
                    } : null,
                }
            }) : null
    }

    return response

    /* END - Network thunk chain callPostMarketList response data converter to IMarketList model of container MarketList. */

}
export const REQUEST_MARKET_LIST_CALL_POST_MARKET_LIST_FROM_IREQUEST_MARKET_LIST = (data: Models.IRequestMarketList): /* START - Container MarketList thunk callPostMarketList call request scheme. */shopListRq.IMain /* END - Container MarketList thunk callPostMarketList call request scheme. */ => {

    /* START - Network thunk chain callPostMarketList request data converter from IRequestMarketList model of container MarketList. */

    const request: shopListRq.IMain = {
        language: data.language === 'English' ? 'en' : 'de'
    }

    return request

    /* END - Network thunk chain callPostMarketList request data converter from IRequestMarketList model of container MarketList. */

}
export const RESPONSE_QUESTION_CALL_POST_QUESTION_TO_IQUESTION_RESULT = (data: /* START - Container Question thunk callPostQuestion call response scheme. */questionRs.IMain/* END - Container Question thunk callPostQuestion call response scheme. */): Models.IQuestionResult => {

    /* START - Network thunk chain callPostQuestion response data converter to IQuestionResult model of container Question. */

    return {
        ...defaultValues.IQuestionResult,
        success: true,
    }

    /* END - Network thunk chain callPostQuestion response data converter to IQuestionResult model of container Question. */

}
export const REQUEST_QUESTION_CALL_POST_QUESTION_FROM_IREQUEST_QUESTION = (data: Models.IRequestQuestion): /* START - Container Question thunk callPostQuestion call request scheme. */questionRq.IMain /* END - Container Question thunk callPostQuestion call request scheme. */ => {

    /* START - Network thunk chain callPostQuestion request data converter from IRequestQuestion model of container Question. */

    const request: questionRq.IMain = {
        phone: data.phone ? data.phone.replace('+', '') : '',
        message: data.message,
        email: data.email,
    }

    // const validatorErrorList: string[] = questionRqValidator.validateIMain(request, 'questionRqValidator', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('questionRqValidator error')
    // }

    return request

    /* END - Network thunk chain callPostQuestion request data converter from IRequestQuestion model of container Question. */

}
export const RESPONSE_IDEA_CALL_POST_IDEA_TO_IIDEA_RESULT = (data: /* START - Container Idea thunk callPostIdea call response scheme. */ideaRs.IMain/* END - Container Idea thunk callPostIdea call response scheme. */): Models.IIdeaResult => {

    /* START - Network thunk chain callPostIdea response data converter to IIdeaResult model of container Idea. */

    return {
        ...defaultValues.IIdeaResult,
        success: true,
    }

    /* END - Network thunk chain callPostIdea response data converter to IIdeaResult model of container Idea. */

}
export const REQUEST_IDEA_CALL_POST_IDEA_FROM_IREQUEST_IDEA = (data: Models.IRequestIdea): /* START - Container Idea thunk callPostIdea call request scheme. */ideaRq.IMain /* END - Container Idea thunk callPostIdea call request scheme. */ => {

    /* START - Network thunk chain callPostIdea request data converter from IRequestIdea model of container Idea. */

    const request: ideaRq.IMain = {
        phone: data.phone ? data.phone.replace('+', '') : '',
        message: data.message,
        email: data.email,
    }

    // const validatorErrorList: string[] = ideaRqValidator.validateIMain(request, 'ideaRqValidator', true)
    // if (validatorErrorList.length > 0) {
    //     util.log(validatorErrorList)
    //     throw new Error('ideaRqValidator error')
    // }

    return request

    /* END - Network thunk chain callPostIdea request data converter from IRequestIdea model of container Idea. */

}
export const RESPONSE_CONFIRM_CALL_POST_CONFIRM_TO_ICONFIRM_RESULT = (data: /* START - Container Confirm thunk callPostConfirm call response scheme. */any/* END - Container Confirm thunk callPostConfirm call response scheme. */): Models.IConfirmResult => {

    /* START - Network thunk chain callPostConfirm response data converter to IConfirmResult model of container Confirm. */

    return {...defaultValues.IConfirmResult, success: true}

    /* END - Network thunk chain callPostConfirm response data converter to IConfirmResult model of container Confirm. */

}
export const REQUEST_CONFIRM_CALL_POST_CONFIRM_FROM_IREQUEST_CONFIRM = (data: Models.IRequestConfirm): /* START - Container Confirm thunk callPostConfirm call request scheme. */any /* END - Container Confirm thunk callPostConfirm call request scheme. */ => {

    /* START - Network thunk chain callPostConfirm request data converter from IRequestConfirm model of container Confirm. */

    return {
        code: data.code,
        email: data.email ? data.email : null,
        phone: data.phone ? data.phone : null,
    }

    /* END - Network thunk chain callPostConfirm request data converter from IRequestConfirm model of container Confirm. */

}
export const RESPONSE_PROFILE_CALL_POST_PROFILE_CONFIRM_TO_IPROFILE_CONFIRM_RESULT = (data: /* START - Container Profile thunk callPostProfileConfirm call response scheme. */any/* END - Container Profile thunk callPostProfileConfirm call response scheme. */): Models.IProfileConfirmResult => {

    /* START - Network thunk chain callPostProfileConfirm response data converter to IProfileConfirmResult model of container Profile. */

    return RESPONSE_REGISTRATION_CALL_POST_REGISTRATION_CONFIRM_TO_IREGISTRATION_CONFIRM_RESULT(data)

    /* END - Network thunk chain callPostProfileConfirm response data converter to IProfileConfirmResult model of container Profile. */

}
export const REQUEST_PROFILE_CALL_POST_PROFILE_CONFIRM_FROM_IREQUEST_PROFILE_CONFIRM = (data: Models.IRequestProfileConfirm): /* START - Container Profile thunk callPostProfileConfirm call request scheme. */any /* END - Container Profile thunk callPostProfileConfirm call request scheme. */ => {

    /* START - Network thunk chain callPostProfileConfirm request data converter from IRequestProfileConfirm model of container Profile. */

    return REQUEST_REGISTRATION_CALL_POST_REGISTRATION_CONFIRM_FROM_IREQUEST_REGISTRATION_CONFIRM(data)

    /* END - Network thunk chain callPostProfileConfirm request data converter from IRequestProfileConfirm model of container Profile. */

}
export const RESPONSE_PROFILE_CALL_POST_PROFILE_TO_IPROFILE_RESULT = (data: /* START - Container Profile thunk callPostProfile call response scheme. */any/* END - Container Profile thunk callPostProfile call response scheme. */): Models.IProfileResult => {

    /* START - Network thunk chain callPostProfile response data converter to IProfileResult model of container Profile. */

    return RESPONSE_REGISTRATION_CALL_POST_REGISTRATION_TO_IREGISTRATION_RESULT(data)

    /* END - Network thunk chain callPostProfile response data converter to IProfileResult model of container Profile. */

}
export const REQUEST_PROFILE_CALL_POST_PROFILE_FROM_IREQUEST_PROFILE = (data: Models.IRequestProfile): /* START - Container Profile thunk callPostProfile call request scheme. */profileEditRq.IMain /* END - Container Profile thunk callPostProfile call request scheme. */ => {

    /* START - Network thunk chain callPostProfile request data converter from IRequestProfile model of container Profile. */

    return REQUEST_REGISTRATION_CALL_POST_REGISTRATION_FROM_IREQUEST_REGISTRATION(data)

    /* END - Network thunk chain callPostProfile request data converter from IRequestProfile model of container Profile. */

}

export const CONVERT_ERROR = (data: any): Models.IError => {

    /* START - Converter to Error model. */

    let convertedError: Models.IError = {...defaultValues.IError}

    if (data && data.type && data.type === 'ServerError' && data.message) {
        convertedError.type = data.type
        convertedError.code = data.code
        convertedError.message = data.message
        convertedError.comment = data.comment
        convertedError.details = data.details
    } else {
        convertedError.type = 'LocalError'
        convertedError.code = 'LocalError'
        convertedError.message = Localization.t('grassica-ui.modal.serverErrorMessage')
        convertedError.details = data.details
    }

    return convertedError

    /* END - Converter to Error model. */

}
