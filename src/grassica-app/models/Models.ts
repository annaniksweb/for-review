/**
 * Models for all containers.
 *
 * @author Burnaev M.U.
 * @see
 */

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
/* START - Additional imports for mobile application. */
import {string} from "prop-types";
import {t} from "i18n-js";
/* END - Additional imports for mobile application. */

export class DefaultValues {
    boolean: boolean = false
    number: number = 0
    string: string = ''
    IStartManagerContext: Models.IStartManagerContext = {
        /* START - IStartManagerContext model default value. */
        // TODO Describe IStartManagerContext model default value.
        /* END - IStartManagerContext model default value. */
    }
    IStartManagerInstance: Models.IStartManagerInstance = {
        /* START - IStartManagerInstance model default value. */
        // TODO Describe IStartManagerInstance model default value.
        /* END - IStartManagerInstance model default value. */
    }
    IUserDetailsResult: Models.IUserDetailsResult = {
        /* START - IUserDetailsResult model default value. */
        id: null,
        bonusAmount: null,
        ufLoyaltyId: '',
        phone: '',
        lastName: '',
        email: '',
        name: '',
        language: '',
        /* END - IUserDetailsResult model default value. */
    }
    IRequestUserDetails: Models.IRequestUserDetails = {
        /* START - IRequestUserDetails model default value. */
        token: null,
        tokenApi: null,
        /* END - IRequestUserDetails model default value. */
    }
    ICredentials: Models.ICredentials = {
        /* START - ICredentials model default value. */
        token: '',
        /* END - ICredentials model default value. */
    }
    ILogoutResult: Models.ILogoutResult = {
        /* START - ILogoutResult model default value. */
        success: false,
        /* END - ILogoutResult model default value. */
    }
    IRequestLogout: Models.IRequestLogout = {
        /* START - IRequestLogout model default value. */
        token: null,
        tokenApi: null,
        /* END - IRequestLogout model default value. */
    }
    IVersionResult: Models.IVersionResult = {
        /* START - IVersionResult model default value. */
        version: '',
        appStoreUrl: '',
        googlePlayUrl: '',
        instagramUrl: '',
        facebookUrl: '',
        whatsAppUrl: '',
        /* END - IVersionResult model default value. */
    }
    IRequestVersion: Models.IRequestVersion = {
        /* START - IRequestVersion model default value. */
        token: null,
        tokenApi: null,
        /* END - IRequestVersion model default value. */
    }
    IRequestLog: Models.IRequestLog = {
        /* START - IRequestLog model default value. */
        logError: '',
        /* END - IRequestLog model default value. */
    }
    IStartManagerContainerExecute: Models.IStartManagerContainerExecute = {
        /* START - IStartManagerContainerExecute model default value. */
        // TODO Describe IStartManagerContainerExecute model default value.
        /* END - IStartManagerContainerExecute model default value. */
    }
    IStartManagerPresenter: Models.IStartManagerPresenter = {
        /* START - IStartManagerPresenter model default value. */
        language: '',
        /* END - IStartManagerPresenter model default value. */
    }
    ISplashContext: Models.ISplashContext = {
        /* START - ISplashContext model default value. */
        // TODO Describe ISplashContext model default value.
        /* END - ISplashContext model default value. */
    }
    ISplashInstance: Models.ISplashInstance = {
        /* START - ISplashInstance model default value. */
        // TODO Describe ISplashInstance model default value.
        /* END - ISplashInstance model default value. */
    }
    ISplashContainerExecute: Models.ISplashContainerExecute = {
        /* START - ISplashContainerExecute model default value. */
        // TODO Describe ISplashContainerExecute model default value.
        /* END - ISplashContainerExecute model default value. */
    }
    ISplashPresenter: Models.ISplashPresenter = {
        /* START - ISplashPresenter model default value. */
        // TODO Describe ISplashPresenter model default value.
        /* END - ISplashPresenter model default value. */
    }
    IRegistrationValidationErrors: Models.IRegistrationValidationErrors = {
        /* START - IRegistrationValidationErrors model default value. */
        inputNameError: null,
        inputLastNameError: null,
        inputPhoneError: null,
        inputEmailError: null,
        inputLanguageError: null,
        inputPasswordError: null,
        inputPasswordConfirmError: null,
        /* END - IRegistrationValidationErrors model default value. */
    }
    IRegistrationContext: Models.IRegistrationContext = {
        /* START - IRegistrationContext model default value. */
        // TODO Describe IRegistrationContext model default value.
        /* END - IRegistrationContext model default value. */
    }
    IRegistrationInstance: Models.IRegistrationInstance = {
        /* START - IRegistrationInstance model default value. */
        // TODO Describe IRegistrationInstance model default value.
        /* END - IRegistrationInstance model default value. */
    }
    IRegistrationConfirmResult: Models.IRegistrationConfirmResult = {
        /* START - IRegistrationConfirmResult model default value. */
        success: false,
        /* END - IRegistrationConfirmResult model default value. */
    }
    IRequestRegistrationConfirm: Models.IRequestRegistrationConfirm = {
        /* START - IRequestRegistrationConfirm model default value. */
        phone: null,
        email: null,
        token: null,
        tokenApi: null,
        /* END - IRequestRegistrationConfirm model default value. */
    }
    IRegistrationResult: Models.IRegistrationResult = {
        /* START - IRegistrationResult model default value. */
        success: false,
        /* END - IRegistrationResult model default value. */
    }
    IRequestRegistration: Models.IRequestRegistration = {
        /* START - IRequestRegistration model default value. */
        token: null,
        tokenApi: null,
        name: null,
        phone: null,
        email: null,
        lastName: null,
        userData: null,
        language: null,
        code: null,
        /* END - IRequestRegistration model default value. */
    }
    IRegistrationContainerExecute: Models.IRegistrationContainerExecute = {
        /* START - IRegistrationContainerExecute model default value. */
        // TODO Describe IRegistrationContainerExecute model default value.
        /* END - IRegistrationContainerExecute model default value. */
    }
    IRegistrationPresenter: Models.IRegistrationPresenter = {
        /* START - IRegistrationPresenter model default value. */
        confirmSeconds: null,
        /* END - IRegistrationPresenter model default value. */
    }
    IAuthorizationValidationErrors: Models.IAuthorizationValidationErrors = {
        /* START - IAuthorizationValidationErrors model default value. */
        errorPhone: '',
        errorCode: '',
        errorEmail: '',
        /* END - IAuthorizationValidationErrors model default value. */
    }
    IAuthorizationContext: Models.IAuthorizationContext = {
        /* START - IAuthorizationContext model default value. */
        // TODO Describe IAuthorizationContext model default value.
        /* END - IAuthorizationContext model default value. */
    }
    IAuthorizationInstance: Models.IAuthorizationInstance = {
        /* START - IAuthorizationInstance model default value. */
        mode: null,
        /* END - IAuthorizationInstance model default value. */
    }
    IAuthorizationResult: Models.IAuthorizationResult = {
        /* START - IAuthorizationResult model default value. */
        success: false,
        /* END - IAuthorizationResult model default value. */
    }
    IRequestAuthorization: Models.IRequestAuthorization = {
        /* START - IRequestAuthorization model default value. */
        phone: null,
        email: null,
        token: null,
        tokenApi: null,
        /* END - IRequestAuthorization model default value. */
    }
    IAuthorizationConfirmResult: Models.IAuthorizationConfirmResult = {
        /* START - IAuthorizationConfirmResult model default value. */
        token: null,
        userData: null,
        /* END - IAuthorizationConfirmResult model default value. */
    }
    IRequestAuthorizationConfirm: Models.IRequestAuthorizationConfirm = {
        /* START - IRequestAuthorizationConfirm model default value. */
        code: '',
        phone: null,
        email: null,
        token: null,
        tokenApi: null,
        /* END - IRequestAuthorizationConfirm model default value. */
    }
    IAuthorizationContainerExecute: Models.IAuthorizationContainerExecute = {
        /* START - IAuthorizationContainerExecute model default value. */
        // TODO Describe IAuthorizationContainerExecute model default value.
        /* END - IAuthorizationContainerExecute model default value. */
    }
    IAuthorizationPresenter: Models.IAuthorizationPresenter = {
        /* START - IAuthorizationPresenter model default value. */
        // TODO Describe IAuthorizationPresenter model default value.
        /* END - IAuthorizationPresenter model default value. */
    }
    IMainContext: Models.IMainContext = {
        /* START - IMainContext model default value. */
        // TODO Describe IMainContext model default value.
        /* END - IMainContext model default value. */
    }
    IMainInstance: Models.IMainInstance = {
        /* START - IMainInstance model default value. */
        // TODO Describe IMainInstance model default value.
        /* END - IMainInstance model default value. */
    }
    IMainContainerExecute: Models.IMainContainerExecute = {
        /* START - IMainContainerExecute model default value. */
        // TODO Describe IMainContainerExecute model default value.
        /* END - IMainContainerExecute model default value. */
    }
    IMainPresenter: Models.IMainPresenter = {
        /* START - IMainPresenter model default value. */
        // TODO Describe IMainPresenter model default value.
        /* END - IMainPresenter model default value. */
    }
    IAboutUsContext: Models.IAboutUsContext = {
        /* START - IAboutUsContext model default value. */
        // TODO Describe IAboutUsContext model default value.
        /* END - IAboutUsContext model default value. */
    }
    IAboutUsInstance: Models.IAboutUsInstance = {
        /* START - IAboutUsInstance model default value. */
        // TODO Describe IAboutUsInstance model default value.
        /* END - IAboutUsInstance model default value. */
    }
    IAboutUsContainerExecute: Models.IAboutUsContainerExecute = {
        /* START - IAboutUsContainerExecute model default value. */
        // TODO Describe IAboutUsContainerExecute model default value.
        /* END - IAboutUsContainerExecute model default value. */
    }
    IAboutUsPresenter: Models.IAboutUsPresenter = {
        /* START - IAboutUsPresenter model default value. */
        // TODO Describe IAboutUsPresenter model default value.
        /* END - IAboutUsPresenter model default value. */
    }
    IClaimValidationErrors: Models.IClaimValidationErrors = {
        /* START - IClaimValidationErrors model default value. */
        inputPhoneError: null,
        inputEmailError: null,
        inputProductError: null,
        inputWhatToFixError: null,
        /* END - IClaimValidationErrors model default value. */
    }
    IClaimContext: Models.IClaimContext = {
        /* START - IClaimContext model default value. */
        // TODO Describe IClaimContext model default value.
        /* END - IClaimContext model default value. */
    }
    IClaimInstance: Models.IClaimInstance = {
        /* START - IClaimInstance model default value. */
        // TODO Describe IClaimInstance model default value.
        /* END - IClaimInstance model default value. */
    }
    IClaimResult: Models.IClaimResult = {
        /* START - IClaimResult model default value. */
        success: false,
        /* END - IClaimResult model default value. */
    }
    IRequestClaim: Models.IRequestClaim = {
        /* START - IRequestClaim model default value. */
        tokenApi: null,
        phone: null,
        message: '',
        producteName: null,
        email: '',
        token: null,
        /* END - IRequestClaim model default value. */
    }
    IClaimContainerExecute: Models.IClaimContainerExecute = {
        /* START - IClaimContainerExecute model default value. */
        // TODO Describe IClaimContainerExecute model default value.
        /* END - IClaimContainerExecute model default value. */
    }
    IClaimPresenter: Models.IClaimPresenter = {
        /* START - IClaimPresenter model default value. */
        // TODO Describe IClaimPresenter model default value.
        /* END - IClaimPresenter model default value. */
    }
    IMarket: Models.IMarket = {
        /* START - IMarket model default value. */
        address: '',
        email: '',
        phone: '',
        coordinates: null,
        /* END - IMarket model default value. */
    }
    IMarketListContext: Models.IMarketListContext = {
        /* START - IMarketListContext model default value. */
        // TODO Describe IMarketListContext model default value.
        /* END - IMarketListContext model default value. */
    }
    IMarketListInstance: Models.IMarketListInstance = {
        /* START - IMarketListInstance model default value. */
        // TODO Describe IMarketListInstance model default value.
        /* END - IMarketListInstance model default value. */
    }
    IMarketList: Models.IMarketList = {
        /* START - IMarketList model default value. */
        data: null
        /* END - IMarketList model default value. */
    }
    IRequestMarketList: Models.IRequestMarketList = {
        /* START - IRequestMarketList model default value. */
        language: 'en',
        /* END - IRequestMarketList model default value. */
    }
    IGeolocation: Models.IGeolocation = {
        /* START - IGeolocation model default value. */
        data: null
        /* END - IGeolocation model default value. */
    }
    IMarketListContainerExecute: Models.IMarketListContainerExecute = {
        /* START - IMarketListContainerExecute model default value. */
        // TODO Describe IMarketListContainerExecute model default value.
        /* END - IMarketListContainerExecute model default value. */
    }
    IMarketListPresenter: Models.IMarketListPresenter = {
        /* START - IMarketListPresenter model default value. */
        // TODO Describe IMarketListPresenter model default value.
        /* END - IMarketListPresenter model default value. */
    }
    IQrCodeContext: Models.IQrCodeContext = {
        /* START - IQrCodeContext model default value. */
        // TODO Describe IQrCodeContext model default value.
        /* END - IQrCodeContext model default value. */
    }
    IQrCodeInstance: Models.IQrCodeInstance = {
        /* START - IQrCodeInstance model default value. */
        // TODO Describe IQrCodeInstance model default value.
        /* END - IQrCodeInstance model default value. */
    }
    IQrCodeContainerExecute: Models.IQrCodeContainerExecute = {
        /* START - IQrCodeContainerExecute model default value. */
        // TODO Describe IQrCodeContainerExecute model default value.
        /* END - IQrCodeContainerExecute model default value. */
    }
    IQrCodePresenter: Models.IQrCodePresenter = {
        /* START - IQrCodePresenter model default value. */
        // TODO Describe IQrCodePresenter model default value.
        /* END - IQrCodePresenter model default value. */
    }
    IBonusContext: Models.IBonusContext = {
        /* START - IBonusContext model default value. */
        // TODO Describe IBonusContext model default value.
        /* END - IBonusContext model default value. */
    }
    IBonusInstance: Models.IBonusInstance = {
        /* START - IBonusInstance model default value. */
        // TODO Describe IBonusInstance model default value.
        /* END - IBonusInstance model default value. */
    }
    IBonusContainerExecute: Models.IBonusContainerExecute = {
        /* START - IBonusContainerExecute model default value. */
        // TODO Describe IBonusContainerExecute model default value.
        /* END - IBonusContainerExecute model default value. */
    }
    IBonusPresenter: Models.IBonusPresenter = {
        /* START - IBonusPresenter model default value. */
        // TODO Describe IBonusPresenter model default value.
        /* END - IBonusPresenter model default value. */
    }
    IQuestionValidationErrors: Models.IQuestionValidationErrors = {
        /* START - IQuestionValidationErrors model default value. */
        inputEmailError: null,
        inputQuestionError: null,
        /* END - IQuestionValidationErrors model default value. */
    }
    IQuestionContext: Models.IQuestionContext = {
        /* START - IQuestionContext model default value. */
        // TODO Describe IQuestionContext model default value.
        /* END - IQuestionContext model default value. */
    }
    IQuestionInstance: Models.IQuestionInstance = {
        /* START - IQuestionInstance model default value. */
        // TODO Describe IQuestionInstance model default value.
        /* END - IQuestionInstance model default value. */
    }
    IQuestionResult: Models.IQuestionResult = {
        /* START - IQuestionResult model default value. */
        success: false,
        /* END - IQuestionResult model default value. */
    }
    IRequestQuestion: Models.IRequestQuestion = {
        /* START - IRequestQuestion model default value. */
        tokenApi: null,
        phone: null,
        message: '',
        email: '',
        token: null,
        /* END - IRequestQuestion model default value. */
    }
    IQuestionContainerExecute: Models.IQuestionContainerExecute = {
        /* START - IQuestionContainerExecute model default value. */
        // TODO Describe IQuestionContainerExecute model default value.
        /* END - IQuestionContainerExecute model default value. */
    }
    IQuestionPresenter: Models.IQuestionPresenter = {
        /* START - IQuestionPresenter model default value. */
        // TODO Describe IQuestionPresenter model default value.
        /* END - IQuestionPresenter model default value. */
    }
    IIdeaValidationErrors: Models.IIdeaValidationErrors = {
        /* START - IIdeaValidationErrors model default value. */
        inputEmailError: null,
        inputIdeaError: null,
        /* END - IIdeaValidationErrors model default value. */
    }
    IIdeaContext: Models.IIdeaContext = {
        /* START - IIdeaContext model default value. */
        // TODO Describe IIdeaContext model default value.
        /* END - IIdeaContext model default value. */
    }
    IIdeaInstance: Models.IIdeaInstance = {
        /* START - IIdeaInstance model default value. */
        // TODO Describe IIdeaInstance model default value.
        /* END - IIdeaInstance model default value. */
    }
    IIdeaResult: Models.IIdeaResult = {
        /* START - IIdeaResult model default value. */
        success: false,
        /* END - IIdeaResult model default value. */
    }
    IRequestIdea: Models.IRequestIdea = {
        /* START - IRequestIdea model default value. */
        tokenApi: null,
        phone: null,
        message: '',
        email: '',
        token: null,
        /* END - IRequestIdea model default value. */
    }
    IIdeaContainerExecute: Models.IIdeaContainerExecute = {
        /* START - IIdeaContainerExecute model default value. */
        // TODO Describe IIdeaContainerExecute model default value.
        /* END - IIdeaContainerExecute model default value. */
    }
    IIdeaPresenter: Models.IIdeaPresenter = {
        /* START - IIdeaPresenter model default value. */
        // TODO Describe IIdeaPresenter model default value.
        /* END - IIdeaPresenter model default value. */
    }
    IConfirmValidationErrors: Models.IConfirmValidationErrors = {
        /* START - IConfirmValidationErrors model default value. */
        inputCodeError: null,
        /* END - IConfirmValidationErrors model default value. */
    }
    IConfirmContext: Models.IConfirmContext = {
        /* START - IConfirmContext model default value. */
        // TODO Describe IConfirmContext model default value.
        /* END - IConfirmContext model default value. */
    }
    IConfirmInstance: Models.IConfirmInstance = {
        /* START - IConfirmInstance model default value. */
        email: null,
        phone: null,
        /* END - IConfirmInstance model default value. */
    }
    IConfirmResult: Models.IConfirmResult = {
        /* START - IConfirmResult model default value. */
        success: false,
        /* END - IConfirmResult model default value. */
    }
    IRequestConfirm: Models.IRequestConfirm = {
        /* START - IRequestConfirm model default value. */
        tokenApi: null,
        token: null,
        email: null,
        phone: null,
        code: '',
        /* END - IRequestConfirm model default value. */
    }
    IConfirmContainerExecute: Models.IConfirmContainerExecute = {
        /* START - IConfirmContainerExecute model default value. */
        // TODO Describe IConfirmContainerExecute model default value.
        /* END - IConfirmContainerExecute model default value. */
    }
    IConfirmPresenter: Models.IConfirmPresenter = {
        /* START - IConfirmPresenter model default value. */
        // TODO Describe IConfirmPresenter model default value.
        /* END - IConfirmPresenter model default value. */
    }
    IProfileValidationErrors: Models.IProfileValidationErrors = {
        /* START - IProfileValidationErrors model default value. */
        inputNameError: null,
        inputLastNameError: null,
        inputPhoneError: null,
        inputEmailError: null,
        inputLanguageError: null,
        inputPasswordError: null,
        inputPasswordConfirmError: null,
        /* END - IProfileValidationErrors model default value. */
    }
    IProfileContext: Models.IProfileContext = {
        /* START - IProfileContext model default value. */
        // TODO Describe IProfileContext model default value.
        /* END - IProfileContext model default value. */
    }
    IProfileInstance: Models.IProfileInstance = {
        /* START - IProfileInstance model default value. */
        // TODO Describe IProfileInstance model default value.
        /* END - IProfileInstance model default value. */
    }
    IProfileConfirmResult: Models.IProfileConfirmResult = {
        /* START - IProfileConfirmResult model default value. */
        success: false,
        /* END - IProfileConfirmResult model default value. */
    }
    IRequestProfileConfirm: Models.IRequestProfileConfirm = {
        /* START - IRequestProfileConfirm model default value. */
        email: null,
        phone: null,
        token: null,
        tokenApi: null,
        /* END - IRequestProfileConfirm model default value. */
    }
    IProfileResult: Models.IProfileResult = {
        /* START - IProfileResult model default value. */
        success: false,
        /* END - IProfileResult model default value. */
    }
    IRequestProfile: Models.IRequestProfile = {
        /* START - IRequestProfile model default value. */
        token: null,
        tokenApi: null,
        name: null,
        phone: null,
        email: null,
        lastName: null,
        userData: null,
        language: null,
        code: null,
        /* END - IRequestProfile model default value. */
    }
    IProfileContainerExecute: Models.IProfileContainerExecute = {
        /* START - IProfileContainerExecute model default value. */
        // TODO Describe IProfileContainerExecute model default value.
        /* END - IProfileContainerExecute model default value. */
    }
    IProfilePresenter: Models.IProfilePresenter = {
        /* START - IProfilePresenter model default value. */
        confirmSeconds: null,
        /* END - IProfilePresenter model default value. */
    }
    IDataSecurityContext: Models.IDataSecurityContext = {
        /* START - IDataSecurityContext model default value. */
        // TODO Describe IDataSecurityContext model default value.
        /* END - IDataSecurityContext model default value. */
    }
    IDataSecurityInstance: Models.IDataSecurityInstance = {
        /* START - IDataSecurityInstance model default value. */
        // TODO Describe IDataSecurityInstance model default value.
        /* END - IDataSecurityInstance model default value. */
    }
    IDataSecurityContainerExecute: Models.IDataSecurityContainerExecute = {
        /* START - IDataSecurityContainerExecute model default value. */
        // TODO Describe IDataSecurityContainerExecute model default value.
        /* END - IDataSecurityContainerExecute model default value. */
    }
    IDataSecurityPresenter: Models.IDataSecurityPresenter = {
        /* START - IDataSecurityPresenter model default value. */
        // TODO Describe IDataSecurityPresenter model default value.
        /* END - IDataSecurityPresenter model default value. */
    }

    IError: Models.IError = {
        type: 'None',
        code: '',
        message: '',
        comment: '',
        details: '',
    }

    /* START - Default values for additional models. */
    IRouteStartManager: Models.IRouteStartManager = {
        data: [{type: 'Splash', instance: {}}],
    }
    IUserData: Models.IUserData = {
        id: null,
        bonusAmount: null,
        ufLoyaltyId: '',
        phone: '',
        lastName: '',
        email: '',
        name: '',
    }
    /* END - Default values for additional models. */
}

export const defaultValues = new DefaultValues()
