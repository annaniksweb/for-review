/**
 * Models for AboutUs container.
 *
 * @author Burnaev M.U.
 * @see
 */

import {defaultValues} from './Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import {IAboutUsState} from 'grassica-app'

/* START - Models used in container AboutUs. */

// TODO Describe models used in AboutUs actions and thunks.

/* END - Models used in container AboutUs. */

const defaultState = {
    get state(): IAboutUsState {
        return {
            containerExecute: defaultValues.IAboutUsContainerExecute,  // Result for "performContainerExecute" thunk.
            containerExecutePhase: 'Never',  // Progress indicator for thunk chain "performContainerExecute".
            containerExecuteOperationId: null,  // Unique identifier of started thunk chain "performContainerExecute".
            containerExecuteError: null,  // Error info for thunk chain "performContainerExecute".
            presenter: defaultValues.IAboutUsPresenter,  // Result for "presenterUpdate" thunk.
            presenterPhase: 'Never',  // Progress indicator for thunk chain "presenterUpdate".
            presenterOperationId: null,  // Unique identifier of started thunk chain "presenterUpdate".
            presenterError: null,  // Error info for thunk chain "presenterUpdate".
            context: defaultValues.IAboutUsContext,  // Container "AboutUs" context parameters.
            instanceId: '',  // Container "AboutUs" instance identity hash.
            instance: defaultValues.IAboutUsInstance,  // Container "AboutUs" instance parameters.

            /* START - Reducer AboutUs default state values for additional fields. */
            // TODO Describe AboutUs reducer state.
            /* END - Reducer AboutUs default state values for additional fields. */
        }
    }
}

export const initialState = {
    get state(): IAboutUsState {
        return defaultState.state
    },
}

export type IPerformContainerExecute = (instanceType: Enums.ContainerAboutUsInstanceType) => void
export type IPerformContainerReset = (instanceType: Enums.ContainerAboutUsInstanceType) => void
export type IPerformContainerRecover = (instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState) => void
export type IPerformContainerInit = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => void
export type IPerformContainerRefresh = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => void
export type IPresenterUpdate = (instanceType: Enums.ContainerAboutUsInstanceType) => void
