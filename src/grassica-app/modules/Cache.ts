import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'

import {AsyncStorage} from 'react-native'

const CACHE_ASYNC_STORAGE_PREFIX = 'SESSION_CACHE_grassicaApp_'

declare const __DEV__: boolean
import * as util from '../common/Util'

interface ICacheRequest {
    url: string,
    header?: string,
    data: string,
    cacheDate: Date,
    tagList: Models.ICacheTag[],
}

interface ICacheSession {
    user: string,
    store: ICacheRequest[],
}

let currentUser: string = ''
let currentCacheSession: ICacheSession = {user: currentUser, store: []}
let cacheTTL: number = 1000 * 60 * 60 * 24

export const responsePersist = (url: Models.IUrl, data: any, header: string): void => {
    if (url.tagList) {
        for (let i = 0; i < url.tagList.length; i++) {
            if (url.tagList[i].tag === 'Disabled') {
                return
            }
        }
    }

    util.log('Response persisted to cache.', url, data)

    currentCacheSession.store.push({
        url: url.url,
        header: JSON.stringify(header),
        data: JSON.stringify(data),
        cacheDate: new Date(),
        tagList: url.tagList,
    })

    AsyncStorage.setItem(CACHE_ASYNC_STORAGE_PREFIX + currentUser, JSON.stringify(currentCacheSession))
}

export const responseRecover = (url: Models.IUrl, data: any, header: any): any => {
    if (url.tagList) {
        for (let i = 0; i < url.tagList.length; i++) {
            if (url.tagList[i].tag === 'Disabled') {
                return null
            }
        }
    }

    sessionResetTag({tag: ''})

    let result = currentCacheSession.store.find((cacheRequest) => {
        return (cacheRequest.url === url.url && cacheRequest.data === JSON.stringify(data) && cacheRequest.header === JSON.stringify(header))
    })

    if (result) {
        util.log('Response recovered from cache.', url, JSON.parse(result.data))
    }

    return result ? JSON.parse(result.data) : null
}

const isCacheValid = (cacheRequest: ICacheRequest, tag: string | Enums.CacheContext | Enums.CachePolicy): boolean => {

    // Remove cache with CachePolicy Disabled tag.
    if (tag === 'Disabled') {
        return false
    }

    // Remove cache with CachePolicy Default tag and expired cache.
    if (tag === 'Default' && (new Date().getTime() > new Date(cacheRequest.cacheDate).getTime() + cacheTTL)) {
        util.log('Cache removed for request ' + cacheRequest.url + '.', currentCacheSession)
        return false
    }

    return true
}

export const sessionResetTag = (findTag: Models.ICacheTag): void => {
    currentCacheSession.store = currentCacheSession.store.filter((cacheRequest) => {
        if (cacheRequest.tagList.find((tag) => (tag.tag === findTag.tag && tag.contextId === findTag.contextId) || !isCacheValid(cacheRequest, tag.tag))) {
            util.log('Cache removed for request ' + cacheRequest.url + '.', currentCacheSession)
            return false
        } else {
            return true
        }
    })
}

export const sessionResetTagList = (findTagList: Models.ICacheTag[]): void => {
    currentCacheSession.store = currentCacheSession.store.filter((cacheRequest) => {

        let tagListFound = true

        findTagList.map((findTag) => {

            if (cacheRequest.tagList.find((tag) => (tag.tag === findTag.tag && tag.contextId === findTag.contextId))) {
                util.log('Cache removed for request ' + cacheRequest.url + '.', currentCacheSession)
                tagListFound = false
            }

        })

        return !tagListFound

    })
}

export const sessionStart = (user: string, onSuccess: Function): void => {
    currentUser = user
    currentCacheSession = {user: currentUser, store: []}

    AsyncStorage.getItem(CACHE_ASYNC_STORAGE_PREFIX + user).then((data) => {
        if (data !== null) {
            currentCacheSession = JSON.parse(data)
            util.log('Cache restored.', currentCacheSession)
        }

        onSuccess()

    })
}

export const sessionReset = (user: string): void => {
    currentCacheSession = {user: currentUser, store: []}

    AsyncStorage.removeItem(CACHE_ASYNC_STORAGE_PREFIX + user)
}
