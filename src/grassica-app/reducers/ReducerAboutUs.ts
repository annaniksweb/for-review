/*
 * Created by Burnaev M.U.
 */

import {handleActions, Action} from 'redux-actions'

import * as actionsAboutUs from '../actions/ActionsAboutUs'
import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

import * as util from '../common/Util'
import {IStartManagerState} from 'grassica-app'
import {ISplashState} from 'grassica-app'
import {IRegistrationState} from 'grassica-app'
import {IAuthorizationState} from 'grassica-app'
import {IMainState} from 'grassica-app'
import {IAboutUsState} from 'grassica-app'
import {IClaimState} from 'grassica-app'
import {IMarketListState} from 'grassica-app'
import {IQrCodeState} from 'grassica-app'
import {IBonusState} from 'grassica-app'
import {IQuestionState} from 'grassica-app'
import {IIdeaState} from 'grassica-app'
import {IConfirmState} from 'grassica-app'
import {IProfileState} from 'grassica-app'
import {IDataSecurityState} from 'grassica-app'
const handleActionsCustom = handleActions as any  // FIXME Types 'redux-actions' error.

/* START - Reducer AboutUs additional imports and module code. */

/* END - Reducer AboutUs additional imports and module code. */

const reducerAboutUs = handleActionsCustom({
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
    [actionsAboutUs.PERFORM_CONTAINER_EXECUTE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_EXECUTE_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE')

        let result: IAboutUsState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_EXECUTE', 100)

        return result
    },
    // Action dispatched on thunk chain 'performContainerExecute' started. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
    [actionsAboutUs.PERFORM_CONTAINER_EXECUTE_EXECUTE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_EXECUTE_EXECUTE_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            containerExecutePhase: 'InProgress',
            containerExecuteOperationId: action.payload.operationId,
            containerExecuteError: null,
        }
    },
    // Action dispatched on success in thunk 'performContainerExecute'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
    [actionsAboutUs.PERFORM_CONTAINER_EXECUTE_SUCCESS]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_EXECUTE_SUCCESS_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            containerExecute: action.payload.data,
            containerExecutePhase: 'Success',
            containerExecuteOperationId: null,
            containerExecuteError: null,
        }
    },
    // Action dispatched on failure in thunk 'performContainerExecute'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
    [actionsAboutUs.PERFORM_CONTAINER_EXECUTE_FAILURE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_EXECUTE_FAILURE_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            containerExecutePhase: 'Failure',
            containerExecuteOperationId: null,
            containerExecuteError: action.payload.error,
        }
    },
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
    [actionsAboutUs.PERFORM_CONTAINER_RESET]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_RESET_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_RESET')

        let result: IAboutUsState = {
            ...ModelsAboutUs.initialState.state,
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_RESET', 100)

        return result
    },
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
    [actionsAboutUs.PERFORM_CONTAINER_RECOVER]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_RECOVER_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_RECOVER')

        let result: IAboutUsState = {
            ...action.payload.stateRecovering,
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_RECOVER', 100)

        return result
    },
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched to init container state.
    [actionsAboutUs.PERFORM_CONTAINER_INIT]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_INIT_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_INIT')

        let result: IAboutUsState = {
            ...ModelsAboutUs.initialState.state,
            context: action.payload.context,
            instance: action.payload.instance,
            instanceId: JSON.stringify(action.payload.instance),
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_INIT', 100)

        return result
    },
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched to refresh container state.
    [actionsAboutUs.PERFORM_CONTAINER_REFRESH]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PERFORM_CONTAINER_REFRESH_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_REFRESH')

        let result: IAboutUsState = {
            ...state,
            context: action.payload.context,
            instance: action.payload.instance,
            instanceId: JSON.stringify(action.payload.instance),
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PERFORM_CONTAINER_REFRESH', 100)

        return result
    },
    // Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAboutUs.PRESENTER_UPDATE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PRESENTER_UPDATE_PAYLOAD>): IAboutUsState => {

        util.performanceCheckStart('REDUCER: ABOUT_US_CONTAINER_PRESENTER_UPDATE')

        let result: IAboutUsState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: ABOUT_US_CONTAINER_PRESENTER_UPDATE', 100)

        return result
    },
    // Action dispatched on thunk chain 'presenterUpdate' started. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAboutUs.PRESENTER_UPDATE_EXECUTE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PRESENTER_UPDATE_EXECUTE_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            presenterPhase: 'InProgress',
            presenterOperationId: action.payload.operationId,
            presenterError: null,
        }
    },
    // Action dispatched on success in thunk 'presenterUpdate'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAboutUs.PRESENTER_UPDATE_SUCCESS]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PRESENTER_UPDATE_SUCCESS_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            presenter: action.payload.data,
            presenterPhase: 'Success',
            presenterOperationId: null,
            presenterError: null,
        }
    },
    // Action dispatched on failure in thunk 'presenterUpdate'. Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAboutUs.PRESENTER_UPDATE_FAILURE]: (state: IAboutUsState, action: Models.IAction<actionsAboutUs.PRESENTER_UPDATE_FAILURE_PAYLOAD>): IAboutUsState => {
        return {
            ...state,
            presenterPhase: 'Failure',
            presenterOperationId: null,
            presenterError: action.payload.error,
        }
    },
}, ModelsAboutUs.initialState.state)

export default reducerAboutUs
