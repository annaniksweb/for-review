/*
 * Created by Burnaev M.U.
 */

import {handleActions, Action} from 'redux-actions'

import * as actionsAuthorization from '../actions/ActionsAuthorization'
import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'
import * as ModelsStartManager from '../models/ModelsStartManager'
import * as ModelsSplash from '../models/ModelsSplash'
import * as ModelsRegistration from '../models/ModelsRegistration'
import * as ModelsAuthorization from '../models/ModelsAuthorization'
import * as ModelsMain from '../models/ModelsMain'
import * as ModelsAboutUs from '../models/ModelsAboutUs'
import * as ModelsClaim from '../models/ModelsClaim'
import * as ModelsMarketList from '../models/ModelsMarketList'
import * as ModelsQrCode from '../models/ModelsQrCode'
import * as ModelsBonus from '../models/ModelsBonus'
import * as ModelsQuestion from '../models/ModelsQuestion'
import * as ModelsIdea from '../models/ModelsIdea'
import * as ModelsConfirm from '../models/ModelsConfirm'
import * as ModelsProfile from '../models/ModelsProfile'
import * as ModelsDataSecurity from '../models/ModelsDataSecurity'

import * as util from '../common/Util'
import {IStartManagerState} from 'grassica-app'
import {ISplashState} from 'grassica-app'
import {IRegistrationState} from 'grassica-app'
import {IAuthorizationState} from 'grassica-app'
import {IMainState} from 'grassica-app'
import {IAboutUsState} from 'grassica-app'
import {IClaimState} from 'grassica-app'
import {IMarketListState} from 'grassica-app'
import {IQrCodeState} from 'grassica-app'
import {IBonusState} from 'grassica-app'
import {IQuestionState} from 'grassica-app'
import {IIdeaState} from 'grassica-app'
import {IConfirmState} from 'grassica-app'
import {IProfileState} from 'grassica-app'
import {IDataSecurityState} from 'grassica-app'
const handleActionsCustom = handleActions as any  // FIXME Types 'redux-actions' error.

/* START - Reducer Authorization additional imports and module code. */

/* END - Reducer Authorization additional imports and module code. */

const reducerAuthorization = handleActionsCustom({
    // Thunk dispatched by "Authorization" screen. Thunk chain used to change authorization mode.
    [actionsAuthorization.PERFORM_INPUT_AUTHORIZATION_MODE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_INPUT_AUTHORIZATION_MODE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_AUTHORIZATION_MODE')

        let result: IAuthorizationState = {
            ...state,
            authorizationMode: action.payload.mode,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_AUTHORIZATION_MODE', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk chain used to input value.
    [actionsAuthorization.PERFORM_INPUT_PHONE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_INPUT_PHONE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_PHONE')

        let result: IAuthorizationState = {
            ...state,
            inputPhone: action.payload.phone,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_PHONE', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk chain used to input value.
    [actionsAuthorization.PERFORM_INPUT_EMAIL]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_INPUT_EMAIL_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_EMAIL')

        let result: IAuthorizationState = {
            ...state,
            inputEmail: action.payload.email,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_EMAIL', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk chain used to input value.
    [actionsAuthorization.PERFORM_INPUT_CODE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_INPUT_CODE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_CODE')

        let result: IAuthorizationState = {
            ...state,
            inputCode: action.payload.code,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_INPUT_CODE', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched on user change value.
    [actionsAuthorization.PERFORM_VALIDATE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_VALIDATE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_VALIDATE')

        let result: IAuthorizationState = {
            ...state,
            validationErrors: util.isAuthorizationValid(state),
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_VALIDATE', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Request authorization.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_RESET]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_RESET_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_RESET')

        let result: IAuthorizationState = {
            ...state,
            authorizationError: null,
            authorizationPhase: 'Never',
            authorizationOperationId: null,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_RESET', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Request authorization.
    [actionsAuthorization.CALL_POST_AUTHORIZATION]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION')

        let result: IAuthorizationState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION', 100)

        return result
    },
    // Action dispatched on network thunk chain 'callPostAuthorization' started. Thunk dispatched by "Authorization" screen. Request authorization.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_REQUEST]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_REQUEST_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorizationPhase: 'InProgress',
            authorizationOperationId: action.payload.operationId,
            authorizationError: null,
        }
    },
    // Action dispatched on fetch succeeded in thunk 'callPostAuthorization'. Thunk dispatched by "Authorization" screen. Request authorization.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_SUCCESS]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_SUCCESS_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorization: action.payload.response.data,
            authorizationCachedDate: action.payload.response.cachedDate,
            authorizationPhase: 'Success',
            authorizationOperationId: null,
            authorizationError: null,
        }
    },
    // Action dispatched on fetch failure in thunk 'callPostAuthorization'. Thunk dispatched by "Authorization" screen. Request authorization.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_FAILURE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_FAILURE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorizationPhase: 'Failure',
            authorizationOperationId: null,
            authorizationError: action.payload.error,
        }
    },
    // Thunk dispatched by "Authorization" screen. Send authorization code confirmation.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_RESET]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_RESET_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_CONFIRM_RESET')

        let result: IAuthorizationState = {
            ...state,
            authorizationConfirmError: null,
            authorizationConfirmPhase: 'Never',
            authorizationConfirmOperationId: null,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_CONFIRM_RESET', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Send authorization code confirmation.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_CONFIRM')

        let result: IAuthorizationState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_CALL_POST_AUTHORIZATION_CONFIRM', 100)

        return result
    },
    // Action dispatched on network thunk chain 'callPostAuthorizationConfirm' started. Thunk dispatched by "Authorization" screen. Send authorization code confirmation.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_REQUEST]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_REQUEST_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorizationConfirmPhase: 'InProgress',
            authorizationConfirmOperationId: action.payload.operationId,
            authorizationConfirmError: null,
        }
    },
    // Action dispatched on fetch succeeded in thunk 'callPostAuthorizationConfirm'. Thunk dispatched by "Authorization" screen. Send authorization code confirmation.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_SUCCESS]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_SUCCESS_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorizationConfirm: action.payload.response.data,
            authorizationConfirmCachedDate: action.payload.response.cachedDate,
            authorizationConfirmPhase: 'Success',
            authorizationConfirmOperationId: null,
            authorizationConfirmError: null,
        }
    },
    // Action dispatched on fetch failure in thunk 'callPostAuthorizationConfirm'. Thunk dispatched by "Authorization" screen. Send authorization code confirmation.
    [actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_FAILURE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.CALL_POST_AUTHORIZATION_CONFIRM_FAILURE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            authorizationConfirmPhase: 'Failure',
            authorizationConfirmOperationId: null,
            authorizationConfirmError: action.payload.error,
        }
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched on to start Container life cycle.
    [actionsAuthorization.PERFORM_CONTAINER_EXECUTE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_EXECUTE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_EXECUTE')

        let result: IAuthorizationState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_EXECUTE', 100)

        return result
    },
    // Action dispatched on thunk chain 'performContainerExecute' started. Thunk dispatched by "Authorization" screen. Thunk dispatched on to start Container life cycle.
    [actionsAuthorization.PERFORM_CONTAINER_EXECUTE_EXECUTE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_EXECUTE_EXECUTE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            containerExecutePhase: 'InProgress',
            containerExecuteOperationId: action.payload.operationId,
            containerExecuteError: null,
        }
    },
    // Action dispatched on success in thunk 'performContainerExecute'. Thunk dispatched by "Authorization" screen. Thunk dispatched on to start Container life cycle.
    [actionsAuthorization.PERFORM_CONTAINER_EXECUTE_SUCCESS]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_EXECUTE_SUCCESS_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            containerExecute: action.payload.data,
            containerExecutePhase: 'Success',
            containerExecuteOperationId: null,
            containerExecuteError: null,
        }
    },
    // Action dispatched on failure in thunk 'performContainerExecute'. Thunk dispatched by "Authorization" screen. Thunk dispatched on to start Container life cycle.
    [actionsAuthorization.PERFORM_CONTAINER_EXECUTE_FAILURE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_EXECUTE_FAILURE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            containerExecutePhase: 'Failure',
            containerExecuteOperationId: null,
            containerExecuteError: action.payload.error,
        }
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched to reset container state to default values.
    [actionsAuthorization.PERFORM_CONTAINER_RESET]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_RESET_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_RESET')

        let result: IAuthorizationState = {
            ...ModelsAuthorization.initialState.state,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_RESET', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched to reset container state to default values.
    [actionsAuthorization.PERFORM_CONTAINER_RECOVER]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_RECOVER_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_RECOVER')

        let result: IAuthorizationState = {
            ...action.payload.stateRecovering,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_RECOVER', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched to init container state.
    [actionsAuthorization.PERFORM_CONTAINER_INIT]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_INIT_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_INIT')

        let result: IAuthorizationState = {
            ...ModelsAuthorization.initialState.state,
            context: action.payload.context,
            instance: action.payload.instance,
            instanceId: JSON.stringify(action.payload.instance),
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_INIT', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched to refresh container state.
    [actionsAuthorization.PERFORM_CONTAINER_REFRESH]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PERFORM_CONTAINER_REFRESH_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_REFRESH')

        let result: IAuthorizationState = {
            ...state,
            context: action.payload.context,
            instance: action.payload.instance,
            instanceId: JSON.stringify(action.payload.instance),
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PERFORM_CONTAINER_REFRESH', 100)

        return result
    },
    // Thunk dispatched by "Authorization" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAuthorization.PRESENTER_UPDATE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PRESENTER_UPDATE_PAYLOAD>): IAuthorizationState => {

        util.performanceCheckStart('REDUCER: AUTHORIZATION_CONTAINER_PRESENTER_UPDATE')

        let result: IAuthorizationState = {
            ...state,
        }

        util.performanceCheckStop('REDUCER: AUTHORIZATION_CONTAINER_PRESENTER_UPDATE', 100)

        return result
    },
    // Action dispatched on thunk chain 'presenterUpdate' started. Thunk dispatched by "Authorization" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAuthorization.PRESENTER_UPDATE_EXECUTE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PRESENTER_UPDATE_EXECUTE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            presenterPhase: 'InProgress',
            presenterOperationId: action.payload.operationId,
            presenterError: null,
        }
    },
    // Action dispatched on success in thunk 'presenterUpdate'. Thunk dispatched by "Authorization" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAuthorization.PRESENTER_UPDATE_SUCCESS]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PRESENTER_UPDATE_SUCCESS_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            presenter: action.payload.data,
            presenterPhase: 'Success',
            presenterOperationId: null,
            presenterError: null,
        }
    },
    // Action dispatched on failure in thunk 'presenterUpdate'. Thunk dispatched by "Authorization" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
    [actionsAuthorization.PRESENTER_UPDATE_FAILURE]: (state: IAuthorizationState, action: Models.IAction<actionsAuthorization.PRESENTER_UPDATE_FAILURE_PAYLOAD>): IAuthorizationState => {
        return {
            ...state,
            presenterPhase: 'Failure',
            presenterOperationId: null,
            presenterError: action.payload.error,
        }
    },
}, ModelsAuthorization.initialState.state)

export default reducerAuthorization
