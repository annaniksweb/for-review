/*
 * Created by Burnaev M.U.
 */

import {defaultValues} from '../models/Models'

import {Enums} from 'grassica-app'
import {Models} from 'grassica-app'

import * as Converters from '../models/Converters'

import * as util from '../common/Util'

import * as actionsAboutUs from '../actions/ActionsAboutUs'

import * as thunkStartManager from '../thunk/ThunkStartManager'
import * as thunkSplash from '../thunk/ThunkSplash'
import * as thunkRegistration from '../thunk/ThunkRegistration'
import * as thunkAuthorization from '../thunk/ThunkAuthorization'
import * as thunkMain from '../thunk/ThunkMain'
import * as thunkAboutUs from '../thunk/ThunkAboutUs'
import * as thunkClaim from '../thunk/ThunkClaim'
import * as thunkMarketList from '../thunk/ThunkMarketList'
import * as thunkQrCode from '../thunk/ThunkQrCode'
import * as thunkBonus from '../thunk/ThunkBonus'
import * as thunkQuestion from '../thunk/ThunkQuestion'
import * as thunkIdea from '../thunk/ThunkIdea'
import * as thunkConfirm from '../thunk/ThunkConfirm'
import * as thunkProfile from '../thunk/ThunkProfile'
import * as thunkDataSecurity from '../thunk/ThunkDataSecurity'
import * as mockAboutUs from '../mock/MockAboutUs'
import {IRootState} from 'RootState'
import {IAboutUsState} from 'grassica-app'

import {
    config,
    /* START - Mobile platform components import. */

    /* END - Mobile platform components import. */
} from 'grassica-ui'

/* START - Thunk AboutUs additional imports and module code. */

/* END - Thunk AboutUs additional imports and module code. */

export const getInstanceReducerState = (state: IRootState, instanceType: Enums.ContainerAboutUsInstanceType): IAboutUsState => {
    switch (instanceType) {
        case 'Default':
            return state.grassicaApp.reducerAboutUs
        default:
            throw new Error('Unknown instance type.')
    }
}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched on to start Container life cycle.
 */
export const guardPerformContainerExecute = (state: IRootState, reducerState: IAboutUsState): boolean => (true && reducerState.containerExecutePhase !== 'InProgress')  // Thunk performContainerExecute transition guard check.
export const guardPerformContainerExecuteSuccess = (state: IRootState, reducerState: IAboutUsState, operationId: string): boolean => (true && reducerState.containerExecutePhase === 'InProgress' && reducerState.containerExecuteOperationId === operationId)  // Thunk performContainerExecuteSuccess transition guard check.
export const guardPerformContainerExecuteFailure = (state: IRootState, reducerState: IAboutUsState, operationId: string): boolean => (true && reducerState.containerExecutePhase === 'InProgress' && reducerState.containerExecuteOperationId === operationId)  // Thunk performContainerExecuteFailure transition guard check.
export const performContainerExecute = (instanceType: Enums.ContainerAboutUsInstanceType) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPerformContainerExecute(state, reducerState)) {
        return
    }
    let operationId = new Date().getTime().toString()

    /* START - Container AboutUs thunk performContainerExecute before execute code. */

    /* END - Container AboutUs thunk performContainerExecute before execute code. */

    dispatch(actionsAboutUs.performContainerExecute(instanceType))
    state = getState()
    reducerState = getInstanceReducerState(state, instanceType)
    if (guardPerformContainerExecute(state, reducerState)) {
        dispatch(actionsAboutUs.performContainerExecuteExecute(instanceType, operationId))

        requestAnimationFrame(() => {
            /* START - Container AboutUs thunk chain performContainerExecute execute code. */

            /* TODO Perform JS Promise and handle success and failure or dispatch performContainerExecuteSuccess and performContainerExecuteFailure later in thunk chain.

            // Success thunk chain resolve.
            state = getState()
            reducerState = getInstanceReducerState(state, instanceType)
            if (!guardPerformContainerExecuteSuccess(state, reducerState, operationId)) return
            
            dispatch(thunkAboutUs.performContainerExecuteSuccess(instanceType, instanceType, null))

            // Failure thunk chain resolve.
            state = getState()
            reducerState = getInstanceReducerState(state, instanceType)
            if (!guardPerformContainerExecuteFailure(state, reducerState, operationId)) return
            
            dispatch(thunkAboutUs.performContainerExecuteFailure(instanceType, instanceType, null)) */

            /* END - Container AboutUs thunk chain performContainerExecute execute code. */
        })

    }

    /* START - Container AboutUs thunk performContainerExecute after execute code. */

    /* END - Container AboutUs thunk performContainerExecute after execute code. */

}

export const performContainerExecuteSuccess = (instanceType: Enums.ContainerAboutUsInstanceType, data: Models.IAboutUsContainerExecute) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    dispatch(actionsAboutUs.performContainerExecuteSuccess(instanceType, data))

    /* START - Container AboutUs thunk performContainerExecuteSuccess before execute code. */
    /* END - Container AboutUs thunk performContainerExecuteSuccess before execute code. */
}

export const performContainerExecuteFailure = (instanceType: Enums.ContainerAboutUsInstanceType, error: Models.IError) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    dispatch(actionsAboutUs.performContainerExecuteFailure(instanceType, error))

    /* START - Container AboutUs thunk performContainerExecuteFailure before execute code. */
    /* END - Container AboutUs thunk performContainerExecuteFailure before execute code. */
}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
 */
export const guardPerformContainerReset = (state: IRootState, reducerState: IAboutUsState): boolean => (true)  // Thunk performContainerReset transition guard check.
export const performContainerReset = (instanceType: Enums.ContainerAboutUsInstanceType) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPerformContainerReset(state, reducerState)) {
        return
    }

    /* START - Container AboutUs thunk performContainerReset before execute code. */

    /* END - Container AboutUs thunk performContainerReset before execute code. */

    dispatch(actionsAboutUs.performContainerReset(instanceType))

    /* START - Container AboutUs thunk performContainerReset after execute code. */

    /* END - Container AboutUs thunk performContainerReset after execute code. */

}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to reset container state to default values.
 */
export const guardPerformContainerRecover = (state: IRootState, reducerState: IAboutUsState): boolean => (true)  // Thunk performContainerRecover transition guard check.
export const performContainerRecover = (instanceType: Enums.ContainerAboutUsInstanceType, stateRecovering: IAboutUsState) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPerformContainerRecover(state, reducerState)) {
        return
    }

    /* START - Container AboutUs thunk performContainerRecover before execute code. */

    /* END - Container AboutUs thunk performContainerRecover before execute code. */

    dispatch(actionsAboutUs.performContainerRecover(instanceType, stateRecovering))

    /* START - Container AboutUs thunk performContainerRecover after execute code. */

    /* END - Container AboutUs thunk performContainerRecover after execute code. */

}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to init container state.
 */
export const guardPerformContainerInit = (state: IRootState, reducerState: IAboutUsState): boolean => (true)  // Thunk performContainerInit transition guard check.
export const performContainerInit = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPerformContainerInit(state, reducerState)) {
        return
    }

    /* START - Container AboutUs thunk performContainerInit before execute code. */

    /* END - Container AboutUs thunk performContainerInit before execute code. */

    dispatch(actionsAboutUs.performContainerInit(instanceType, context, instance, ))

    /* START - Container AboutUs thunk performContainerInit after execute code. */

    /* END - Container AboutUs thunk performContainerInit after execute code. */

}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched to refresh container state.
 */
export const guardPerformContainerRefresh = (state: IRootState, reducerState: IAboutUsState): boolean => (true)  // Thunk performContainerRefresh transition guard check.
export const performContainerRefresh = (instanceType: Enums.ContainerAboutUsInstanceType, context: Models.IAboutUsContext, instance: Models.IAboutUsInstance, ) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPerformContainerRefresh(state, reducerState)) {
        return
    }

    /* START - Container AboutUs thunk performContainerRefresh before execute code. */

    /* END - Container AboutUs thunk performContainerRefresh before execute code. */

    dispatch(actionsAboutUs.performContainerRefresh(instanceType, context, instance, ))

    /* START - Container AboutUs thunk performContainerRefresh after execute code. */

    /* END - Container AboutUs thunk performContainerRefresh after execute code. */

}

/*
 * Thunk dispatched by "AboutUs" screen. Thunk dispatched on to update presenter data. Store complex and time-consuming calculated view data.
 */
export const guardPresenterUpdate = (state: IRootState, reducerState: IAboutUsState): boolean => (true && reducerState.presenterPhase !== 'InProgress')  // Thunk presenterUpdate transition guard check.
export const guardPresenterUpdateSuccess = (state: IRootState, reducerState: IAboutUsState, operationId: string): boolean => (true && reducerState.presenterPhase === 'InProgress' && reducerState.presenterOperationId === operationId)  // Thunk presenterUpdateSuccess transition guard check.
export const guardPresenterUpdateFailure = (state: IRootState, reducerState: IAboutUsState, operationId: string): boolean => (true && reducerState.presenterPhase === 'InProgress' && reducerState.presenterOperationId === operationId)  // Thunk presenterUpdateFailure transition guard check.
export const presenterUpdate = (instanceType: Enums.ContainerAboutUsInstanceType) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    if (!guardPresenterUpdate(state, reducerState)) {
        return
    }
    let operationId = new Date().getTime().toString()

    /* START - Container AboutUs thunk presenterUpdate before execute code. */

    /* END - Container AboutUs thunk presenterUpdate before execute code. */

    dispatch(actionsAboutUs.presenterUpdate(instanceType))
    state = getState()
    reducerState = getInstanceReducerState(state, instanceType)
    if (guardPresenterUpdate(state, reducerState)) {
        dispatch(actionsAboutUs.presenterUpdateExecute(instanceType, operationId))

        requestAnimationFrame(() => {
            /* START - Container AboutUs thunk chain presenterUpdate execute code. */

            /* TODO Perform JS Promise and handle success and failure or dispatch presenterUpdateSuccess and presenterUpdateFailure later in thunk chain.

            // Success thunk chain resolve.
            state = getState()
            reducerState = getInstanceReducerState(state, instanceType)
            if (!guardPresenterUpdateSuccess(state, reducerState, operationId)) return
            
            dispatch(thunkAboutUs.presenterUpdateSuccess(instanceType, instanceType, null))

            // Failure thunk chain resolve.
            state = getState()
            reducerState = getInstanceReducerState(state, instanceType)
            if (!guardPresenterUpdateFailure(state, reducerState, operationId)) return
            
            dispatch(thunkAboutUs.presenterUpdateFailure(instanceType, instanceType, null)) */

            /* END - Container AboutUs thunk chain presenterUpdate execute code. */
        })

    }

    /* START - Container AboutUs thunk presenterUpdate after execute code. */

    /* END - Container AboutUs thunk presenterUpdate after execute code. */

}

export const presenterUpdateSuccess = (instanceType: Enums.ContainerAboutUsInstanceType, data: Models.IAboutUsPresenter) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    dispatch(actionsAboutUs.presenterUpdateSuccess(instanceType, data))

    /* START - Container AboutUs thunk presenterUpdateSuccess before execute code. */
    /* END - Container AboutUs thunk presenterUpdateSuccess before execute code. */
}

export const presenterUpdateFailure = (instanceType: Enums.ContainerAboutUsInstanceType, error: Models.IError) => (dispatch: Function, getState: () => IRootState): void => {
    let state = getState()
    let reducerState = getInstanceReducerState(state, instanceType)

    dispatch(actionsAboutUs.presenterUpdateFailure(instanceType, error))

    /* START - Container AboutUs thunk presenterUpdateFailure before execute code. */
    /* END - Container AboutUs thunk presenterUpdateFailure before execute code. */
}
